package com.morabaa.team.morabaacrafts.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.morabaa.team.morabaacrafts.R;
import com.morabaa.team.morabaacrafts.activities.PhoneActivity;

public class ConfirmPhoneFragment extends Fragment {

  private OnPhoneConfirmListener onPhoneConfirmListener;

  private EditText txtConfirm;
  private String phoneNumber;

  public ConfirmPhoneFragment() {}

  public static ConfirmPhoneFragment newInstance(String phoneNumber) {
    ConfirmPhoneFragment fragment = new ConfirmPhoneFragment();
    Bundle args = new Bundle();
    args.putString(PhoneActivity.PHONE_NUMBER, phoneNumber);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if (getArguments() != null) {
      phoneNumber = getArguments().getString(PhoneActivity.PHONE_NUMBER);
    }
  }

  @Override
  public View onCreateView(
      @NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    return inflater.inflate(R.layout.fragment_phone_confirm, container, false);
  }

  @SuppressLint("MissingPermission")
  @Override
  public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    txtConfirm = view.findViewById(R.id.txtConfirm);
    Button btnConfirm = view.findViewById(R.id.btnConfirm);

    btnConfirm.setOnClickListener(
        view1 -> {
          view1.setEnabled(false);
          onPhoneConfirmListener.onConfirm(txtConfirm.getText().toString());
        });
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    if (context instanceof OnPhoneConfirmListener) {
      onPhoneConfirmListener = (OnPhoneConfirmListener) context;
    } else {
      throw new RuntimeException(context.toString() + " must implement OnPhoneConfirmListener");
    }
  }

  @Override
  public void onDetach() {
    super.onDetach();
    onPhoneConfirmListener = null;
  }

  public interface OnPhoneConfirmListener {
    void onConfirm(String confirmCode);
  }
}
