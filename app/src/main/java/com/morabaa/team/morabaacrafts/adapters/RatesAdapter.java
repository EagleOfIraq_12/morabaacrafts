package com.morabaa.team.morabaacrafts.adapters;


import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build.VERSION_CODES;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.morabaa.team.morabaacrafts.R;
import com.morabaa.team.morabaacrafts.model.Rate;
import com.morabaa.team.morabaacrafts.utils.Utils;

import java.util.List;

/**
 * Created by eagle on 10/21/2017.
 */

public class RatesAdapter extends RecyclerView.Adapter<RateHolder> {

    private List<Rate> rates;
    private Context ctx;
    private OnRateClickedListener onRateClickedListener;

    public RatesAdapter(Context ctx, List<Rate> rates) {
        this.rates = rates;
        this.ctx = ctx;

    }

    @Override
    public RateHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CardView view = (CardView) LayoutInflater.from(parent.getContext()).inflate(
                R.layout.rate_layout, parent, false);
        return new RateHolder(view);
    }

    @RequiresApi(api = VERSION_CODES.LOLLIPOP)
    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final RateHolder rateHolder,
                                 @SuppressLint("RecyclerView") final int position) {
        try {
            rateHolder.txtUserComment.setText(rates.get(position).getComment());
            rateHolder.txtUserCommentDate.setText(
                    Utils.getRelativeTimeSpan(rates.get(position).getTimeStamp())
            );
            rateHolder.ratingUserRating.setRating(rates.get(position).getRating());

            rateHolder.view.setOnClickListener(view ->
                    onRateClickedListener.onWorkClicked(
                            rates.get(rateHolder.getAdapterPosition())
                    ));

            rateHolder.txtUserName.setText(rates.get(position).getRater().getName());

            Glide.with(ctx)
                    .load(rates.get(position).getRater().getMediaItem().getUrl())
                    .into(rateHolder.imgUserImage);
        } catch (Exception ignored) {
        }

    }


    public void setOnRateClickedListener(
            OnRateClickedListener onRateClickedListener) {
        this.onRateClickedListener = onRateClickedListener;
    }

    @Override
    public int getItemCount() {
        return rates.size();
    }

    public List<Rate> getRates() {
        return rates;
    }

    public void setRates(List<Rate> rates) {
        this.rates = rates;
        notifyDataSetChanged();
    }

    public interface OnRateClickedListener {

        void onWorkClicked(Rate rate);
    }
}


class RateHolder extends RecyclerView.ViewHolder {

    CardView view;
    TextView txtUserName;
    ImageView imgUserImage;
    TextView txtUserCommentDate;
    RatingBar ratingUserRating;
    TextView txtUserComment;

    RateHolder(View itemView) {
        super(itemView);
        this.view = (CardView) itemView;
        txtUserName = view.findViewById(R.id.txtSenderName);
        imgUserImage = view.findViewById(R.id.imgUserImage);
        txtUserCommentDate = view.findViewById(R.id.txtUserCommentDate);
        ratingUserRating = view.findViewById(R.id.ratingUserRating);
        txtUserComment = view.findViewById(R.id.txtUserComment);
    }
}
