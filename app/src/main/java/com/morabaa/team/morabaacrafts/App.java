package com.morabaa.team.morabaacrafts;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;

import com.danikula.videocache.HttpProxyCacheServer;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.morabaa.team.morabaacrafts.model.Craft;
import com.morabaa.team.morabaacrafts.utils.RoomDB;
import com.morabaa.team.morabaacrafts.utils.HttpRequest;
import com.morabaa.team.morabaacrafts.utils.LocalData;

import java.util.Locale;

public class App extends Application {
  public static App instance;

  public static App getInstance() {
    return instance;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    instance = this;
    HttpRequest.init(this);
    LocalData.init(this);
    RoomDB.init(this);

    RoomDB.getInstance().craftDuo().insert(new Craft());

    HttpRequest.TIME_OUT = 25000;

    Configuration configuration = new Configuration(Resources.getSystem().getConfiguration());
    configuration.locale = new Locale("ar"); // or whichever locale you desire
    Resources.getSystem().updateConfiguration(configuration, null);

    FirebaseFirestore firestore = FirebaseFirestore.getInstance();
    FirebaseFirestoreSettings settings =
        new FirebaseFirestoreSettings.Builder().setTimestampsInSnapshotsEnabled(true).build();
    firestore.setFirestoreSettings(settings);

  }

  private HttpProxyCacheServer proxy;

  public static HttpProxyCacheServer getProxy(Context context) {
    App app = (App) context.getApplicationContext();
    return app.proxy == null ? (app.proxy = app.newProxy()) : app.proxy;
  }

  private HttpProxyCacheServer newProxy() {
    return new HttpProxyCacheServer(this);
  }
}
