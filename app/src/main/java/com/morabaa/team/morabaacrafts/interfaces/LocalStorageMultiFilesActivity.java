package com.morabaa.team.morabaacrafts.interfaces;

import android.app.Activity;
import android.content.ClipData;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.webkit.MimeTypeMap;
import android.widget.Toast;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class LocalStorageMultiFilesActivity extends Activity {
      
      private static LocalStorageMultiFilesActivity instance;
      private int reqCode;
      
      public static LocalStorageMultiFilesActivity getInstance() {
            return instance;
      }
      
      public static void setInstance(
            LocalStorageMultiFilesActivity instance) {
            LocalStorageMultiFilesActivity.instance = instance;
      }
      
      public static void end() {
            instance.finish();
      }
      
      @Override
      protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            String[] types = getIntent().getStringArrayExtra("types");
            this.reqCode = getIntent().getIntExtra("reqCode", 0);
            setInstance(this);
            StringBuilder typesStrings = new StringBuilder();
            for (String type : types) {
                  typesStrings.append(type);
            }
            
            Intent intent = new Intent();
            intent.setType(typesStrings.toString().trim());
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(
                  intent, "Select Pictures"),
                  reqCode
            );

//            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
//
//            photoPickerIntent.setType(typesStrings.toString().trim());
//            startActivityForResult(photoPickerIntent, reqCode);
      }
      
      @Override
      protected void onActivityResult(int req, int resultCode, Intent data) {
            
            try {
                  // When an Image is picked
                  if (req == reqCode && resultCode == RESULT_OK
                        && null != data) {
                        // Get the Image from data
                        
                        String[] filePathColumn = {MediaStore.Images.Media.DATA};
                        List<String> imagesEncodedList = new ArrayList<String>();
                        if (data.getData() != null) {
                              
                              Uri mImageUri = data.getData();
                              
                              // Get the cursor
                              Cursor cursor = getContentResolver().query(mImageUri,
                                    filePathColumn, null, null, null);
                              // Move to first row
                              cursor.moveToFirst();
                              
                              int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                              String imageEncoded = cursor.getString(columnIndex);
                              cursor.close();
                              
                        } else {
                              if (data.getClipData() != null) {
                                    ClipData mClipData = data.getClipData();
                                    ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
                                    
                                    LocalStorageMultiFiles.getInstance()
                                          .getOnFilesReceivedListener()
                                          .onFileReceived(mArrayUri, new ArrayList<String>(),
                                                "fileExt", "fileType");
                                    for (int i = 0; i < mClipData.getItemCount(); i++) {
                                          
                                          ClipData.Item item = mClipData.getItemAt(i);
                                          Uri uri = item.getUri();
                                          mArrayUri.add(uri);
                                          // Get the cursor
                                          Cursor cursor = getContentResolver()
                                                .query(uri, filePathColumn, null, null, null);
                                          // Move to first row
                                          cursor.moveToFirst();
                                          
                                          int columnIndex = cursor
                                                .getColumnIndex(filePathColumn[0]);
                                          String imageEncoded = cursor.getString(columnIndex);
                                          imagesEncodedList.add(imageEncoded);
                                          cursor.close();
                                          
                                    }
                                    Log.v("LOG_TAG", "Selected Images" + mArrayUri.size());
                              }
                        }
                  } else {
                        Toast.makeText(this, "You haven't picked Image",
                              Toast.LENGTH_LONG).show();
                  }
            } catch (Exception e) {
                  Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                        .show();
            }
            
            super.onActivityResult(reqCode, resultCode, data);
            
            if (resultCode == RESULT_OK && reqCode == this.reqCode || false) {
                  
                  Uri imageUri;
                  imageUri = data.getData();
                  String fileName = getName(imageUri);
                  String fileExt = getExtension(imageUri);
                  String imgPath = getPath(imageUri);
                  String fileType = imageUri.getEncodedPath().toLowerCase()
                        .contains("image".toLowerCase()) ?
                        "image" : "video";
                  if (LocalStorageMultiFiles.getInstance() != null) {
//                        LocalStorageMultiFiles.getInstance()
//                              .getOnFilesReceivedListener()
//                              .onFileReceived(imageUri, fileName, fileExt, fileType);
                  } else {
                        finish();
                  }
                  
            } else if (false) {
                  Toast.makeText(LocalStorageMultiFilesActivity.this, "You haven't picked file",
                        Toast.LENGTH_LONG)
                        .show();
                  if (LocalStorageMultiFiles.getInstance() != null) {
                        LocalStorageMultiFiles.getInstance()
                              .getOnFilesReceivedListener()
                              .onFileError("You haven't picked file");
                  } else {
                        finish();
                  }
            }
      }
      
      @Override
      protected void onResume() {
            super.onResume();
      }
      
      public String getPath(Uri uri) {
            String[] projection = {MediaStore.Images.Media.DATA};
            Cursor cursor = LocalStorageMultiFilesActivity.this.getContentResolver()
                  .query(uri, projection, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(projection[0]);
            String filePath = cursor.getString(columnIndex);
            String path = cursor.getString(column_index);
            cursor.close();
            Bitmap yourSelectedImage = BitmapFactory.decodeFile(filePath);
            File file = new File(filePath);
            return path;
      }
      
      public String getName(Uri uri) {
            ///external/images/media/7131 <- uri.getEncodedPath()
            String path = uri.getEncodedPath();
            String[] parts = path.split("/");
            return parts[parts.length - 1];
      }
      
      public String getExtension(Uri uri) {
            String extension;
            
            //Check uri format to avoid null
            if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
                  //If scheme is a content
                  final MimeTypeMap mime = MimeTypeMap.getSingleton();
                  extension = mime
                        .getExtensionFromMimeType(
                              LocalStorageMultiFilesActivity.this.getContentResolver()
                                    .getType(uri));
            } else {
                  //If scheme is a File
                  //This will replace white spaces with %20 and also other special characters. This will avoid returning null values on file name with spaces and special characters.
                  extension = MimeTypeMap
                        .getFileExtensionFromUrl(Uri.fromFile(new File(uri.getPath())).toString());
                  
            }
            
            return extension;
      }
}
