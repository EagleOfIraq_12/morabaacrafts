package com.morabaa.team.morabaacrafts.fragments;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.GsonBuilder;
import com.morabaa.team.morabaacrafts.R;
import com.morabaa.team.morabaacrafts.activities.AddWorkActivity;
import com.morabaa.team.morabaacrafts.activities.MainActivity;
import com.morabaa.team.morabaacrafts.activities.WorkDetailsActivity;
import com.morabaa.team.morabaacrafts.activities.WorksActivity;
import com.morabaa.team.morabaacrafts.adapters.RatesAdapter;
import com.morabaa.team.morabaacrafts.adapters.WorksAdapter;
import com.morabaa.team.morabaacrafts.interfaces.OnCompleteListener;
import com.morabaa.team.morabaacrafts.model.Person;
import com.morabaa.team.morabaacrafts.model.Rate;
import com.morabaa.team.morabaacrafts.model.Request;
import com.morabaa.team.morabaacrafts.model.Work;
import com.morabaa.team.morabaacrafts.utils.AWSS3Storage;
import com.morabaa.team.morabaacrafts.utils.CheckPermission;
import com.willy.ratingbar.ScaleRatingBar;

import java.util.ArrayList;
import java.util.List;

/** A simple {@link Fragment} subclass. */
public class CraftManProfileFragment extends Fragment {

  private RecyclerView recyclerViewWorks;
  private ProgressBar progressBarWorks;
  private ProgressBar progressBarRates;
  private RecyclerView recyclerViewRates;
  private Person person = new Person();

  private ImageView imgImage;
  private ImageView imgAddToFavorites;
  private TextView txtName;
  private RatingBar ratingRating;
  private TextView txtCraft;
  private TextView txtAddress;

  private TextView txtQualityRate;
  private TextView txtCostRate;
  private TextView txtDateRate;

  public CraftManProfileFragment() {
    // Required empty public constructor
  }

  public static CraftManProfileFragment newInstance(Bundle args) {
    CraftManProfileFragment fragment = new CraftManProfileFragment();
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public View onCreateView(
      LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    return inflater.inflate(R.layout.fragment_craft_man_profile, container, false);
  }

  @RequiresApi(api = VERSION_CODES.M)
  @Override
  public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
    recyclerViewWorks = view.findViewById(R.id.recyclerViewWorks);
    progressBarWorks = view.findViewById(R.id.progressBarWorks);
    progressBarRates = view.findViewById(R.id.progressBarRates);
    recyclerViewRates = view.findViewById(R.id.recyclerViewRates);
    TextView txtLoadMoreWorks = view.findViewById(R.id.txtLoadMoreWorks);
    Button btnAddRequest = view.findViewById(R.id.btnAddRequest);
    Button btnAddWork = view.findViewById(R.id.btnAddWork);

    txtQualityRate = view.findViewById(R.id.txtQualityRate);
    txtCostRate = view.findViewById(R.id.txtCostRate);
    txtDateRate = view.findViewById(R.id.txtDateRate);

    imgImage = view.findViewById(R.id.imgImage);
    imgAddToFavorites = view.findViewById(R.id.imgAddToFavorites);
    txtName = view.findViewById(R.id.txtName);
    ratingRating = view.findViewById(R.id.ratingRating);
    txtCraft = view.findViewById(R.id.txtCraft);
    txtAddress = view.findViewById(R.id.txtAddress);

    assert getArguments() != null;
    String craftManString = getArguments().getString("craftManString");
    person = Person.fromJson(craftManString);
    if (person == null) {
      person = Person.getCurrentPerson();
    }
    //        person = Person.getCurrentAsCraftMan();
    //        person.setPhoneNumber("9647814968474");

    view.findViewById(R.id.layoutContactCall)
        .setOnClickListener(v -> callNumber(person.getPhoneNumber()));
    view.findViewById(R.id.layoutContactSMS)
        .setOnClickListener(v -> sendSMS(person.getPhoneNumber()));
    view.findViewById(R.id.layoutContactViber)
        .setOnClickListener(v -> callViberNumber(person.getPhoneNumber()));
    view.findViewById(R.id.layoutContactWhatsapp)
        .setOnClickListener(v -> openWhatsApp(person.getPhoneNumber()));
    view.findViewById(R.id.layoutContactFacebook)
        .setOnClickListener(v -> openFacebookAccount(person.getPhoneNumber()));

    imgAddToFavorites.setOnClickListener(
        viewi -> {
          Person.getCurrentPerson()
              .favorite(
                  "",
                  person,
                  new OnCompleteListener<Person>() {
                    @Override
                    public void onComplete(List<Person> users) {
                      Toast.makeText(getContext(), "تمت الاضافه الى المفضله", Toast.LENGTH_SHORT)
                          .show();
                    }

                    @Override
                    public void onError(String error) {
                      Toast.makeText(getContext(), "خطأ في الاضافه الى المفضله", Toast.LENGTH_SHORT)
                          .show();
                    }
                  });
        });
    if (person.getId() == Person.getCurrentPerson().getId()) {
      view.findViewById(R.id.layoutContactContainer).setVisibility(View.GONE);
      imgAddToFavorites.setVisibility(View.GONE);
      btnAddRequest.setVisibility(View.GONE);
      btnAddWork.setVisibility(View.VISIBLE);
      btnAddWork.setOnClickListener(
          view1 -> {
            Intent addWorkActivityIntent = new Intent(getContext(), AddWorkActivity.class);
            startActivity(addWorkActivityIntent);
          });
    } else {
      btnAddRequest.setVisibility(View.VISIBLE);
      btnAddRequest.setOnClickListener(
          view12 -> {
            Request request = new Request();
            request.setSender(Person.getCurrentPerson());
            request.setReceiver(person);
            request.setStatus(Request.STATUS.INIT);
            AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
            alertDialog.setCancelable(true);
            LayoutInflater inflater =
                (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View dialogView =
                inflater.inflate(R.layout.dialog_user_edit_request_layout, null, false);

            dialogView.findViewById(R.id.frameDialog).setBackgroundColor(Color.argb(0, 0, 0, 0));
            Button btnOk = dialogView.findViewById(R.id.btnOk);
            final EditText txtRequestUserNote = dialogView.findViewById(R.id.txtRequestSenderNote);
            final EditText txtAddress = dialogView.findViewById(R.id.txtAddress);
            txtAddress.setText(Person.getCurrentPerson().getAddress());
            final TextView txtDialogTitle = dialogView.findViewById(R.id.txtDialogTitle);
            txtDialogTitle.setText("اضافة طلب");
            txtRequestUserNote.setText(request.getSenderNote());
            final ProgressBar progressBarUpdating =
                dialogView.findViewById(R.id.progressBarUpdating);

            btnOk.setOnClickListener(
                view1 -> {
                  progressBarUpdating.setVisibility(View.VISIBLE);
                  request.setSenderNote(txtRequestUserNote.getText().toString());
                  request.setAddress(txtAddress.getText().toString());
                  request.push(
                      "",
                      new OnCompleteListener<Request>() {
                        @Override
                        public void onComplete(List<Request> requests) {
                          System.out.println("requests = [" + requests.get(0).getStatus() + "]");
                          progressBarUpdating.setVisibility(View.GONE);
                          alertDialog.dismiss();
                          person.setRating(0.0F);
                          try {
                            ((MainActivity) getActivity()).onDataUpdated(0);
                          } catch (Exception e) {
                          }
                        }

                        @Override
                        public void onError(String error) {
                          progressBarUpdating.setVisibility(View.GONE);
                          alertDialog.dismiss();
                        }
                      });
                });

            alertDialog.setView(dialogView);
            alertDialog.show();
          });
      btnAddWork.setVisibility(View.GONE);
    }
    try {
      txtName.setText(person.getName());
      txtAddress.setText(person.getAddress());
      ratingRating.setRating(person.getRating());
    } catch (Exception e) {
    }
    person.getById(
        this.getClass().getName(),
        new OnCompleteListener<Person>() {
          @Override
          public void onComplete(List<Person> craftMEN) {
            person = craftMEN.get(0);
            initPerson(person);
          }

          @Override
          public void onError(String error) {
            System.out.println("error = " + error);
          }
        });
    person.setWorks(new ArrayList<>());

    Work work = new Work();
    work.setPerson(person);
    work.getMostRecent(
        "",
        new OnCompleteListener<Work>() {
          @Override
          public void onComplete(List<Work> works) {

            initWorks(works);
          }

          @Override
          public void onError(String error) {}
        });

    final Rate rate = new Rate();
    rate.setRated(person);
    rate.getPersonRates(
        "",
        new OnCompleteListener<Rate>() {
          @Override
          public void onComplete(List<Rate> rates) {
            initRates(rates);
          }

          @Override
          public void onError(String error) {}
        });

    txtLoadMoreWorks.setOnClickListener(
        view13 -> {
          Intent craftManWorksIntent = new Intent(getContext(), WorksActivity.class);
          craftManWorksIntent.putExtra(
              "craftManString",
              new GsonBuilder()
                  .serializeSpecialFloatingPointValues()
                  .create()
                  .toJson(person, Person.class));
          startActivity(craftManWorksIntent);
        });

    ((MainActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
  }

  private void initPerson(Person person) {
    this.person = person;
    txtName.setText(String.valueOf(person.getName()));
    float rating =
        person.getRate().getRatingSpeed()
            + person.getRate().getRatingAccuracy()
            + person.getRate().getRatingDate();
    ratingRating.setRating(rating / 3);
    txtAddress.setText(person.getAddress());

    if (person.getCrafts() != null && person.getCrafts().size() > 0) {
      StringBuilder craftsString = new StringBuilder(person.getCrafts().get(0).getName());
      for (int i = 1; i < person.getCrafts().size(); i++) {
        craftsString.append(" | ").append(person.getCrafts().get(i).getName());
      }
      txtCraft.setText(craftsString.toString());
    }

    try {
      assert getContext() != null;
      Glide.with(getContext())
          .load(AWSS3Storage.getReference(person.getMediaItem().getUrl()))
          .into(imgImage);
    } catch (Exception e) {
      //            Glide.with(getContext())
      //
      // .load(getContext().getResources().getDrawable(R.drawable.mastar_crafter_icon))
      //                    .into(imgImage);
    }
    initRate(person.getRate());
  }

  private void initRate(Rate rate) {
    if ((int) rate.getRatingSpeed() == -1) {
      rate.setRatingSpeed(0.0F);
    }
    if ((int) rate.getRatingDate() == -1) {
      rate.setRatingDate(0.0F);
    }
    if ((int) rate.getRatingAccuracy() == -1) {
      rate.setRatingAccuracy(0.0F);
    }
    txtCostRate.setText(String.valueOf((int) (rate.getRatingSpeed() * 20)));
    txtQualityRate.setText(String.valueOf((int) (rate.getRatingAccuracy() * 20)));
    txtDateRate.setText(String.valueOf((int) (rate.getRatingDate() * 20)));
  }

  private void initWorks(List<Work> works) {
    progressBarWorks.setVisibility(View.GONE);
    recyclerViewWorks.setLayoutManager(
        new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
    WorksAdapter worksAdapter =
        new WorksAdapter(getContext(), new ArrayList<>(), R.layout.work_layout_horizontal);

    worksAdapter.setOnWorkClickedListener(
        work -> {
          Intent workActivityIntent = new Intent(getContext(), WorkDetailsActivity.class);
          workActivityIntent.putExtra(
              "workString",
              new GsonBuilder()
                  .serializeSpecialFloatingPointValues()
                  .create()
                  .toJson(work, Work.class));
          startActivity(workActivityIntent);
        });

    recyclerViewWorks.setAdapter(worksAdapter);

    worksAdapter.setWorks(works);
  }

  private void initRates(List<Rate> rates) {
    progressBarRates.setVisibility(View.GONE);
    RatesAdapter ratesAdapter = new RatesAdapter(getContext(), new ArrayList<>());

    recyclerViewRates.setLayoutManager(
        new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
    ratesAdapter.setOnRateClickedListener(
        rate -> {
          if (Person.getCurrentPerson() != null
              && rate.getRater().getId() == Person.getCurrentPerson().getId()) {
            AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
            alertDialog.setCancelable(true);
            LayoutInflater inflater =
                (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view =
                inflater.inflate(R.layout.dialog_rate_craft_man_request_layout, null, false);
            view.findViewById(R.id.frameDialog).setBackgroundColor(Color.argb(0, 0, 0, 0));
            Button btnOk = view.findViewById(R.id.btnOk);
            final EditText txtRequestCraftManRate = view.findViewById(R.id.txtRequestCraftManRate);
            final ScaleRatingBar ratingCraftManRatingSpeed =
                view.findViewById(R.id.ratingCraftManRatingSpeed);
            final ScaleRatingBar ratingCraftManRatingDate =
                view.findViewById(R.id.ratingCraftManRatingDate);
            final ScaleRatingBar ratingCraftManRatingAccuracy =
                view.findViewById(R.id.ratingCraftManRatingAccuracy);
            final ProgressBar progressBarUpdating = view.findViewById(R.id.progressBarUpdating);
            txtRequestCraftManRate.setText(rate.getComment());
            ratingCraftManRatingAccuracy.setRating(rate.getRatingAccuracy());
            ratingCraftManRatingDate.setRating(rate.getRatingDate());
            ratingCraftManRatingSpeed.setRating(rate.getRatingSpeed());
            btnOk.setOnClickListener(
                view1 -> {
                  progressBarUpdating.setVisibility(View.VISIBLE);
                  rate.setComment(txtRequestCraftManRate.getText().toString());
                  rate.setRatingAccuracy(ratingCraftManRatingAccuracy.getRating());
                  rate.setRatingDate(ratingCraftManRatingDate.getRating());
                  rate.setRatingSpeed(ratingCraftManRatingSpeed.getRating());
                  rate.setRated(person);
                  rate.setRater(Person.getCurrentPerson());
                  rate.updatePersonRate(
                      "",
                      new OnCompleteListener<Rate>() {
                        @Override
                        public void onComplete(List<Rate> rates) {
                          progressBarUpdating.setVisibility(View.GONE);
                          alertDialog.dismiss();
                          getFragmentManager()
                              .beginTransaction()
                              .detach(CraftManProfileFragment.this)
                              .attach(CraftManProfileFragment.this)
                              .commit();
                        }

                        @Override
                        public void onError(String error) {
                          progressBarUpdating.setVisibility(View.GONE);
                          alertDialog.dismiss();
                        }
                      });
                });
            alertDialog.setView(view);
            alertDialog.show();
          }
        });
    recyclerViewRates.setAdapter(ratesAdapter);

    ratesAdapter.setRates(rates);
  }

  public void openFacebookAccount(String url) {

    PackageManager pm = getContext().getPackageManager();
    Uri uri = Uri.parse(url);

    try {
      ApplicationInfo applicationInfo = pm.getApplicationInfo("com.facebook.katana", 0);
      if (applicationInfo.enabled) {
        uri = Uri.parse("fb://facewebmodal/f?href=" + url);
      }
    } catch (PackageManager.NameNotFoundException ignored) {
    }

    getContext().startActivity(new Intent(Intent.ACTION_VIEW, uri));
  }

  public void addViberNumber(String phone) {
    String viberPackageName = "com.viber.voip";

    try {
      getContext()
          .startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("viber://add?number=" + phone)));
    } catch (ActivityNotFoundException ex) {
      try {
        getContext()
            .startActivity(
                new Intent(
                    Intent.ACTION_VIEW, Uri.parse("market://details?id=+" + viberPackageName)));
      } catch (ActivityNotFoundException exe) {
        getContext()
            .startActivity(
                new Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse(
                        "https://play.google.com/store/apps/details?id=" + viberPackageName)));
      }
    }
  }

  public void callViberNumber(Context context, String phone) {
    Uri uri = Uri.parse("tel:" + Uri.encode(phone));
    Intent intent = new Intent("android.intent.action.VIEW");
    intent.setClassName("com.viber.voip", "com.viber.voip.WelcomeActivity");
    intent.setData(uri);
    context.startActivity(intent);
  }

  public void sendSMS(String phoneNumber) {
    String number = "+964" + phoneNumber; // The number on which you want to send SMS
    startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", number, null)));
  }

  public void callNumber(String phoneNumber) {
    CheckPermission.newInstance(
        getContext(),
        Manifest.permission.CALL_PHONE,
        granted -> {
          if (granted) {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + "+964" + phoneNumber));
            startActivity(callIntent);
          } else {

          }
        });
  }

  public void callViberNumber(String phoneNumber) {
    String finalPhoneNumber = "+964" + phoneNumber;
    CheckPermission.newInstance(
        getContext(),
        Manifest.permission.READ_CONTACTS,
        granted -> {
          if (granted) {

            Uri uri = getUriFromPhoneNumber(finalPhoneNumber);
            if (uri != null) {
              Intent intent = new Intent("android.intent.action.VIEW");
              intent.setClassName("com.viber.voip", "com.viber.voip.SystemDialogActivityPublic");
              intent.setData(uri);
              intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
              getContext().startActivity(intent);
            } else {
              Toast.makeText(
                      getContext(), "Number is not in Viber Contacts List", Toast.LENGTH_LONG)
                  .show();
              addViberNumber(finalPhoneNumber);
            }
          } else {

          }
        });
  }

  private Uri getUriFromPhoneNumber(String phoneNumber) {
    Uri uri = null;
    String contactId = getContactIdByPhoneNumber(phoneNumber);
    if (!TextUtils.isEmpty(contactId)) {
      Cursor cursor =
          getContext()
              .getContentResolver()
              .query(
                  ContactsContract.Data.CONTENT_URI,
                  new String[] {ContactsContract.Data._ID},
                  ContactsContract.Data.DATA2
                      + "=? AND "
                      + ContactsContract.Data.CONTACT_ID
                      + " = ?",
                  new String[] {"Viber", contactId},
                  null);
      if (cursor != null) {
        while (cursor.moveToNext()) {
          String id = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.Data._ID));
          if (!TextUtils.isEmpty(id)) {
            uri = Uri.parse(ContactsContract.Data.CONTENT_URI + "/" + id);
            break;
          }
        }
        cursor.close();
      }
    }
    return uri;
  }

  private String getContactIdByPhoneNumber(String phoneNumber) {
    ContentResolver contentResolver = getContext().getContentResolver();
    String contactId = null;
    Uri uri =
        Uri.withAppendedPath(
            ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));

    String[] projection = new String[] {ContactsContract.PhoneLookup._ID};

    Cursor cursor = contentResolver.query(uri, projection, null, null, null);

    if (cursor != null) {
      while (cursor.moveToNext()) {
        contactId =
            cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.PhoneLookup._ID));
      }
      cursor.close();
    }
    return contactId;
  }

  private void openWhatsApp(String phoneNumber) {
    phoneNumber = "+964" + phoneNumber;
    try {
      Intent sendIntent = new Intent("android.intent.action.MAIN");
      // sendIntent.setComponent(new ComponentName("com.whatsapp", "com.whatsapp.Conversation"));
      sendIntent.setAction(Intent.ACTION_SEND);
      sendIntent.setType("text/plain");
      sendIntent.putExtra(Intent.EXTRA_TEXT, "This is my text to send.");
      sendIntent.putExtra(
          "jid", phoneNumber + "@s.whatsapp.net"); // phone number without "+" prefix
      sendIntent.setPackage("com.whatsapp");
      startActivity(sendIntent);
    } catch (Exception e) {
      final String appPackageName = "com.whatsapp";
      try {
        startActivity(
            new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
      } catch (android.content.ActivityNotFoundException anfe) {
        startActivity(
            new Intent(
                Intent.ACTION_VIEW,
                Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
      }
      Toast.makeText(getContext(), "قم بتنزيل التطبيق", Toast.LENGTH_SHORT).show();
      //            Toast.makeText(getContext(), "Error/n" + e.toString(),
      // Toast.LENGTH_SHORT).show();
    }
  }
}
