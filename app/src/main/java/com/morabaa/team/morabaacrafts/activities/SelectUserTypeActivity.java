package com.morabaa.team.morabaacrafts.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.morabaa.team.morabaacrafts.R;

public class SelectUserTypeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_select_user_type);
        ImageView imgUser = findViewById(R.id.imgSenderImage);
        ImageView imgCraftMan = findViewById(R.id.imgReceiverImage);
        imgUser.setOnClickListener(view1 -> {
            Intent mainIntent = new Intent(this,
                    SignUpActivity.class);
            startActivity(mainIntent);
            finish();

        });
        imgCraftMan.setOnClickListener(view1 -> {
            Intent mainIntent = new Intent(this,
                    SignUpActivity.class);
            startActivity(mainIntent);
            finish();

        });

    }
}
