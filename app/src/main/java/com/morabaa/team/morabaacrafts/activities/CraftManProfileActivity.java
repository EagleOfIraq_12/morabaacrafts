package com.morabaa.team.morabaacrafts.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.transition.Scene;
import android.support.transition.TransitionManager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.GsonBuilder;
import com.morabaa.team.morabaacrafts.R;
import com.morabaa.team.morabaacrafts.adapters.RatesAdapter;
import com.morabaa.team.morabaacrafts.adapters.RatesAdapter.OnRateClickedListener;
import com.morabaa.team.morabaacrafts.adapters.WorksAdapter;
import com.morabaa.team.morabaacrafts.adapters.WorksAdapter.OnWorkClickedListener;
import com.morabaa.team.morabaacrafts.interfaces.OnCompleteListener;
import com.morabaa.team.morabaacrafts.model.MediaItem;
import com.morabaa.team.morabaacrafts.model.Person;
import com.morabaa.team.morabaacrafts.model.Rate;
import com.morabaa.team.morabaacrafts.model.Request;
import com.morabaa.team.morabaacrafts.model.Work;
import com.morabaa.team.morabaacrafts.utils.LocalData;

import java.util.ArrayList;
import java.util.List;

public class CraftManProfileActivity extends AppCompatActivity {
      
      private Scene minScene;
      private Scene maxScene;
      private Scene midScene;
      private WorksAdapter worksAdapter;
      private RatesAdapter ratesAdapter;
      private RecyclerView recyclerViewWorks;
      private RecyclerView recyclerViewRates;
      private TextView txtLoadMoreWorks;
      private TextView txtHeaderUserName;
      private Person person;
      private Button btnAddWork;
      private Button btnAddRequest;
      private NestedScrollView nestedScrollView;
      private int s = 0;
      
      @Override
      protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_craft_man_profile);
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            recyclerViewWorks = findViewById(R.id.recyclerViewWorks);
            recyclerViewRates = findViewById(R.id.recyclerViewRates);
//            nestedScrollView = findViewById(R.id.nestedScrollView);
            txtLoadMoreWorks = findViewById(R.id.txtLoadMoreWorks);
            btnAddRequest = findViewById(R.id.btnAddRequest);
            btnAddWork = findViewById(R.id.btnAddWork);
            String craftManString = getIntent().getStringExtra("craftManString");
            person = new GsonBuilder()
                              .serializeSpecialFloatingPointValues()
                              .create().fromJson(craftManString, Person.class);
            setTitle(person.getName());
            if (LocalData.getInstance().getInt("personType") == 0 || true) {
                  btnAddRequest.setVisibility(View.GONE);
                  btnAddWork.setVisibility(View.VISIBLE);
                  btnAddWork.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View view) {
                              Intent addWorkActivityIntent = new Intent(
                                    CraftManProfileActivity.this,
                                    AddWorkActivity.class);
                              startActivity(addWorkActivityIntent);
                        }
                  });
            } else {
                  btnAddRequest.setVisibility(View.VISIBLE);
                  btnAddWork.setVisibility(View.GONE);
            }
            person.getById(this.getClass().getName(), new OnCompleteListener<Person>() {
                  @Override
                  public void onComplete(List<Person> craftMEN) {
                        person = craftMEN.get(0);
                        initHeaderData();
                        Toast.makeText(CraftManProfileActivity.this, "" + person.getName(),
                              Toast.LENGTH_SHORT)
                              .show();
                  }
                  
                  @Override
                  public void onError(String error) {
                        
                  }
            });
            
            person.setWorks(new ArrayList<Work>());
            final List<Request> requests = new ArrayList<>();
            for (int i = 0; i < 20; i++) {
                  final int finalI = i;
                  requests.add(new Request() {{
                        setPrice(finalI + finalI * 0.1f);
                  }});
            }
            List<Work> works = new ArrayList<>();
            
            for (int i = 0; i < 20; i++) {
                  final int finalI = i;
                  works.add(new Work() {{
                        setTitle("عمل  " + finalI + finalI * 0.1f);
                        setRequest(requests.get(finalI));
                        setMediaItems(new ArrayList<MediaItem>());
                  }});
            }
            for (final Work work : works) {
                  for (int i = 0; i <= 2; i++) {
                        final int finalI = i;
                        work.getMediaItems()
                              .add(
                                    new MediaItem() {{
                                          setId(finalI);
                                          setUrl(
                                                "https://dummyimage.com/2650x2000/a2fea3/8f24e6&text="
                                                      + work.getTitle() + finalI);
                                    }}
                              );
                  }
            }
            
            Work work = new Work();
            work.setPerson(person);
            work.getMostRecent("", new OnCompleteListener<Work>() {
                  @Override
                  public void onComplete(List<Work> works) {
                        for (final Work work : works) {
                              for (int i = 0; i <= 2; i++) {
                                    final int finalI = i;
                                    work.getMediaItems()
                                          .add(
                                                new MediaItem() {{
                                                      setId(finalI);
                                                      setUrl(
                                                            "https://dummyimage.com/2650x2000/a2fea3/8f24e6&text="
                                                                  + work.getTitle() + finalI);
                                                }}
                                          );
                              }
                        }
                        initWorks(works);
                  }
                  
                  @Override
                  public void onError(String error) {
                  
                  }
            });
            
            Rate rate = new Rate();
            rate.setRated(person);
            rate.getAll("", new OnCompleteListener<Rate>() {
                  @Override
                  public void onComplete(List<Rate> rates) {
                        initRates(rates);
                        
                  }
                  
                  @Override
                  public void onError(String error) {
                  
                  }
            });
            txtLoadMoreWorks.setOnClickListener(new OnClickListener() {
                  @Override
                  public void onClick(View view) {
                        Intent craftManWorksIntent = new Intent(
                              CraftManProfileActivity.this, WorksActivity.class
                        );
                        craftManWorksIntent.putExtra("craftManString",
                              new GsonBuilder()
                              .serializeSpecialFloatingPointValues()
                              .create().toJson(person, Person.class));
                        startActivity(craftManWorksIntent);
                  }
            });
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            final ViewGroup mSceneRoot = findViewById(R.id.frameHeaderScene);
            
            minScene =
                  Scene.getSceneForLayout(mSceneRoot, R.layout.min_scene, this);
            maxScene =
                  Scene.getSceneForLayout(mSceneRoot, R.layout.max_scene, this);
            
            midScene =
                  Scene.getSceneForLayout(mSceneRoot, R.layout.mid_scene, this);
            TransitionManager.beginDelayedTransition(mSceneRoot);
            TransitionManager.go(maxScene);
            txtHeaderUserName = findViewById(R.id.txtHeaderUserName);
            initHeaderData();
            
            setTitle(" ");
            
            final CollapsingToolbarLayout toolbarLayout =
                  findViewById(R.id.toolbarLayout);
            AppBarLayout appBarLayout = findViewById(R.id.appBarLayout);
            appBarLayout.addOnOffsetChangedListener(
                  new AppBarLayout.OnOffsetChangedListener() {
                        boolean isShow = true;
                        int scrollRange = -1;
                        int mid = 1;
                        
                        @Override
                        public void onOffsetChanged(AppBarLayout appBarLayout,
                              int verticalOffset) {
//                              System.out
//                                    .println("verticalOffset : " + verticalOffset);
                              mid = (int) (scrollRange * 0.5 * -1);
                              if (scrollRange == -1) {
                                    scrollRange = appBarLayout.getTotalScrollRange();
                              }
                              if (scrollRange + verticalOffset == 0 && false) {
                                    TransitionManager.go(minScene);
                              } else if (verticalOffset > mid - 50 && verticalOffset < mid + 50
                                    && false) {

//                              TransitionManager.go(midScene);
                              } else if (verticalOffset > mid + mid / 2) {
                                    if (s != 1) {
                                          TransitionManager.go(maxScene);
                                          initHeaderData();
                                          s = 1;
                                    }
                                    
                              } else if (verticalOffset < mid) {
                                    if (s != 2) {
                                          TransitionManager.go(minScene);
                                          initHeaderData();
                                          s = 2;
                                    }
                              }
                              initHeaderData();
                       /*
                         if (scrollRange + verticalOffset == 0) {
                              toolbarLayout.setTitle(" ");
                              TransitionManager.go(minScene);
                               nestedScrollView.scrollTo(0, 0);
      
                               isShow = true;
                              toolbarLayout.setTitle(" ");
                              TransitionManager.go(maxScene);
                              //carefull there should a space between double quote otherwise it wont work
                              isShow = false;
                        }  */
                        }
                  });
      }
      
      private void initHeaderData() {
            txtHeaderUserName.setText(person.getName());
            txtHeaderUserName.setVisibility(View.GONE);
            
      }
      
      private void initWorks(List<Work> works) {
            recyclerViewWorks.setLayoutManager(new LinearLayoutManager(
                  CraftManProfileActivity.this,
                  LinearLayoutManager.HORIZONTAL, false
            ));
            worksAdapter = new WorksAdapter(
                  CraftManProfileActivity.this,
                  new ArrayList<Work>(),
                  R.layout.work_layout_horizontal
            );
           
            worksAdapter.setOnWorkClickedListener(new OnWorkClickedListener() {
                  @Override
                  public void onWorkClicked(Work work) {
                        Intent workActivityIntent = new Intent(
                              CraftManProfileActivity.this,
                              WorkDetailsActivity.class
                        );
                        workActivityIntent.putExtra("workString",
                              new GsonBuilder()
                              .serializeSpecialFloatingPointValues()
                              .create().toJson(work, Work.class));
                        startActivity(workActivityIntent);
                  }
            });
           
            recyclerViewWorks.setAdapter(worksAdapter);
            
            worksAdapter.setWorks(works);
      }
      
      private void initRates(List<Rate> rates) {
            ratesAdapter = new RatesAdapter(
                  CraftManProfileActivity.this,
                  new ArrayList<Rate>()
            );
            
            recyclerViewRates.setLayoutManager(new LinearLayoutManager(
                  CraftManProfileActivity.this,
                  LinearLayoutManager.VERTICAL, false
            ));
            ratesAdapter.setOnRateClickedListener(new OnRateClickedListener() {
                  @Override
                  public void onWorkClicked(Rate rate) {
                  
                  }
            });
            recyclerViewRates.setAdapter(ratesAdapter);
            
            ratesAdapter.setRates(rates);
      }
}
