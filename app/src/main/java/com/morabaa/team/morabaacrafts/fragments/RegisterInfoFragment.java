package com.morabaa.team.morabaacrafts.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import com.asksira.bsimagepicker.BSImagePicker;
import com.bumptech.glide.Glide;
import com.morabaa.team.morabaacrafts.R;
import com.morabaa.team.morabaacrafts.activities.PhoneActivity;
import com.morabaa.team.morabaacrafts.interfaces.OnCompleteListener;
import com.morabaa.team.morabaacrafts.model.Governate;
import com.morabaa.team.morabaacrafts.model.MediaItem;
import com.morabaa.team.morabaacrafts.model.Person;
import com.morabaa.team.morabaacrafts.utils.AWSS3Storage;
import com.morabaa.team.morabaacrafts.utils.RoomDB;

import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.glide.transformations.BlurTransformation;

import static com.bumptech.glide.request.RequestOptions.bitmapTransform;

public class RegisterInfoFragment extends Fragment
    implements BSImagePicker.OnSingleImageSelectedListener {

  private String phoneNumber;
  private EditText txtFullName;
  private EditText txtAge;
  private Spinner spinnerGovernate;
  private EditText txtAddress;
  private EditText txtContactViber;
  private EditText txtContactFacebook;
  private EditText txtContactWhatsapp;
  private Button btnSignUp;
  private Governate governate = new Governate();
  private ImageView imgPersonImage;
  private Person person = new Person();
  private CheckBox checkAcceptPolicy;

  public RegisterInfoFragment() {}

  public static RegisterInfoFragment newInstance(String phoneNumber) {
    RegisterInfoFragment fragment = new RegisterInfoFragment();
    Bundle args = new Bundle();
    args.putString(PhoneActivity.PHONE_NUMBER, phoneNumber);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if (getArguments() != null) {
      phoneNumber = getArguments().getString(PhoneActivity.PHONE_NUMBER);
    }
  }

  @Override
  public View onCreateView(
      LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    return inflater.inflate(R.layout.fragment_register_info, container, false);
  }

  @Override
  public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    checkAcceptPolicy = view.findViewById(R.id.checkAcceptPolicy);
    imgPersonImage = view.findViewById(R.id.imgPersonImage);
    txtFullName = view.findViewById(R.id.txtFullName);
    txtAge = view.findViewById(R.id.txtAge);
    spinnerGovernate = view.findViewById(R.id.spinnerGovernate);
    txtAddress = view.findViewById(R.id.txtAddress);
    txtContactViber = view.findViewById(R.id.txtContactViber);
    txtContactFacebook = view.findViewById(R.id.txtContactFacebook);
    txtContactWhatsapp = view.findViewById(R.id.txtContactWhatsapp);
    btnSignUp = view.findViewById(R.id.btnSignUp);

    new Governate()
        .getAll(
            "",
            new OnCompleteListener<Governate>() {
              @Override
              public void onComplete(List<Governate> governates) {
                RoomDB.getInstance()
                    .governateDuo()
                    .insert(governates.toArray(new Governate[governates.size()]));
                List<String> strings = new ArrayList<>();
                for (Governate governate : governates) {
                  strings.add(governate.getGovernate());
                }
                ArrayAdapter<String> governatesAdapter =
                    new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, strings);
                governatesAdapter.setDropDownViewResource(
                    android.R.layout.simple_spinner_dropdown_item);
                spinnerGovernate.setAdapter(governatesAdapter);
                spinnerGovernate.setOnItemSelectedListener(
                    new AdapterView.OnItemSelectedListener() {
                      @Override
                      public void onItemSelected(
                          AdapterView<?> parent, View view, int position, long id) {
                        String s = spinnerGovernate.getItemAtPosition(position).toString();
                        try {
                          for (Governate governate : governates) {
                            if (governate.getGovernate().equals(s)) {
                              RegisterInfoFragment.this.governate = governate;
                            }
                          }
                        } catch (Exception ignored) {
                        }
                      }

                      @Override
                      public void onNothingSelected(AdapterView<?> parent) {}
                    });
              }

              @Override
              public void onError(String error) {
                List<Governate> governates = RoomDB.getInstance().governateDuo().GOVERNATES();
                List<String> strings = new ArrayList<>();
                for (Governate governate : governates) {
                  strings.add(governate.getGovernate());
                }
                ArrayAdapter<String> governatesAdapter =
                    new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, strings);
                governatesAdapter.setDropDownViewResource(
                    android.R.layout.simple_spinner_dropdown_item);
                spinnerGovernate.setAdapter(governatesAdapter);
                spinnerGovernate.setOnItemSelectedListener(
                    new AdapterView.OnItemSelectedListener() {
                      @Override
                      public void onItemSelected(
                          AdapterView<?> parent, View view, int position, long id) {
                        String s = spinnerGovernate.getItemAtPosition(position).toString();
                        for (Governate governate : governates) {
                          if (governate.getGovernate().equals(s)) {
                            RegisterInfoFragment.this.governate = governate;
                          }
                        }
                      }

                      // todo craftman by craft id
                      @Override
                      public void onNothingSelected(AdapterView<?> parent) {}
                    });
              }
            });
    imgPersonImage.setOnClickListener(this::pickImage);
    btnSignUp.setOnClickListener(this::signUp);
  }

  private void signUp(View view) {
    getParentFragment()
        .getChildFragmentManager()
        .beginTransaction()
        .replace(R.id.frameRegister, RegisterCraftsFragment.newInstance(""))
        .addToBackStack(null)
        .commit();
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
  }

  @Override
  public void onDetach() {
    super.onDetach();
  }

  public void pickImage(View view) {
    BSImagePicker singleSelectionPicker =
        new BSImagePicker.Builder("com.morabaa.team.morabaacrafts.fileprovider")
            //                .setMaximumDisplayingImages(
            //                        24) //Default: Integer.MAX_VALUE. Don't worry about
            // performance :)
            .setSpanCount(3) // Default: 3. This is the number of columns
            .setGridSpacing(
                com.asksira.bsimagepicker.Utils.dp2px(
                    2)) // Default: 2dp. Remember to pass in a value in pixel.
            .setPeekHeight(
                com.asksira.bsimagepicker.Utils.dp2px(
                    360)) // Default: 360dp. This is the initial height of the dialog.
            .hideCameraTile() // Default: show. Set this if you don't want user to take photo.
            .hideGalleryTile() // Default: show. Set this if you don't want to further let user
            // select from a gallery app. In such case, I suggest you to set
            // maximum     displaying    images to Integer.MAX_VALUE.
            .build();
    singleSelectionPicker.show(getFragmentManager(), "picker");
  }

  @Override
  public void onSingleImageSelected(Uri uri) {
    Glide.with(this)
        .load(uri)
        .apply(bitmapTransform(new BlurTransformation(10, 3)))
        .into(imgPersonImage);

    AWSS3Storage.getInstance(getContext())
        .uploadMini(
            uri,
            new AWSS3Storage.OnUploadProgressListener() {
              @Override
              public void onProgress(double progress) {}

              @Override
              public void onSuccess(String fileName) {
                Glide.with(getContext()).load(uri).into(imgPersonImage);
                MediaItem mediaItem = new MediaItem();
                mediaItem.setUrl(fileName);
                person.setMediaItem(mediaItem);
              }

              @Override
              public void onFailure(String error) {}
            });
  }

  public void acceptPolicy(View view) {
    String url = "http://morabaacraftsapi-dev.us-east-2.elasticbeanstalk.com";
    Intent i = new Intent(Intent.ACTION_VIEW);
    i.setData(Uri.parse(url));
    startActivity(i);
  }
}
