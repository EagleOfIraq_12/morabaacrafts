package com.morabaa.team.morabaacrafts.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;

import com.morabaa.team.morabaacrafts.utils.Utils;

import java.util.List;

@Entity
public class Usera extends Person {
    private static final String CLASS_ROOT = Utils.API_ROOT + "User/";

    //region properties
    @Ignore
    private List<Request> requests;
    //endregion

    public Usera() {
        super();
    }

//    private static Type getClassType() {
//        return new TypeToken<List<User>>() {
//        }.getType();
//    }

    //region getters and setters
    public List<Request> getReceivedRequests() {
        return requests;
    }

    public void setReceivedRequests(List<Request> receivedRequests) {
        this.requests = receivedRequests;
    }


//    public void register(String tag,
//                         OnCompleteListener<User> onCompleteListener) {
//        HttpRequest.getInstance()
//                .addRequest(CLASS_ROOT + "register",
//                        new GsonBuilder()
//                                .serializeSpecialFloatingPointValues()
//                                .create().toJson(this, User.class),
//                        getClassType(), tag, onCompleteListener);
//    }

//    public void favorite(String tag, Person craftMan,
//                         OnCompleteListener<User> onCompleteListener) {
//        HttpRequest.getInstance()
//                .addRequest(CLASS_ROOT + "favorite",
//                        "{\"userId\":" + this.getId() + "," +
//                                "\"craftManId\":" + craftMan.getId() + "}",
//                        getClassType(), tag, onCompleteListener);
//    }

//    public void update(String tag,
//                       OnCompleteListener<User> onCompleteListener) {
//        HttpRequest.getInstance()
//                .addRequest(CLASS_ROOT + "update",
//                        new GsonBuilder()
//                                .serializeSpecialFloatingPointValues()
//                                .create().toJson(this, this.getClass()),
//                        getClassType(), tag, onCompleteListener);
//    }

//    public void getFavorites(String tag,
//                             OnCompleteListener<Person> onCompleteListener) {
//        HttpRequest.getInstance()
//                .addRequest(CLASS_ROOT + "getFavorites",
//                        new GsonBuilder()
//                                .serializeSpecialFloatingPointValues()
//                                .create().toJson(this, this.getClass()),
//                        Person.getClassType(), tag, onCompleteListener);
//    }

    //endregion

//    @Dao
//    public interface UserDuo {
//
//        @Query("SELECT * FROM User")
//        List<User> USERS();
//
//        @Insert(onConflict = OnConflictStrategy.REPLACE)
//        void insert(User... users);
//
//        @Delete
//        void delete(User... users);
//
//        @Update
//        void Update(User... users);
//
//        @Query("SELECT COUNT(id) FROM User")
//        int count();
//    }
}

/*
 @Dao
      public interface Duo {
            
            @Query("SELECT * FROM ")
            List<> ();
            
            @Insert(onConflict = OnConflictStrategy.REPLACE)
            void insert(... );
            
            @Delete
            void delete(... );
            
            @Update
            void Update(... );
            
            @Query("SELECT COUNT(id) FROM ")
            int count();
      }
*/