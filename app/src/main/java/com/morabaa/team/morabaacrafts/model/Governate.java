package com.morabaa.team.morabaacrafts.model;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.morabaa.team.morabaacrafts.interfaces.ApiModel;
import com.morabaa.team.morabaacrafts.interfaces.OnCompleteListener;
import com.morabaa.team.morabaacrafts.utils.RoomDB;
import com.morabaa.team.morabaacrafts.utils.HttpRequest;
import com.morabaa.team.morabaacrafts.utils.Utils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by eagle on 3/10/2018.
 */
@Entity
public class Governate implements ApiModel<Governate> {

    //      regio n request s
    private static final String CLASS_ROOT = Utils.API_ROOT + "Governate/";
    //region properties
    @PrimaryKey
    private long id;
    //endregion
    private String governate;

    public Governate() {
    }

    private static Type getClassType() {
        return new TypeToken<List<Governate>>() {
        }.getType();
    }


    @Override
    public void getAll(String tag, final OnCompleteListener onCompleteListener) {
        HttpRequest.getInstance()
                .addRequest(CLASS_ROOT + "getAll",
                        "",
                        getClassType(), tag, onCompleteListener);
    }

    @Override
    public void push(String tag, final OnCompleteListener<Governate> onCompleteListener) {
        HttpRequest.getInstance()
                .addRequest(CLASS_ROOT + "push",
                        new GsonBuilder()
                                .serializeSpecialFloatingPointValues()
                                .create().toJson(this, this.getClass()),
                        getClassType(), tag, onCompleteListener);
    }

    public void add(final OnCompleteListener<Governate> onCompleteListener) {
        GovernateDuo governateDuo = RoomDB.getInstance().governateDuo();
        List<Long> id = governateDuo.insert(this);
        if (id.get(0) > 0) {
            Governate governate = governateDuo.getGovernateById(id.get(0));
            List<Governate> governates = new ArrayList<>();
            governates.add(governate);
            onCompleteListener.onComplete(governates);
        } else {
            onCompleteListener.onError("GOVERNATE DID NOT ADDED");
        }
    }

    @Override
    public void update(String tag, final OnCompleteListener<Governate> onCompleteListener) {
        HttpRequest.getInstance()
                .addRequest(CLASS_ROOT + "update",
                        new GsonBuilder()
                                .serializeSpecialFloatingPointValues()
                                .create().toJson(this, this.getClass()),
                        getClassType(), tag, onCompleteListener);
    }

    @Override
    public void remove(String tag, final OnCompleteListener<Governate> onCompleteListener) {
        HttpRequest.getInstance()
                .addRequest(CLASS_ROOT + "remove",
                        new GsonBuilder()
                                .serializeSpecialFloatingPointValues()
                                .create().toJson(this, this.getClass()),
                        getClassType(), tag,
                        onCompleteListener);
    }

    public void remove(final OnCompleteListener<Governate> onCompleteListener) {
        GovernateDuo governateDuo = RoomDB.getInstance().governateDuo();
        int c = governateDuo.delete();
        if (c > 0) {
            List<Governate> governates = new ArrayList<>();
            governates.add(this);
            onCompleteListener.onComplete(governates);
        } else {
            onCompleteListener.onError("delete failed");
        }
    }

    public void push(String tag) {
        HttpRequest.getInstance()
                .addRequest(CLASS_ROOT + "push",
                        new GsonBuilder()
                                .serializeSpecialFloatingPointValues()
                                .create().toJson(this, this.getClass()),
                        getClassType(), tag, new OnCompleteListener<Governate>() {
                            @Override
                            public void onComplete(List<Governate> governates) {

                            }

                            @Override
                            public void onError(String error) {

                            }
                        });
    }
    //endregion

    //region getters and setters
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getGovernate() {
        return governate;
    }

    public void setGovernate(String governate) {
        this.governate = governate;
    }

    //endregion

    @Dao
    public interface GovernateDuo {

        @Query("SELECT * FROM Governate")
        List<Governate> GOVERNATES();

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        List<Long> insert(Governate... governates);

        @Delete
        int delete(Governate... governates);

        @Update
        void Update(Governate... governates);

        @Query("SELECT COUNT(id) FROM Governate")
        int count();

        @Query("SELECT * FROM Governate WHERE id =:id")
        Governate getGovernateById(long id);
    }

}
