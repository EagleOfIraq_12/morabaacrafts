package com.morabaa.team.morabaacrafts.adapters;


import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build.VERSION_CODES;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.morabaa.team.morabaacrafts.R;
import com.morabaa.team.morabaacrafts.model.Work;
import com.morabaa.team.morabaacrafts.utils.AWSS3Storage;
import com.morabaa.team.morabaacrafts.utils.Utils;

import java.util.List;

/**
 * Created by eagle on 10/21/2017.
 */

public class WorksAdapter extends RecyclerView.Adapter<WorkHolder> {

    private List<Work> works;
    private Context ctx;
    private OnWorkClickedListener onWorkClickedListener;
    private int layout;

    public WorksAdapter(Context ctx, List<Work> works, int layout) {
        this.works = works;
        this.ctx = ctx;
        this.layout = layout;
    }

    @Override
    public WorkHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CardView view = (CardView) LayoutInflater.from(parent.getContext()).inflate(
                layout, parent, false);
        return new WorkHolder(view);
    }

    @RequiresApi(api = VERSION_CODES.LOLLIPOP)
    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final WorkHolder workHolder,
                                 @SuppressLint("RecyclerView") final int position) {

        workHolder.txtWorkTitle.setText(works.get(position).getTitle() + "");

        workHolder.view.setOnClickListener(view ->
                onWorkClickedListener.onWorkClicked(
                        works.get(workHolder.getAdapterPosition())
                ));
        workHolder.txtWorkDate.setText(
                Utils.getRelativeTimeSpan(works.get(position).getTimeStamp())
        );

        try {
            String url = works.get(position).getMediaItems().get(0).getUrl();

            Glide.with(ctx)
                    .load(AWSS3Storage.getReference(url))
                    .thumbnail(0.1f)
                    .into(workHolder.imgWorkMedia);
        } catch (Exception ignored) {
        }


    }


    public void setOnWorkClickedListener(
            OnWorkClickedListener onWorkClickedListener) {
        this.onWorkClickedListener = onWorkClickedListener;
    }

    @Override
    public int getItemCount() {
        return works.size();
    }

    public List<Work> getWorks() {
        return works;
    }

    public void setWorks(List<Work> works) {
        this.works = works;
        notifyDataSetChanged();
    }

    public interface OnWorkClickedListener {

        void onWorkClicked(Work work);
    }
}


class WorkHolder extends RecyclerView.ViewHolder {

    CardView view;

    TextView txtWorkTitle;
    FrameLayout frameWorkMedia;
    ImageView imgWorkMedia;
    TextView txtWorkDate;

    WorkHolder(View itemView) {
        super(itemView);
        this.view = (CardView) itemView;

        txtWorkTitle = view.findViewById(R.id.txtWorkTitle);
        frameWorkMedia = view.findViewById(R.id.frameWorkMedia);
        imgWorkMedia = view.findViewById(R.id.imgWorkMedia);
        txtWorkDate = view.findViewById(R.id.txtWorkDate);
    }
}
