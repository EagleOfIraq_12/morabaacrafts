package com.morabaa.team.morabaacrafts.activities;

import android.os.Bundle;
import android.os.Handler;
import android.support.transition.Scene;
import android.support.transition.TransitionManager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.view.ViewGroup;
import com.morabaa.team.morabaacrafts.R;

public class TestActivity extends AppCompatActivity {
      
      Scene minScene;
      Scene maxScene;
      NestedScrollView scrollMain;
      int prev = 0;
      
      @Override
      protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            
            try {
                  setContentView(R.layout.activity_test);
            } catch (Exception e) {
                  e.printStackTrace();
            }
            
            ViewGroup mSceneRoot = findViewById(R.id.frameHeader);
            scrollMain = findViewById(R.id.scrollMain);
            minScene =
                  Scene.getSceneForLayout(mSceneRoot, R.layout.min_scene, this);
            maxScene =
                  Scene.getSceneForLayout(mSceneRoot, R.layout.max_scene, this);
            TransitionManager.go(maxScene);
            new Handler().postDelayed(new Runnable() {
                  @Override
                  public void run() {
                        TransitionManager.go(minScene);
                  }
            }, 2000);
//            scrollMain.setOnScrollChangeListener(new OnScrollChangeListener() {
//                  @Override
//                  public void onScrollChange(NestedScrollView v, int scrollX, int scrollY,
//                        int oldScrollX,
//                        int oldScrollY) {
//
//                           Toast.makeText(TestActivity.this
//                                 , ""+scrollY,
//                                 Toast.LENGTH_SHORT).show();
//                        if (scrollMain.getChildAt(0).getBottom() ==
//                              (scrollMain.getHeight() + scrollMain.getScrollY())) {
////                            bottomProgressBar.setVisibility(View.VISIBLE);
//                              new CountDownTimer(5000, 1) {
//
//                                    @Override
//                                    public void onTick(long millisUntilFinished) {
//                                          // do something after 1s
//                                    }
//
//                                    @Override
//                                    public void onFinish() {
////                                    bottomProgressBar.setVisibility(View.GONE);
//                                    }
//
//                              }.start();
//                        }
//
//                        int now = scrollMain.getScrollY();
//
//                        if (prev > now) {
//                              scrollMain.setBackgroundColor(Color.BLUE);
//                        } else {
//                              scrollMain.setVisibility(View.GONE);
//                        }
//                        prev = now;
//                  }
//            });
            
      }
}