package com.morabaa.team.morabaacrafts.bottomSheets;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.GsonBuilder;
import com.morabaa.team.morabaacrafts.R;
import com.morabaa.team.morabaacrafts.interfaces.OnCompleteListener;
import com.morabaa.team.morabaacrafts.model.Request;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

public class AcceptRequestBottomSheet extends BottomSheetDialogFragment {
  private Request request;
  private OnDoneListener onDoneListener;

  public void setOnDoneListener(OnDoneListener onDoneListener) {
    this.onDoneListener = onDoneListener;
  }

  @Nullable
  @Override
  public View onCreateView(
      @NonNull LayoutInflater inflater,
      @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    assert getArguments() != null;
    String requestString = getArguments().getString("requestString");
    request =
        new GsonBuilder()
            .serializeSpecialFloatingPointValues()
            .create()
            .fromJson(requestString, Request.class);
    View view = inflater.inflate(R.layout.dialog_accept_request_layout, container, false);
    view.findViewById(R.id.frameDialog).setBackgroundColor(Color.argb(0, 0, 0, 0));
    Button btnOk = view.findViewById(R.id.btnOk);
    final EditText txtRequestNote = view.findViewById(R.id.txtRequestReceiverNote);
    final EditText txtYear = view.findViewById(R.id.txtYear);
    final EditText txtMonth = view.findViewById(R.id.txtMonth);
    final EditText txtDay = view.findViewById(R.id.txtDay);
    final EditText txtPrice = view.findViewById(R.id.txtPrice);
    final Spinner spinnerUnit = view.findViewById(R.id.spinnerUnit);

    final ProgressBar progressBarUpdating = view.findViewById(R.id.progressBarUpdating);

    btnOk.setOnClickListener(
        view1 -> {
          int day = 0, month = 0, year = 0;
          try {
            year = Integer.parseInt(txtYear.getText().toString());
            month = Integer.parseInt(txtMonth.getText().toString());
            day = Integer.parseInt(txtDay.getText().toString());
          } catch (NumberFormatException ignored) {
          }
          Calendar todayCalendar = new GregorianCalendar();
          todayCalendar.set(year + 0, month - 1, day + 0);

          long date = todayCalendar.getTimeInMillis(); // new Date(year, month, day).getTime();

          if (date < new GregorianCalendar().getTimeInMillis()) {
            Toast.makeText(getContext(), "الرجاء ادخال تاريخ صالح", Toast.LENGTH_SHORT).show();
            return;
          }
          request.setUnit(spinnerUnit.getSelectedItem().toString());
          if (txtRequestNote.getText().toString().trim().length() < 1) {
            Toast.makeText(getContext(), "الرجاء قم بادخال ملاحظه", Toast.LENGTH_SHORT).show();
            txtRequestNote.requestFocus();
            return;
          }
          request.setReceiverNote(txtRequestNote.getText().toString());
          request.setWorkDate(date);
          if (txtPrice.getText().toString().trim().length() < 1) {
            Toast.makeText(getContext(), "الرجاء قم بادخال السعر", Toast.LENGTH_SHORT).show();
            txtPrice.requestFocus();
            return;
          }
          request.setPrice(Float.parseFloat(txtPrice.getText().toString()));
          progressBarUpdating.setVisibility(View.VISIBLE);
          request.setStatus(Request.STATUS.ACCEPTED);
          request.update(
              "",
              new OnCompleteListener<Request>() {
                @Override
                public void onComplete(List<Request> requests) {
                  System.out.println("requests = [" + requests.get(0).getStatus() + "]");
                  progressBarUpdating.setVisibility(View.GONE);
                  onDoneListener.onDone(true);
                  dismiss();
                }

                @Override
                public void onError(String error) {
                  onDoneListener.onDone(false);
                  dismiss();
                }
              });
        });

    return view;
  }
}
