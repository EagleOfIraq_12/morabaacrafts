package com.morabaa.team.morabaacrafts.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.morabaa.team.morabaacrafts.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class FavoritesContainerFragment extends Fragment {


    public FavoritesContainerFragment() {
        // Required empty public constructor
    }
    public static FavoritesContainerFragment newInstance() {
        FavoritesContainerFragment fragment = new FavoritesContainerFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_favorites_container, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        getChildFragmentManager()
                .beginTransaction()
                .add(
                        R.id.frameFavoritesContainer, FavoritesFragment.newInstance()
                )
                .addToBackStack(null)
                .commit();
    }

}
