package com.morabaa.team.morabaacrafts.model;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.morabaa.team.morabaacrafts.interfaces.ApiModel;
import com.morabaa.team.morabaacrafts.interfaces.OnCompleteListener;
import com.morabaa.team.morabaacrafts.utils.HttpRequest;
import com.morabaa.team.morabaacrafts.utils.Utils;

import java.lang.reflect.Type;
import java.util.List;

@Entity
public class Request implements ApiModel<Request> {

  private static final String CLASS_ROOT = Utils.API_ROOT + "Request/";

  // region properties
  @PrimaryKey private long id;
  private long workDate;
  private int status;
  private float price;
  private String unit = UNITS.IQD;
  @Ignore private List<Rate> rates;
  @Ignore private Person sender;
  @Ignore private Person receiver;
  private long creationDate;
  private String address;
  private String senderNote;
  private String receiverNote;
  @Ignore private Work work;
  private boolean posted;
  private boolean receiverRated;
  private boolean workRated;
  // endregion

  public Request() {}

  private static Type getClassType(int... parms) {
    if (parms.length > 0) {
      return Request.class;
    }
    return new TypeToken<List<Request>>() {}.getType();
  }

  // region getters and setters
  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public long getWorkDate() {
    return workDate;
  }

  public void setWorkDate(long workDate) {
    this.workDate = workDate;
  }

  public int isStatus() {
    return status;
  }

  public float getPrice() {
    return price;
  }

  public void setPrice(float price) {
    this.price = price;
  }

  public List<Rate> getRates() {
    return rates;
  }

  public void setRates(List<Rate> rates) {
    this.rates = rates;
  }

  public Person getSender() {
    return sender;
  }

  public void setSender(Person sender) {
    this.sender = sender;
  }

  public Person getReceiver() {
    return receiver;
  }

  public void setReceiver(Person receiver) {
    this.receiver = receiver;
  }

  public long getCreationDate() {
    return creationDate;
  }

  public void setCreationDate(long creationDate) {
    this.creationDate = creationDate;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getSenderNote() {
    return senderNote;
  }

  public void setSenderNote(String senderNote) {
    this.senderNote = senderNote;
  }

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  public String getReceiverNote() {
    return receiverNote;
  }

  public void setReceiverNote(String receiverNote) {
    this.receiverNote = receiverNote;
  }

  public Work getWork() {
    return work;
  }

  public void setWork(Work work) {
    this.work = work;
  }

  public String getUnit() {
    return unit;
  }

  public void setUnit(String unit) {
    this.unit = unit;
  }

  public boolean isPosted() {
    return posted;
  }

  public void setPosted(boolean posted) {
    this.posted = posted;
  }

  public boolean isReceiverRated() {
    return receiverRated;
  }

  public void setReceiverRated(boolean receiverRated) {
    this.receiverRated = receiverRated;
  }

  public boolean isWorkRated() {
    return workRated;
  }

  public void setWorkRated(boolean workRated) {
    this.workRated = workRated;
  }

  @Override
  public void getAll(String tag, final OnCompleteListener<Request> onCompleteListener) {
    HttpRequest.getInstance()
        .addRequest(CLASS_ROOT + "getAll", "", getClassType(), tag, onCompleteListener);
  }

  public void getAllByStatus(String tag, final OnCompleteListener<Request> onCompleteListener) {
    HttpRequest.getInstance()
        .addRequest(
            CLASS_ROOT + "getAllByStatus",
            new GsonBuilder()
                .serializeSpecialFloatingPointValues()
                .create()
                .toJson(this, getClassType(0)),
            getClassType(),
            tag,
            onCompleteListener);
  }

  public void getSent(String tag, OnCompleteListener<Request> onCompleteListener) {
    HttpRequest.getInstance()
        .addRequest(
            CLASS_ROOT + "getSent",
            new GsonBuilder()
                .serializeSpecialFloatingPointValues()
                .create()
                .toJson(this, getClassType(0)),
            getClassType(),
            tag,
            onCompleteListener);
  }

  public void getReceived(String tag, OnCompleteListener<Request> onCompleteListener) {
    HttpRequest.getInstance()
        .addRequest(
            CLASS_ROOT + "getReceived",
            new GsonBuilder()
                .serializeSpecialFloatingPointValues()
                .create()
                .toJson(this, getClassType(0)),
            getClassType(),
            tag,
            onCompleteListener);
  }

  @Override
  public void push(String tag, OnCompleteListener<Request> onCompleteListener) {
    HttpRequest.getInstance()
        .addRequest(
            CLASS_ROOT + "push",
            new GsonBuilder()
                .serializeSpecialFloatingPointValues()
                .create()
                .toJson(this, getClassType(0)),
            getClassType(),
            tag,
            onCompleteListener);
  }

  @Override
  public void update(String tag, OnCompleteListener<Request> onCompleteListener) {
    HttpRequest.getInstance()
        .addRequest(
            CLASS_ROOT + "update",
            new GsonBuilder()
                .serializeSpecialFloatingPointValues()
                .create()
                .toJson(this, getClassType(0)),
            getClassType(),
            tag,
            onCompleteListener);
  }

  @Override
  public void remove(String tag, OnCompleteListener<Request> onCompleteListener) {}

  public void getOneByStatus(String tag, OnCompleteListener<Request> onCompleteListener) {
    HttpRequest.getInstance()
        .addRequest(
            CLASS_ROOT + "getOneByStatus",
            new GsonBuilder()
                .serializeSpecialFloatingPointValues()
                .create()
                .toJson(this, getClassType(0)),
            getClassType(),
            tag,
            onCompleteListener);
  }
  // endregion

  @Dao
  public interface RequestDuo {

    @Query("SELECT * FROM Request")
    List<Request> REQUESTS();

    @Query("SELECT * FROM Request WHERE id=:id")
    Request getRequestById(long id);

    @Query("UPDATE Request SET status=:status WHERE id=:id")
    long update(long id, int status);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Request... requests);

    @Delete
    void delete(Request... requests);

    @Update
    void Update(Request... requests);

    @Query("SELECT COUNT(id) FROM Request")
    int count();

    @Query("SELECT MAX(id) FROM Request")
    long lastId();
  }

  public static final class UNITS {
    public static final String IQD = "د.ع";
    public static final String USD = "$";
  }

  public static final class TYPE {
    public static final int SENT = 1;
    public static final int RECEIVED = 2;
  }

  public static final class STATUS {
    public static final int INIT = 0;
    public static final int ACCEPTED = 1;
    public static final int REJECTED = 2;
    public static final int DEAL = 3;
    public static final int ABORTED = 4;
    public static final int DONE = 5;
    public static final int ALL = 6;
  }
}
