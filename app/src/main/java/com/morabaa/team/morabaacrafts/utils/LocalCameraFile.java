package com.morabaa.team.morabaacrafts.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.morabaa.team.morabaacrafts.CameraLActivity;

public class LocalCameraFile {

    private static LocalCameraFile instance;
    private OnFileReceivedListener onFileReceivedListener;

    public LocalCameraFile(Context ctx, int cameraAction) {
        Intent intent = new Intent(ctx, CameraLActivity.class);
        intent.putExtra("reqCode", 88);
        intent.putExtra("cameraAction", cameraAction);
        ctx.startActivity(intent);
        instance = this;
    }

    public static LocalCameraFile getInstance() {
        return instance;
    }

    public static void release() {
        CameraLActivity.getInstance().finish();
        instance = null;
    }

    public static LocalCameraFile newInstance(Context ctx, int cameraAction) {
        return new LocalCameraFile(ctx, cameraAction);
    }

    public static LocalCameraFile captureImage(Context ctx) {
        return new LocalCameraFile(ctx, CAMERA_ACTION.CAPTURE_IMAGE);
    }

    public static LocalCameraFile recordVideo(Context ctx) {
        return new LocalCameraFile(ctx, CAMERA_ACTION.RECORD_VIDEO);
    }


    public OnFileReceivedListener getOnFileReceivedListener() {
        return onFileReceivedListener;
    }

    public LocalCameraFile setOnFileReceivedListener(
            OnFileReceivedListener onFileReceivedListener) {
        this.onFileReceivedListener = onFileReceivedListener;
        return this;
    }

    public interface OnFileReceivedListener {

        void onFileReceived(Uri uri);

        void onFileError(String error);
    }

    public static final class CAMERA_ACTION {

        public static final int CAPTURE_IMAGE = 1;
        public static final int RECORD_VIDEO = 2;
    }
}
