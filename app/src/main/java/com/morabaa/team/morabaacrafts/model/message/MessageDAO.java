package com.morabaa.team.morabaacrafts.model.message;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface MessageDAO {

  @Query("SELECT * FROM Message")
  List<Message> MESSAGES();

  @Query("SELECT * FROM Message WHERE id=:id")
  Message getMessageById(long id);

  @Insert(onConflict = OnConflictStrategy.REPLACE)
  void insert(Message... messages);

  @Delete
  void delete(Message... messages);

  @Update
  void Update(Message... messages);

  @Query("SELECT COUNT(id) FROM Message")
  int count();

  @Query("SELECT MAX(id) FROM Message")
  long lastId();
}
