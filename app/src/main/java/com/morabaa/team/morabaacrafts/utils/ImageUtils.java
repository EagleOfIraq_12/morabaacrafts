package com.morabaa.team.morabaacrafts.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;

import com.morabaa.team.morabaacrafts.App;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class ImageUtils {


    public static String saveLocalFile(Context ctx, Uri uri) {
        File file = new File(
                /*root Dir*/ctx.getFilesDir(),
                /*file name*/uri.getLastPathSegment());
        InputStream in;
        OutputStream out;
        try {
            in = ctx.getContentResolver().openInputStream(uri);
            out = new FileOutputStream(file);
            byte[] buf = new byte[1024];
            int len;
            assert in != null;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            out.close();
            in.close();
        } catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
            return null;
        }
        return file.getPath();
    }

    public static Uri getUriFromPath(String path) {
        return Uri.fromFile(new File(path));
    }

    public static Bitmap getBitmabFromBytes(byte[] bytes) {
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
    }

    public static File saveTempBitmap(Bitmap bitmap) {
        File f3 = new File(Environment.getExternalStorageDirectory() + "/inpaint/");
        if (!f3.exists())
            f3.mkdirs();
        OutputStream outStream = null;
        File file = new File(Environment.getExternalStorageDirectory() + "/inpaint/" + System.currentTimeMillis() + ".png");
        try {
            outStream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 85, outStream);
            outStream.close();
            return file;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String compress(String path, long maxSize) {
        File img = new File(path);
        double oSize = img.length() / 8 + 0.0D;

        BitmapFactory.Options options = new BitmapFactory.Options();
        Bitmap bitmap = null;
        options.inSampleSize = 1;
        while (img.length() / 8 > maxSize) {
            options.inSampleSize = options.inSampleSize + 1;
            bitmap = BitmapFactory.decodeFile(path, options);
            img.delete();
            try {
                FileOutputStream fos = new FileOutputStream(path);
                bitmap.compress(path.toLowerCase().endsWith("png") ?
                        Bitmap.CompressFormat.PNG :
                        Bitmap.CompressFormat.JPEG, 100, fos);
                fos.close();
            } catch (Exception errVar) {
                errVar.printStackTrace();
            }
        }
        double nSize = img.length() / 8 + 0.0D;

        System.out.println(
                "nSize = " + nSize + " \t " + "oSize = " + oSize + " \t compressionRatio \t" +
                        nSize / oSize * 100 + " %"
        );
        return path;
    }

    public static String compressMini(String path, long maxSize) {
        File img = new File(path);
        double oSize = img.length() / 8 + 0.0D;
        try {

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 1;
            Bitmap bitmap = BitmapFactory.decodeFile(path, options);
            Bitmap bitmapOut = Bitmap.createScaledBitmap(bitmap, 50, 50, true);
            FileOutputStream outStream = new FileOutputStream(new File(App.getInstance().getCacheDir(), "tempBMP"));
            bitmapOut.compress(Bitmap.CompressFormat.JPEG, 75, outStream);

            outStream.close();

        } catch (IOException e) {
        }

        double nSize = img.length() / 8 + 0.0D;

        System.out.println(
                "nSize = " + nSize + " \t " + "oSize = " + oSize + " \t compressionRatio \t" +
                        nSize / oSize * 100 + " %"
        );
        return path;
    }

}