package com.morabaa.team.morabaacrafts.utils;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.webkit.MimeTypeMap;

import com.morabaa.team.morabaacrafts.utils.LocalCameraFileX.CAMERA_ACTION;

import java.io.File;

public class LocalCameraFileActivity extends Activity {

    private static LocalCameraFileActivity instance;
    private int reqCode;
    private Uri uriFilePath;

    public static LocalCameraFileActivity getInstance() {
        return instance;
    }

    public static void setInstance(LocalCameraFileActivity instance) {
        LocalCameraFileActivity.instance = instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.reqCode = getIntent().getIntExtra("reqCode", 0);
        int cameraAction = getIntent().getIntExtra("cameraAction", 1);

        setInstance(this);
        PackageManager packageManager = getPackageManager();
        if (packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            File mainDirectory = new File(
                    Environment.getExternalStorageDirectory(), "MyFolder/tmp");
            if (!mainDirectory.exists()) {
                mainDirectory.mkdirs();
            }

            Intent intent;
            if (cameraAction == CAMERA_ACTION.CAPTURE_IMAGE) {
                uriFilePath = Uri.fromFile(
                        new File(mainDirectory, "IMG_" + System.currentTimeMillis() +
                                ".jpeg"));
                intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            } else {
                uriFilePath = Uri.fromFile(
                        new File(mainDirectory, "VID_" + System.currentTimeMillis() + ".mp4"));
                intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
            }
            intent.putExtra(MediaStore.EXTRA_OUTPUT, uriFilePath);
            startActivityForResult(intent, reqCode);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
//            if (uriFilePath != null) {
//                  outState.putString("uri_file_path", uriFilePath.toString());
//            }
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == reqCode && resultCode == RESULT_OK) {
            LocalCameraFile.getInstance()
                    .getOnFileReceivedListener()
                    .onFileReceived(uriFilePath);
        } else {
            LocalCameraFile.getInstance()
                    .getOnFileReceivedListener()
                    .onFileError("Error: no file");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = LocalCameraFileActivity.this.getContentResolver()
                .query(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        int columnIndex = cursor.getColumnIndex(projection[0]);
        String filePath = cursor.getString(columnIndex);
        String path = cursor.getString(column_index);
        cursor.close();
        Bitmap yourSelectedImage = BitmapFactory.decodeFile(filePath);
        File file = new File(filePath);
        return path;
    }

    public String getName(Uri uri) {
        ///external/images/media/7131 <- uri.getEncodedPath()
        String path = uri.getEncodedPath();
        String[] parts = path.split("/");
        return parts[parts.length - 1];
    }

    public String getExtension(Uri uri) {
        String extension;

        //Check uri format to avoid null
        if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            //If scheme is a content
            final MimeTypeMap mime = MimeTypeMap.getSingleton();
            extension = mime
                    .getExtensionFromMimeType(
                            LocalCameraFileActivity.this.getContentResolver().getType(uri));
        } else {
            //If scheme is a File
            //This will replace white spaces with %20 and also other special characters. This will avoid returning null values on file name with spaces and special characters.
            extension = MimeTypeMap
                    .getFileExtensionFromUrl(Uri.fromFile(new File(uri.getPath())).toString());

        }

        return extension;
    }
}
