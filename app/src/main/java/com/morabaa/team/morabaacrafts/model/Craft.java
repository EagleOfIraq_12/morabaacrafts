package com.morabaa.team.morabaacrafts.model;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.morabaa.team.morabaacrafts.interfaces.ApiModel;
import com.morabaa.team.morabaacrafts.interfaces.OnCompleteListener;
import com.morabaa.team.morabaacrafts.utils.HttpRequest;
import com.morabaa.team.morabaacrafts.utils.Utils;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.List;

@Entity
public class Craft implements ApiModel<Craft>, Serializable {

    private static final String CLASS_ROOT = Utils.API_ROOT + "Craft/";

    //region properties
    @PrimaryKey
    private long id;
    private String name;
    @Ignore
    private MediaItem mediaItem;
    //endregion

    public Craft() {
    }

    private static Type getClassType() {
        return new TypeToken<List<Craft>>() {
        }.getType();
    }

    //region getters and setters
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MediaItem getMediaItem() {
        return mediaItem;
    }

    public void setMediaItem(MediaItem mediaItem) {
        this.mediaItem = mediaItem;
    }

    @Override
    public void getAll(String tag, OnCompleteListener<Craft> onCompleteListener) {
        Person person = Person.getCurrentPerson();
        person.setName(this.getName() != null ?
                this.getName() : "");

        HttpRequest.getInstance()
                .addRequest(CLASS_ROOT + "getAll",
                        new GsonBuilder()
                                .serializeSpecialFloatingPointValues()
                                .create()
                                .toJson(person, Person.class),
                        getClassType(), tag, onCompleteListener);
    }

    public void getAllForAdmin(String tag, OnCompleteListener<Craft> onCompleteListener) {
        Person person = new Person();
        Governate governate = new Governate();
        governate.setId(1);
        person.setGovernate(governate);
        person.setName("");

        HttpRequest.getInstance()
                .addRequest(CLASS_ROOT + "getAll",
                        new GsonBuilder()
                                .serializeSpecialFloatingPointValues()
                                .create()
                                .toJson(person, Person.class),
                        getClassType(), tag, onCompleteListener);
    }

    public void getList(String tag, OnCompleteListener<Craft> onCompleteListener) {
        HttpRequest.getInstance()
                .addRequest(CLASS_ROOT + "getList",
                        new GsonBuilder()
                                .serializeSpecialFloatingPointValues()
                                .create().toJson(Person.getCurrentPerson(), Person.class),
                        getClassType(), tag, onCompleteListener);
    }

    public static void getOne(String tag, OnCompleteListener<Craft> onCompleteListener) {
        HttpRequest.getInstance()
                .addRequest(CLASS_ROOT + "getOne",
                        new GsonBuilder()
                                .serializeSpecialFloatingPointValues()
                                .create().toJson(Person.getCurrentPerson(), Person.class), Craft.class, tag, onCompleteListener);
    }

    public static void getInt(String tag, OnCompleteListener<Integer> onCompleteListener) {
        HttpRequest.getInstance()
                .addRequest(CLASS_ROOT + "getInt",
                        new GsonBuilder()
                                .serializeSpecialFloatingPointValues()
                                .create().toJson(Person.getCurrentPerson(), Person.class), Integer.class, tag, onCompleteListener);
    }

    public static void getString(String tag, OnCompleteListener<String> onCompleteListener) {
        HttpRequest.getInstance()
                .addRequest(CLASS_ROOT + "getString",
                        new GsonBuilder()
                                .serializeSpecialFloatingPointValues()
                                .create().toJson(Person.getCurrentPerson(), Person.class), String.class, tag, onCompleteListener);
    }

    public static void getBool(String tag, OnCompleteListener<Boolean> onCompleteListener) {
        HttpRequest.getInstance()
                .addRequest(CLASS_ROOT + "getBool",
                        new GsonBuilder()
                                .serializeSpecialFloatingPointValues()
                                .create().toJson(Person.getCurrentPerson(), Person.class), Boolean.class, tag, onCompleteListener);
    }


    @Override
    public void push(String tag, OnCompleteListener<Craft> onCompleteListener) {
        HttpRequest.getInstance()
                .addRequest(CLASS_ROOT + "push",
                        new GsonBuilder()
                                .serializeSpecialFloatingPointValues()
                                .create()
                                .toJson(this, Craft.class),
                        getClassType(), tag, onCompleteListener);
    }

    @Override
    public void update(String tag, OnCompleteListener<Craft> onCompleteListener) {
        HttpRequest.getInstance()
                .addRequest(CLASS_ROOT + "update",
                        new GsonBuilder()
                                .serializeSpecialFloatingPointValues()
                                .create().toJson(this, Craft.class),
                        getClassType(), tag, onCompleteListener);
    }

    @Override
    public void remove(String tag, OnCompleteListener<Craft> onCompleteListener) {
        HttpRequest.getInstance()
                .addRequest(CLASS_ROOT + "remove",
                        new GsonBuilder()
                                .serializeSpecialFloatingPointValues()
                                .create().toJson(this, Craft.class),
                        getClassType(), tag, onCompleteListener);
    }

    //endregion


    @Dao
    public interface CraftDuo {

        @Query("SELECT * FROM Craft"/*" WHERE id in :ids"*/)
        List<Craft> CRAFTS(/*List<Long> ids*/);

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        void insert(Craft... crafts);

        @Delete
        void delete(Craft... crafts);

        @Update
        void Update(Craft... crafts);

        @Query("SELECT COUNT(id) FROM Craft")
        int count();

        @Query("DELETE FROM Craft")
        void deleteAll();
    }
}
