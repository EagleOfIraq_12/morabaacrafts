package com.morabaa.team.morabaacrafts.bottomSheets;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.google.gson.GsonBuilder;
import com.morabaa.team.morabaacrafts.R;
import com.morabaa.team.morabaacrafts.interfaces.OnCompleteListener;
import com.morabaa.team.morabaacrafts.model.Person;
import com.morabaa.team.morabaacrafts.model.Rate;
import com.morabaa.team.morabaacrafts.model.Request;
import com.willy.ratingbar.ScaleRatingBar;

import java.util.List;

public class RateReceiverBottomSheet extends BottomSheetDialogFragment {
  private Request request;
  private OnDoneListener onDoneListener;

  public void setOnDoneListener(OnDoneListener onDoneListener) {
    this.onDoneListener = onDoneListener;
  }

  @Nullable
  @Override
  public View onCreateView(
      @NonNull LayoutInflater inflater,
      @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    assert getArguments() != null;
    String requestString = getArguments().getString("requestString");
    request =
        new GsonBuilder()
            .serializeSpecialFloatingPointValues()
            .create()
            .fromJson(requestString, Request.class);

    View view = inflater.inflate(R.layout.dialog_rate_craft_man_request_layout, container, false);

    view.findViewById(R.id.frameDialog).setBackgroundColor(Color.argb(0, 0, 0, 0));
    Button btnOk = view.findViewById(R.id.btnOk);
    final EditText txtRequestCraftManRate = view.findViewById(R.id.txtRequestCraftManRate);
    final ScaleRatingBar ratingCraftManRatingSpeed =
        view.findViewById(R.id.ratingCraftManRatingSpeed);
    final ScaleRatingBar ratingCraftManRatingDate =
        view.findViewById(R.id.ratingCraftManRatingDate);
    final ScaleRatingBar ratingCraftManRatingAccuracy =
        view.findViewById(R.id.ratingCraftManRatingAccuracy);
    final ProgressBar progressBarUpdating = view.findViewById(R.id.progressBarUpdating);
    btnOk.setOnClickListener(
        view1 -> {
          progressBarUpdating.setVisibility(View.VISIBLE);
          Rate rate = new Rate();
          rate.setComment(txtRequestCraftManRate.getText().toString());
          rate.setRatingAccuracy(ratingCraftManRatingAccuracy.getRating());
          rate.setRatingDate(ratingCraftManRatingDate.getRating());
          rate.setRatingSpeed(ratingCraftManRatingSpeed.getRating());
          rate.setRated(request.getReceiver());
          rate.setRater(Person.getCurrentPerson());
          rate.push(
              "",
              request.getId(),
              new OnCompleteListener<Rate>() {
                @Override
                public void onComplete(List<Rate> rates) {
                  progressBarUpdating.setVisibility(View.GONE);
                  onDoneListener.onDone(true);
                  dismiss();
                }

                @Override
                public void onError(String error) {
                  progressBarUpdating.setVisibility(View.GONE);
                  onDoneListener.onDone(false);
                  dismiss();
                }
              });
        });

    return view;
  }
}
