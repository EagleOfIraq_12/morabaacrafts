package com.morabaa.team.morabaacrafts.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;

import com.morabaa.team.morabaacrafts.R;
import com.morabaa.team.morabaacrafts.adapters.CraftsAdapter;
import com.morabaa.team.morabaacrafts.interfaces.OnCompleteListener;
import com.morabaa.team.morabaacrafts.model.Craft;

import java.util.ArrayList;
import java.util.List;

public class CraftsFragment extends Fragment {

    ProgressBar bottomProgressBar;
    private CraftsAdapter craftsAdapter;
    private RecyclerView craftsRecyclerView;
    private SearchView txtSearchCrafts;
    private TextView txtNoItems;

    public CraftsFragment() {
        // Required empty public constructor
    }

    public static CraftsFragment newInstance(Bundle args) {
        CraftsFragment fragment = new CraftsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            // TODO get parms
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_crafts, container, false);
    }

    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        txtSearchCrafts = view.findViewById(R.id.txtSearchCrafts);
        bottomProgressBar = view.findViewById(R.id.bottomProgressBar);
        craftsRecyclerView = view.findViewById(R.id.craftsRecyclerView);
        txtNoItems = view.findViewById(R.id.txtNoItems);

        craftsAdapter = new CraftsAdapter(getContext(), new ArrayList<Craft>());
        craftsAdapter.setOnCraftClickedListener(craft ->
                getParentFragment().getChildFragmentManager()
                        .beginTransaction()
                        .add(
                                R.id.frameMainContainer,
                                CraftMenFragment.newInstance(craft.getId())
                        )
                        .addToBackStack(null)
                        .commit());
        craftsRecyclerView.setLayoutManager(
                new GridLayoutManager(getContext(), 3, GridLayoutManager.VERTICAL, false));
        craftsRecyclerView.setAdapter(craftsAdapter);

        new Craft().getAll(this.getClass().getName(),
                new OnCompleteListener<Craft>() {
                    @Override
                    public void onComplete(List<Craft> crafts) {
                        txtNoItems.setVisibility(
                                crafts.size() > 0 ?
                                        View.GONE : View.VISIBLE
                        );
                        craftsAdapter.setCrafts(crafts);
                        bottomProgressBar.setVisibility(View.GONE);
//                        RoomDB.getInstance()
//                                .craftDuo().deleteAll();
//                        RoomDB.getInstance()
//                                .craftDuo().insert(crafts.toArray(new Craft[crafts.size()]));
                    }

                    @Override
                    public void onError(String error) {
                        txtNoItems.setVisibility(View.VISIBLE);
//                        List<Craft> crafts = RoomDB.getInstance()
//                                .craftDuo().CRAFTS();
//                        craftsAdapter.setCrafts(crafts);
                        bottomProgressBar.setVisibility(View.GONE);
                    }
                });
        txtSearchCrafts.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                craftsAdapter.clear();
                bottomProgressBar.setVisibility(View.VISIBLE);
                Craft craft = new Craft();
                craft.setName(query);
                craft.getAll(this.getClass().getName(),
                        new OnCompleteListener<Craft>() {
                            @Override
                            public void onComplete(List<Craft> crafts) {
                                txtNoItems.setVisibility(
                                        crafts.size() > 0 ?
                                                View.GONE : View.VISIBLE
                                );
                                craftsAdapter.setCrafts(crafts);
                                bottomProgressBar.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError(String error) {
//                                List<Craft> crafts = RoomDB.getInstance()
//                                        .craftDuo().CRAFTS();
//                                craftsAdapter.setCrafts(crafts);
                                txtNoItems.setVisibility(View.VISIBLE);
                                bottomProgressBar.setVisibility(View.GONE);
                            }
                        });
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.equals("")) {
                    this.onQueryTextSubmit("");
                    txtSearchCrafts.setIconified(true);
                }
                return false;
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
