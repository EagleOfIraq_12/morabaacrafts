package com.morabaa.team.morabaacrafts.services;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.morabaa.team.morabaacrafts.R;
import com.morabaa.team.morabaacrafts.activities.MainActivity;


/**
 * Created by eagle on 12/30/2017.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    Vibrator vibrator;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        new Thread(() ->
        {
            try {
                RingtoneManager.getRingtone(
                        getApplicationContext(),
                        RingtoneManager.getDefaultUri(
                                RingtoneManager.TYPE_NOTIFICATION
                        )
                ).play();

                vibrator.vibrate(200);
                Thread.sleep(400);
                vibrator.vibrate(200);
                Thread.sleep(400);
                vibrator.vibrate(800);

            } catch (InterruptedException ignored) {
            }
        }).start();

        showNoti(remoteMessage);
    }

    public void showNoti(RemoteMessage remoteMessage) {

        String title = remoteMessage.getData().get("title");
        String body = remoteMessage.getData().get("body");
        String click_action = remoteMessage.getData().get("click_action");
        String parms = remoteMessage.getData().get("parms");

        try {
            Uri notification = RingtoneManager
                    .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
            r.play();
        } catch (Exception ignored) {
        }

        Intent intent = new Intent(click_action);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra("parms", parms);
        if (MainActivity.getInstance() != null) {
            MainActivity.getInstance().runOnUiThread(() ->
                    MainActivity.getInstance().initPager()
            );
        }
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.logo)
                        .setContentTitle(title)
                        .setContentText(body)
                        .setAutoCancel(true)
                        .setChannelId("req")
                        .setContentIntent(PendingIntent.getActivity(this,
                                0, intent,
                                PendingIntent.FLAG_CANCEL_CURRENT));

        NotificationManager notificationManager = (NotificationManager) getSystemService(
                Context.NOTIFICATION_SERVICE);
        assert notificationManager != null;
        notificationManager.notify("messageId",
                (int) (System.currentTimeMillis() % Integer.MAX_VALUE)
                , builder.build());
    }


}
