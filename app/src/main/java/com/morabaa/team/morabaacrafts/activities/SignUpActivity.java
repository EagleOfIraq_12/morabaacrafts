package com.morabaa.team.morabaacrafts.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.asksira.bsimagepicker.BSImagePicker;
import com.bumptech.glide.Glide;
import com.google.gson.GsonBuilder;
import com.morabaa.team.morabaacrafts.R;
import com.morabaa.team.morabaacrafts.interfaces.OnCompleteListener;
import com.morabaa.team.morabaacrafts.model.Craft;
import com.morabaa.team.morabaacrafts.model.Governate;
import com.morabaa.team.morabaacrafts.model.MediaItem;
import com.morabaa.team.morabaacrafts.model.Person;
import com.morabaa.team.morabaacrafts.utils.AWSS3Storage;
import com.morabaa.team.morabaacrafts.utils.RoomDB;
import com.morabaa.team.morabaacrafts.utils.LocalData;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import jp.wasabeef.glide.transformations.BlurTransformation;
import me.gujun.android.taggroup.TagGroup;

import static com.bumptech.glide.request.RequestOptions.bitmapTransform;

public class SignUpActivity extends AppCompatActivity
    implements BSImagePicker.OnSingleImageSelectedListener {

  private EditText txtFullName;
  private EditText txtAge;
  private EditText txtPhoneNumber;
  private Spinner spinnerCraft;
  private LinearLayout layoutCraft;
  private Spinner spinnerGovernate;
  private EditText txtAddress;
  private EditText txtContactViber;
  private EditText txtContactFacebook;
  private EditText txtContactWhatsapp;
  private Button btnSignUp;
  private Governate governate = new Governate();
  private Craft craft = new Craft();
  private ImageView imgPersonImage;
  private Person person = new Person();
  private TagGroup mTagGroup;
  private List<Craft> crafts = new ArrayList<>();
  private Person currentPerson;
  private CheckBox checkAcceptPolicy;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_sign_up);

    checkAcceptPolicy = findViewById(R.id.checkAcceptPolicy);
    imgPersonImage = findViewById(R.id.imgPersonImage);
    txtFullName = findViewById(R.id.txtFullName);
    txtAge = findViewById(R.id.txtAge);
    txtPhoneNumber = findViewById(R.id.txtPhoneNumber);
    spinnerCraft = findViewById(R.id.spinnerCraft);
    layoutCraft = findViewById(R.id.layoutCraft);
    spinnerGovernate = findViewById(R.id.spinnerGovernate);
    txtAddress = findViewById(R.id.txtAddress);
    txtContactViber = findViewById(R.id.txtContactViber);
    txtContactFacebook = findViewById(R.id.txtContactFacebook);
    txtContactWhatsapp = findViewById(R.id.txtContactWhatsapp);
    btnSignUp = findViewById(R.id.btnSignUp);
    mTagGroup = findViewById(R.id.tag_group);
    spinnerCraft.setOnItemSelectedListener(
        new AdapterView.OnItemSelectedListener() {
          @Override
          public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if (!isCanAdd) return;
            List<String> tags = new ArrayList<>(Arrays.asList(mTagGroup.getTags()));
            String newTag = spinnerCraft.getItemAtPosition(position).toString();
            if (!tags.contains(newTag)) {
              tags.add(newTag);
            }
            mTagGroup.setTags(tags);
            String s = spinnerCraft.getItemAtPosition(position).toString();
            for (Craft craft : crafts) {
              if (craft.getName().equals(s)) {
                SignUpActivity.this.craft = craft;
              }
            }
          }

          @Override
          public void onNothingSelected(AdapterView<?> parent) {}
        });
    mTagGroup.setTags();
    mTagGroup.setOnTagClickListener(
        tag -> {
          List<String> tags = new ArrayList<>();
          for (int i = 0; i < mTagGroup.getTags().length; i++) {
            if (!mTagGroup.getTags()[i].equals(tag)) {
              tags.add(mTagGroup.getTags()[i]);
            }
          }
          mTagGroup.setTags(tags.toArray(new String[tags.size()]));
        });
    currentPerson =
        new GsonBuilder()
            .serializeSpecialFloatingPointValues()
            .create()
            .fromJson(getIntent().getStringExtra("personString"), Person.class);
    if (currentPerson != null) {
      currentPerson.getMediaItem().setId(0);
      fillPerson(currentPerson);
      btnSignUp.setText("تحديث");
    }
    new Governate()
        .getAll(
            "",
            new OnCompleteListener<Governate>() {
              @Override
              public void onComplete(List<Governate> governates) {
                RoomDB.getInstance()
                    .governateDuo()
                    .insert(governates.toArray(new Governate[governates.size()]));
                List<String> strings = new ArrayList<>();
                for (Governate governate : governates) {
                  strings.add(governate.getGovernate());
                }
                ArrayAdapter<String> governatesAdapter =
                    new ArrayAdapter(
                        SignUpActivity.this, android.R.layout.simple_spinner_item, strings);
                governatesAdapter.setDropDownViewResource(
                    android.R.layout.simple_spinner_dropdown_item);
                spinnerGovernate.setAdapter(governatesAdapter);
                spinnerGovernate.setOnItemSelectedListener(
                    new AdapterView.OnItemSelectedListener() {
                      @Override
                      public void onItemSelected(
                          AdapterView<?> parent, View view, int position, long id) {
                        String s = spinnerGovernate.getItemAtPosition(position).toString();
                        try {
                          for (Governate governate : governates) {
                            if (governate.getGovernate().equals(s)) {
                              SignUpActivity.this.governate = governate;
                            }
                          }
                        } catch (Exception ignored) {
                        }
                      }

                      @Override
                      public void onNothingSelected(AdapterView<?> parent) {}
                    });
              }

              @Override
              public void onError(String error) {
                List<Governate> governates = RoomDB.getInstance().governateDuo().GOVERNATES();
                List<String> strings = new ArrayList<>();
                for (Governate governate : governates) {
                  strings.add(governate.getGovernate());
                }
                ArrayAdapter<String> governatesAdapter =
                    new ArrayAdapter(
                        SignUpActivity.this, android.R.layout.simple_spinner_item, strings);
                governatesAdapter.setDropDownViewResource(
                    android.R.layout.simple_spinner_dropdown_item);
                spinnerGovernate.setAdapter(governatesAdapter);
                spinnerGovernate.setOnItemSelectedListener(
                    new AdapterView.OnItemSelectedListener() {
                      @Override
                      public void onItemSelected(
                          AdapterView<?> parent, View view, int position, long id) {

                        String s = spinnerGovernate.getItemAtPosition(position).toString();
                        for (Governate governate : governates) {
                          if (governate.getGovernate().equals(s)) {
                            SignUpActivity.this.governate = governate;
                          }
                        }
                      }

                      // todo craftman by craft id
                      @Override
                      public void onNothingSelected(AdapterView<?> parent) {}
                    });
              }
            });
    craft.getList(
        "",
        new OnCompleteListener<Craft>() {
          @Override
          public void onComplete(List<Craft> crafts) {
            isCanAdd = false;
            SignUpActivity.this.crafts = crafts;
            List<String> strings = new ArrayList<>();
            for (Craft craft : crafts) {
              strings.add(craft.getName());
            }
            ArrayAdapter<String> craftsAdapter =
                new ArrayAdapter(
                    SignUpActivity.this, android.R.layout.simple_spinner_item, strings);
            craftsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerCraft.setAdapter(craftsAdapter);
            new Handler().postDelayed(() -> isCanAdd = true, 1000);
          }

          @Override
          public void onError(String error) {}
        });

    btnSignUp.setOnClickListener(this::signUp);
  }

  private boolean isCanAdd = true;

  private void signUp(View view) {
    if (!checkAcceptPolicy.isChecked()) {
      Toast.makeText(this, "يرجى الموافقه على شروط الخصوصيه.", Toast.LENGTH_SHORT).show();
      checkAcceptPolicy.requestFocus();
      return;
    }
    if (txtPhoneNumber.getText().toString().length() < 10) {
      Toast.makeText(this, "الرجاء ادخال رقم هاتف صالح", Toast.LENGTH_SHORT).show();
      txtPhoneNumber.requestFocus();
      return;
    }
    if (txtFullName.getText().toString().trim().length() < 1) {
      Toast.makeText(this, "الرجاء ادخال الاسم", Toast.LENGTH_SHORT).show();
      txtFullName.requestFocus();
      return;
    }
    if (txtAge.getText().toString().length() < 1) {
      Toast.makeText(this, "الرجاء ادخال العمر", Toast.LENGTH_SHORT).show();
      txtAge.requestFocus();
      return;
    }
    if (txtAddress.getText().toString().length() < 1) {
      Toast.makeText(this, "الرجاء ادخال العنوان", Toast.LENGTH_SHORT).show();
      txtAddress.requestFocus();
      return;
    }

    if (governate == null) {
      Toast.makeText(this, "الرجاء اختيار محافظه.", Toast.LENGTH_SHORT).show();
      imgPersonImage.requestFocus();
      return;
    }
    if (person.getMediaItem() == null) {
      Toast.makeText(this, "الرجاء اختيار صوره", Toast.LENGTH_SHORT).show();
      imgPersonImage.requestFocus();
      return;
    }
    //        if (mTagGroup.getTags().length < 1 ) {
    //            Toast.makeText(this, "الرجاء اختيار مهنه",
    //                    Toast.LENGTH_SHORT).show();
    //            return;
    //        }
    person.setPhoneNumber(txtPhoneNumber.getText().toString());
    person.setName(txtFullName.getText().toString());
    person.setGovernate(governate);
    person.setAddress(txtAddress.getText().toString());
    person.setAge(Integer.parseInt(txtAge.getText().toString()));
    findViewById(R.id.frameLayoutWait).setVisibility(View.VISIBLE);
    view.setVisibility(View.GONE);
    List<Craft> currentCrafts = new ArrayList<>();
    for (String tag : mTagGroup.getTags()) {
      for (Craft craft : crafts) {
        if (craft.getName().equals(tag)) {
          currentCrafts.add(craft);
        }
      }
    }
    person.setCrafts(currentCrafts);
    if (currentPerson != null) {
      person.setId(currentPerson.getId());
      person.update(
          "",
          new OnCompleteListener<Person>() {
            @Override
            public void onComplete(List<Person> people) {
              LocalData.getInstance()
                  .add(
                      Person.CURRENT_PERSON,
                      new GsonBuilder()
                          .serializeSpecialFloatingPointValues()
                          .create()
                          .toJson(people.get(0), Person.class));
              startActivity(new Intent(SignUpActivity.this, WelcomeActivity.class));
              findViewById(R.id.frameLayoutWait).setVisibility(View.GONE);

              finish();
            }

            @Override
            public void onError(String error) {
              findViewById(R.id.frameLayoutWait).setVisibility(View.GONE);
              view.setVisibility(View.VISIBLE);
              System.out.println(error);
            }
          });
    } else {
      person.register(
          "",
          new OnCompleteListener<Person>() {
            @Override
            public void onComplete(List<Person> people) {
              if (!people.get(0).isNewRecord()) {
                Snackbar.make(txtPhoneNumber, "هذا الرقم مسجل مسبقا", 5000)
                    .setAction("تسجيل الدخول؟", SignUpActivity.this::goToLogIn)
                    .show();
                findViewById(R.id.frameLayoutWait).setVisibility(View.GONE);
                view.setVisibility(View.VISIBLE);
                return;
              }
              LocalData.getInstance()
                  .add(
                      Person.CURRENT_PERSON,
                      new GsonBuilder()
                          .serializeSpecialFloatingPointValues()
                          .create()
                          .toJson(people.get(0), Person.class));
              startActivity(new Intent(SignUpActivity.this, WelcomeActivity.class));
              findViewById(R.id.frameLayoutWait).setVisibility(View.GONE);
              finish();
            }

            @Override
            public void onError(String error) {
              findViewById(R.id.frameLayoutWait).setVisibility(View.GONE);
              view.setVisibility(View.VISIBLE);
              System.out.println(error);
            }
          });
    }
  }

  private void goToLogIn(View view) {
    Intent intent = new Intent(this, PhoneActivity.class);
    intent.putExtra("phoneNumber", txtPhoneNumber.getText().toString());
    startActivity(intent);
    finish();
  }

  private void fillPerson(Person person) {
    Glide.with(this)
        .load(AWSS3Storage.getReference(person.getMediaItem().getUrl()))
        .into(imgPersonImage);
    this.person.setMediaItem(person.getMediaItem());
    txtFullName.setText(person.getName());
    txtAge.setText(String.valueOf(person.getAge()));
    txtPhoneNumber.setText(person.getPhoneNumber());
    txtAddress.setText(person.getAddress());
    List<String> tags = new ArrayList<>();
    for (Craft craft : person.getCrafts()) {
      tags.add(craft.getName());
    }
    mTagGroup.setTags(tags.toArray(new String[tags.size()]));
    checkAcceptPolicy.setChecked(true);
  }

  public void pickImage(View view) {
    BSImagePicker singleSelectionPicker =
        new BSImagePicker.Builder("com.morabaa.team.morabaacrafts.fileprovider")
            //                .setMaximumDisplayingImages(
            //                        24) //Default: Integer.MAX_VALUE. Don't worry about
            // performance :)
            .setSpanCount(3) // Default: 3. This is the number of columns
            .setGridSpacing(
                com.asksira.bsimagepicker.Utils.dp2px(
                    2)) // Default: 2dp. Remember to pass in a value in pixel.
            .setPeekHeight(
                com.asksira.bsimagepicker.Utils.dp2px(
                    360)) // Default: 360dp. This is the initial height of the dialog.
            .hideCameraTile() // Default: show. Set this if you don't want user to take photo.
            .hideGalleryTile() // Default: show. Set this if you don't want to further let user
            // select from a gallery app. In such case, I suggest you to set
            // maximum     displaying    images to Integer.MAX_VALUE.
            .build();
    singleSelectionPicker.show(getSupportFragmentManager(), "picker");
  }

  @Override
  public void onSingleImageSelected(Uri uri) {
    Glide.with(this)
        .load(uri)
        .apply(bitmapTransform(new BlurTransformation(10, 3)))
        .into(imgPersonImage);

    AWSS3Storage.getInstance(this)
        .uploadMini(
            uri,
            new AWSS3Storage.OnUploadProgressListener() {
              @Override
              public void onProgress(double progress) {}

              @Override
              public void onSuccess(String fileName) {
                Glide.with(SignUpActivity.this).load(uri).into(imgPersonImage);
                MediaItem mediaItem = new MediaItem();
                mediaItem.setUrl(fileName);
                person.setMediaItem(mediaItem);
              }

              @Override
              public void onFailure(String error) {}
            });
  }

  public void acceptPolicy(View view) {
    String url = "http://morabaacraftsapi-dev.us-east-2.elasticbeanstalk.com";
    Intent i = new Intent(Intent.ACTION_VIEW);
    i.setData(Uri.parse(url));
    startActivity(i);
  }
}
