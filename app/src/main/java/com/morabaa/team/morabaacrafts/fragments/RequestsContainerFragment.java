package com.morabaa.team.morabaacrafts.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.morabaa.team.morabaacrafts.R;
import com.morabaa.team.morabaacrafts.adapters.PagerAdapter;
import com.morabaa.team.morabaacrafts.model.Person;
import com.morabaa.team.morabaacrafts.model.Request;

import java.util.ArrayList;
import java.util.List;

public class RequestsContainerFragment extends Fragment
    implements RequestsFragment.OnDataUpdatedListener {

  private static RequestsContainerFragment instance;
  private ViewPager pagerRequestsFragments;
  private PagerAdapter pagerAdapter;
  private TabLayout tabs;
  private boolean update;

  private int requestsPageIndex;

  public RequestsContainerFragment() {
    // Required empty public constructor
  }

  public static RequestsContainerFragment newInstance(boolean update, int requestsPageIndex) {
    RequestsContainerFragment fragment = new RequestsContainerFragment();
    Bundle bundle = new Bundle();
    bundle.putBoolean("update", update);
    bundle.putInt("requestsPageIndex", requestsPageIndex);
    fragment.setArguments(bundle);
    return fragment;
  }

  public static RequestsContainerFragment getInstance() {
    return instance = instance == null ? newInstance(false, 0) : instance;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if (getArguments() != null) {
      this.update = getArguments().getBoolean("update");
      this.requestsPageIndex = getArguments().getInt("requestsPageIndex");
    }
  }

  @Override
  public View onCreateView(
      LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    return inflater.inflate(R.layout.fragment_requests_container, container, false);
  }

  public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

    pagerRequestsFragments = view.findViewById(R.id.pagerRequestsFragments);
    tabs = view.findViewById(R.id.requestsTabs);
    List<Fragment> fragments = new ArrayList<>();
    fragments.add(RequestsFragment.newInstance(Request.TYPE.SENT));

    if (Person.getCurrentPerson().getCrafts() != null
        && Person.getCurrentPerson().getCrafts().size() > 0) {
      fragments.add(RequestsFragment.newInstance(Request.TYPE.RECEIVED));
    } else {
      TabLayout.Tab tab = tabs.getTabAt(1);
      tabs.removeTab(tab);
    }
    pagerAdapter = new PagerAdapter(getChildFragmentManager(), fragments);
    pagerRequestsFragments.addOnPageChangeListener(
        new TabLayout.TabLayoutOnPageChangeListener(tabs));
    tabs.addOnTabSelectedListener(
        new TabLayout.ViewPagerOnTabSelectedListener(pagerRequestsFragments));
    pagerRequestsFragments.setAdapter(pagerAdapter);
    pagerRequestsFragments.setCurrentItem(requestsPageIndex);
  }

  @Override
  public void onDataUpdated(int tabIndex) {}
}
