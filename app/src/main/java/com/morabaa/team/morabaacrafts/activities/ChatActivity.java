package com.morabaa.team.morabaacrafts.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.GsonBuilder;
import com.morabaa.team.morabaacrafts.R;
import com.morabaa.team.morabaacrafts.fragments.MessagesFragment;
import com.morabaa.team.morabaacrafts.interfaces.OnCompleteListener;
import com.morabaa.team.morabaacrafts.model.Person;
import com.morabaa.team.morabaacrafts.model.Request;
import com.morabaa.team.morabaacrafts.utils.AWSS3Storage;
import com.morabaa.team.morabaacrafts.utils.Utils;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;

import java.util.List;

public class ChatActivity extends AppCompatActivity {

  private Request currentRequest;

  private TextView txtRequestPrice;
  private TextView txtRequestUnit;
  private TextView txtRequestWorkDate;
  private TextView txtRequestStartDate;

  private TextView txtRequestAddress;

  private LinearLayout layoutRequestInfo;
  private LinearLayout layoutSenderInfo;
  private TextView txtSenderName;
  private TextView txtSenderAddress;
  private ImageView imgSenderImage;

  private LinearLayout layoutReceiverInfo;
  private ImageView imgReceiverImage;
  private TextView txtReceiverName;
  private RatingBar ratingReceiverRating;
  private TextView txtReceiverAddress;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_chat);
    txtRequestPrice = findViewById(R.id.txtRequestPrice);

    txtRequestUnit = findViewById(R.id.txtRequestUnit);
    txtRequestWorkDate = findViewById(R.id.txtRequestWorkDate);
    txtRequestStartDate = findViewById(R.id.txtRequestStartDate);
    txtRequestAddress = findViewById(R.id.txtRequestAddress);
    layoutRequestInfo = findViewById(R.id.layoutRequestInfo);
    layoutSenderInfo = findViewById(R.id.layoutSenderInfo);
    txtSenderName = findViewById(R.id.txtSenderName);
    txtSenderAddress = findViewById(R.id.txtSenderAddress);
    imgSenderImage = findViewById(R.id.imgSenderImage);

    layoutReceiverInfo = findViewById(R.id.layoutReceiverInfo);
    imgReceiverImage = findViewById(R.id.imgReceiverImage);
    txtReceiverName = findViewById(R.id.txtReceiverName);
    ratingReceiverRating = findViewById(R.id.ratingReceiverRating);
    txtReceiverAddress = findViewById(R.id.txtReceiverAddress);

    String chatId = getIntent().getStringExtra("chatId");
    String currentRequestString = getIntent().getStringExtra("currentRequestString");
    currentRequest =
        new GsonBuilder()
            .serializeSpecialFloatingPointValues()
            .create()
            .fromJson(currentRequestString, Request.class);
    if (currentRequest != null) {
      currentRequest.getOneByStatus(
          "",
          new OnCompleteListener<Request>() {
            @Override
            public void onComplete(List<Request> requests) {
              currentRequest = requests.get(0);
              initRequest();
            }

            @Override
            public void onError(String error) {}
          });
    }

    getSupportFragmentManager()
        .beginTransaction()
        .add(R.id.frameInBoxContainer, MessagesFragment.newInstance(chatId))
        //        .addToBackStack(null)
        .commit();

    KeyboardVisibilityEvent.setEventListener(
        this, isOpen -> layoutRequestInfo.setVisibility(isOpen ? View.GONE : View.VISIBLE));
  }

  private void initRequest() {
    if (currentRequest.getSender() != null) {
      txtSenderName.setText(currentRequest.getSender().getName());
      txtSenderAddress.setText(currentRequest.getSender().getAddress());
      try {
        Glide.with(this)
            .load(AWSS3Storage.getReference(currentRequest.getReceiver().getMediaItem().getUrl()))
            .into(imgSenderImage);
      } catch (Exception ignored) {
      }
    }
    if (currentRequest.getReceiver() != null) {
      try {
        Glide.with(this)
            .load(AWSS3Storage.getReference(currentRequest.getReceiver().getMediaItem().getUrl()))
            .into(imgReceiverImage);
      } catch (Exception ignored) {
      }
      txtReceiverName.setText(currentRequest.getReceiver().getName());
      ratingReceiverRating.setRating(currentRequest.getReceiver().getRating());
      txtReceiverAddress.setText(currentRequest.getReceiver().getAddress());
    }

    if (currentRequest.getSender().getId() == Person.getCurrentPerson().getId()) {
      layoutSenderInfo.setVisibility(View.GONE);
      layoutReceiverInfo.setVisibility(View.VISIBLE);

    } else if (currentRequest.getReceiver().getId() == Person.getCurrentPerson().getId()) {
      layoutSenderInfo.setVisibility(View.VISIBLE);
      layoutReceiverInfo.setVisibility(View.GONE);
    } else {
      return;
    }

    txtRequestPrice.setText(String.valueOf(currentRequest.getPrice()));

    txtRequestUnit.setText(currentRequest.getUnit());
    txtRequestWorkDate.setText(Utils.getRelativeTimeSpan(currentRequest.getWorkDate()));
    txtRequestStartDate.setText(Utils.getRelativeTimeSpan(currentRequest.getCreationDate()));
    txtRequestAddress.setText(currentRequest.getAddress());
  }

  @Override
  public void onBackPressed() {
    super.onBackPressed();
  }
}
