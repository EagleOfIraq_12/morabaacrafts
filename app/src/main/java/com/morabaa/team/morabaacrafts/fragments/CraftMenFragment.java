package com.morabaa.team.morabaacrafts.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.SearchView;

import com.google.gson.GsonBuilder;
import com.morabaa.team.morabaacrafts.R;
import com.morabaa.team.morabaacrafts.adapters.CraftMenAdapter;
import com.morabaa.team.morabaacrafts.interfaces.OnCompleteListener;
import com.morabaa.team.morabaacrafts.model.Craft;
import com.morabaa.team.morabaacrafts.model.Person;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class CraftMenFragment extends Fragment {

    static CraftMenFragment instance;
    private ProgressBar bottomProgressBar;
    private long craftId = 0;
    private List<Person> people = new ArrayList<>();
    private CraftMenAdapter craftMenAdapter;
    private SearchView txtSearchCraftMen;
    private RecyclerView craftMenRecyclerView;

    public CraftMenFragment() {
        // Required empty public constructor
    }

    public static CraftMenFragment newInstance(long craftId) {
        CraftMenFragment fragment = new CraftMenFragment();
        Bundle bundle = new Bundle();
        bundle.putLong("craftId", craftId);
        fragment.setArguments(bundle);
        return fragment;
    }

    public static CraftMenFragment getInstance(long craftId) {
        return instance = instance == null ? newInstance(craftId) : instance;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            craftId = getArguments().getLong("craftId");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_craft_men, container, false);
    }


    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        txtSearchCraftMen = view.findViewById(R.id.txtSearchCraftMen);
        bottomProgressBar = view.findViewById(R.id.bottomProgressBar);
        craftMenRecyclerView = view.findViewById(R.id.craftMenRecyclerView);

        craftMenAdapter = new CraftMenAdapter(getContext(), people);
        craftMenAdapter.setOnCraftManClickedListener(this::onCraftManClicked);
        craftMenRecyclerView.setLayoutManager(
                new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        craftMenRecyclerView.setAdapter(craftMenAdapter);
        Craft craft = new Craft();
        craft.setId(craftId);
        Person person = new Person();
        List<Craft> crafts = new ArrayList<>();
        crafts.add(craft);
        person.setCrafts(crafts);
        person.getAll(this.getClass().getName(), new OnCompleteListener<Person>() {
            @Override
            public void onComplete(List<Person> people) {
                craftMenAdapter.setPeople(people);
                bottomProgressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError(String error) {
                craftMenAdapter.setPeople(people);
                bottomProgressBar.setVisibility(View.GONE);
            }
        });

    }


    private void onCraftManClicked(Person person) {
        Bundle bundle = new Bundle();
        bundle.putString("craftManString",
                new GsonBuilder()
                        .serializeSpecialFloatingPointValues()
                        .create()
                        .toJson(person, Person.class)
        );
        assert getParentFragment() != null;
        getParentFragment().
                getChildFragmentManager()
                .beginTransaction()
                .add(
                        R.id.frameMainContainer,
                        CraftManProfileFragment.newInstance(bundle),
                        "CraftManProfileFragment"
                )
                .addToBackStack(null)
                .commit();
    }
}