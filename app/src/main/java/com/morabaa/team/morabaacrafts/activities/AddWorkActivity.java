package com.morabaa.team.morabaacrafts.activities;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.asksira.bsimagepicker.BSImagePicker;
import com.google.gson.GsonBuilder;
import com.morabaa.team.morabaacrafts.R;
import com.morabaa.team.morabaacrafts.camera.CameraHelper;
import com.morabaa.team.morabaacrafts.holders.MediaHolder;
import com.morabaa.team.morabaacrafts.interfaces.OnCompleteListener;
import com.morabaa.team.morabaacrafts.model.Person;
import com.morabaa.team.morabaacrafts.model.MediaItem;
import com.morabaa.team.morabaacrafts.model.Request;
import com.morabaa.team.morabaacrafts.model.Work;
import com.morabaa.team.morabaacrafts.utils.CheckPermission;

import java.util.Date;
import java.util.List;

public class AddWorkActivity extends AppCompatActivity
        implements
        BSImagePicker.OnSingleImageSelectedListener,
        BSImagePicker.OnMultiImageSelectedListener {

    private EditText txtWorkTitle;
    private EditText txtWorkDescription;
    private Button btnAddNewMedia;
    private Button btnCaptureNewMedia;
    private Button btnSaveWork;
    private LinearLayout layoutWorkImagesContainer;
    private MediaHolder mediaHolder;
    private Request request;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_work);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        String notificationBody = getIntent().getStringExtra("notificationBody");
        if (notificationBody != null) {
            Toast.makeText(this, notificationBody, Toast.LENGTH_SHORT).show();
        }

        String requestString = getIntent().getStringExtra("requestString");

        request = new GsonBuilder()
                .serializeSpecialFloatingPointValues()
                .create()
                .fromJson(requestString, Request.class);


        layoutWorkImagesContainer = findViewById(R.id.layoutWorkImagesContainer);
        txtWorkTitle = findViewById(R.id.txtWorkTitle);
        txtWorkDescription = findViewById(R.id.txtWorkDescription);
        btnAddNewMedia = findViewById(R.id.btnAddNewMedia);
        btnCaptureNewMedia = findViewById(R.id.btnCaptureNewMedia);
        btnSaveWork = findViewById(R.id.btnSaveWork);
        btnCaptureNewMedia
                .setOnClickListener(view -> {
                    CheckPermission.newInstance(this, Manifest.permission.RECORD_AUDIO, granted -> {
                        if (granted) {
                            CheckPermission.newInstance(this, Manifest.permission.CAMERA, granted1 -> {
                                if (granted1) {
                                    CameraHelper.newInstance(this, CameraHelper.SessionType.IMAGE,
                                            this::onSingleImageSelected);
                                }
                            });
                        }
                    });
                });

        BSImagePicker multiSelectionPicker = new BSImagePicker.Builder(
                "com.yourdomain.yourpackage.fileprovider")
                .isMultiSelect() //Set this if you want to use multi selection mode.
//                              .setMinimumMultiSelectCount(3) //Default: 1.
//                              .setMaximumMultiSelectCount(6) //Default: Integer.MAX_VALUE (i.e. User can select as many images as he/she wants)
                .setMultiSelectBarBgColor(
                        android.R.color.white)
                .setMultiSelectTextColor(
                        R.color.primary_text)
                .setMultiSelectDoneTextColor(
                        R.color.colorAccent)
                .setOverSelectTextColor(
                        R.color.error_text)
                .disableOverSelectionMessage()
                .build();

        btnAddNewMedia.setOnClickListener(view -> {
            multiSelectionPicker.show(getSupportFragmentManager(), "picker");
        });

        mediaHolder = new MediaHolder(AddWorkActivity.this, layoutWorkImagesContainer).build();
        Person person = Person.getCurrentPerson();


        btnSaveWork.setOnClickListener(view -> {
            if (!mediaHolder.isCanPost()) {
                Toast.makeText(this, "الرجاء انتظار لحين اكتمال رفع الصور", Toast.LENGTH_SHORT).show();
                return;
            }
            Work work = new Work();
            work.setRequest(request);
            work.setTitle(txtWorkTitle.getText().toString());
            work.setDescription(txtWorkDescription.getText().toString());
            work.setMediaItems(mediaHolder.getMediaItems());
            work.setPerson(person);
            System.out.println(new Date().getTime());
            view.setEnabled(false);
            view.setVisibility(View.GONE);
            work.push("", new OnCompleteListener<Work>() {
                @Override
                public void onComplete(List<Work> works) {
                    Intent workIntent = new Intent(AddWorkActivity.this,
                            WorkDetailsActivity.class);
                    workIntent.putExtra("workString",
                            new GsonBuilder()
                                    .serializeSpecialFloatingPointValues()
                                    .create()
                                    .toJson(
                                            works.get(0),
                                            Work.class
                                    ));

                    if (request != null) {
                        request.setPosted(true);
                        request.update("", new OnCompleteListener<Request>() {
                            @Override
                            public void onComplete(List<Request> requests) {
                                view.setEnabled(true);
                                startActivity(workIntent);
                                view.setVisibility(View.VISIBLE);
                                finish();
                            }

                            @Override
                            public void onError(String error) {

                            }
                        });
                    } else {
                        view.setEnabled(true);
                        startActivity(workIntent);
                        view.setVisibility(View.VISIBLE);
                        finish();
                    }

                }

                @Override
                public void onError(String error) {
                    System.out.println("error = [" + error + "]");
                    view.setEnabled(true);
                    view.setVisibility(View.VISIBLE);
                }
            });
        });
    }


    @Override
    public void onMultiImageSelected(List<Uri> uriList) {
        System.out.println("uriList = " + uriList);
        for (Uri uri : uriList) {

//            String path = ImageUtils.saveLocalFile(this, uri);
//            System.out.println("path = " + path);
//            Uri uri1 = ImageUtils.getUriFromPath(path);

            MediaItem mediaItem = new MediaItem();
            mediaItem.setUri(uri);
            mediaHolder.addMedia(mediaItem);
        }
    }

    @Override
    public void onSingleImageSelected(Uri uri) {
        System.out.println("uri = " + uri);
        MediaItem mediaItem = new MediaItem();
        mediaItem.setUri(uri);
        mediaHolder.addMedia(mediaItem);
    }
}
