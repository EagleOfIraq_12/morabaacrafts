package com.morabaa.team.morabaacrafts.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.morabaa.team.morabaacrafts.R;
import com.morabaa.team.morabaacrafts.activities.AdminActivity;
import com.morabaa.team.morabaacrafts.activities.PhoneActivity;
import com.morabaa.team.morabaacrafts.utils.CheckPermission;

public class LoginFragment extends Fragment {

  private Button btnLogin;
  private EditText txtPhoneNumber;

  private String phoneNumber;

  private OnLoginListener onLoginListener;

  public LoginFragment() {}

  public static LoginFragment newInstance(String phoneNumber) {
    LoginFragment fragment = new LoginFragment();
    Bundle args = new Bundle();
    args.putString(PhoneActivity.PHONE_NUMBER, phoneNumber);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if (getArguments() != null) {
      phoneNumber = getArguments().getString(PhoneActivity.PHONE_NUMBER);
    }
  }

  @Override
  public View onCreateView(
      LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    return inflater.inflate(R.layout.fragment_login, container, false);
  }

  @SuppressLint({"MissingPermission", "HardwareIds"})
  @Override
  public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    txtPhoneNumber = view.findViewById(R.id.txtPhoneNumber);
    txtPhoneNumber.setText(phoneNumber == null ? "" : phoneNumber);
    btnLogin = view.findViewById(R.id.btnRegister);
    TelephonyManager telephonyManager =
        ((TelephonyManager) getContext().getSystemService(Context.TELEPHONY_SERVICE));
    assert telephonyManager != null;
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
      CheckPermission.newInstance(
          getContext(),
          Manifest.permission.READ_PHONE_NUMBERS,
          granted -> {
            if (granted) {
              txtPhoneNumber.setText(
                  phoneNumber != null ? phoneNumber : telephonyManager.getLine1Number());
            } else {
              txtPhoneNumber.setText(phoneNumber != null ? phoneNumber : "");
            }
          });
    } else {
      txtPhoneNumber.setText(phoneNumber != null ? phoneNumber : "");
    }

    btnLogin.setOnClickListener(
        view1 -> {
          view1.setEnabled(false);
          if (txtPhoneNumber.getText().toString().equals("1234")) {
            startActivity(new Intent(getContext(), AdminActivity.class));
          }
          onLoginListener.onLogin(txtPhoneNumber.getText().toString());
        });
  }

  public void onButtonPressed(View view) {
    if (onLoginListener != null) {
      onLoginListener.onLogin(phoneNumber);
    }
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    if (context instanceof OnLoginListener) {
      onLoginListener = (OnLoginListener) context;
    } else {
      throw new RuntimeException(context.toString() + " must implement OnRegisterListener");
    }
  }

  @Override
  public void onDetach() {
    super.onDetach();
    onLoginListener = null;
  }

  public interface OnLoginListener {
    // TODO: Update argument type and name
    void onLogin(String phoneNumber);
  }
}
