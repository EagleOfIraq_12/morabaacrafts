package com.morabaa.team.morabaacrafts.model.message;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import com.morabaa.team.morabaacrafts.model.Person;

import java.util.HashMap;
import java.util.Map;
@Entity
public class Message {
  @PrimaryKey
  private long id;
  private String body;
  private long date;
  private String conversationKey;
  private long senderId;
  @Ignore
  private Person sender;

  public Message() {}

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getBody() {
    return body;
  }

  public void setBody(String body) {
    this.body = body;
  }

  public long getDate() {
    return date;
  }

  public void setDate(long date) {
    this.date = date;
  }

  public String getConversationKey() {
    return conversationKey;
  }

  public void setConversationKey(String conversationKey) {
    this.conversationKey = conversationKey;
  }

  public long getSenderId() {
    return senderId;
  }

  public void setSenderId(long senderId) {
    this.senderId = senderId;
  }

  public Person getSender() {
    return sender;
  }

  public void setSender(Person sender) {
    this.sender = sender;
  }

  public Map<String, Object> toMap() {
    Map<String, Object> map = new HashMap();
    map.put("id", this.id);
    map.put("body", this.body);
    map.put("conversationKey", this.conversationKey);
    map.put("date", this.date);
    map.put("senderId", this.senderId);
    map.put("sender", null);
    return map;
  }
}
