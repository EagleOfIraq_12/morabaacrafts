package com.morabaa.team.morabaacrafts.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.GsonBuilder;
import com.morabaa.team.morabaacrafts.R;
import com.morabaa.team.morabaacrafts.activities.AddWorkActivity;
import com.morabaa.team.morabaacrafts.activities.ChatActivity;
import com.morabaa.team.morabaacrafts.activities.MainActivity;
import com.morabaa.team.morabaacrafts.activities.NotificationActivity;
import com.morabaa.team.morabaacrafts.activities.WorkDetailsActivity;
import com.morabaa.team.morabaacrafts.adapters.RequestsAdapter;
import com.morabaa.team.morabaacrafts.adapters.RequestsAdapter.OnRequestClickedListener;
import com.morabaa.team.morabaacrafts.bottomSheets.AcceptRequestBottomSheet;
import com.morabaa.team.morabaacrafts.bottomSheets.RateReceiverBottomSheet;
import com.morabaa.team.morabaacrafts.bottomSheets.RateWorkBottomSheet;
import com.morabaa.team.morabaacrafts.interfaces.OnCompleteListener;
import com.morabaa.team.morabaacrafts.model.Person;
import com.morabaa.team.morabaacrafts.model.Request;
import com.morabaa.team.morabaacrafts.model.Request.STATUS;
import com.morabaa.team.morabaacrafts.model.Work;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class RequestsFragment extends Fragment implements OnRequestClickedListener {

  private static RequestsFragment instance;
  private int type;
  private ProgressBar bottomProgressBar;
  private RequestsAdapter requestsAdapter;
  private SearchView txtSearchCraftManRequests;
  private RecyclerView recyclerViewCraftManRequests;
  private long[] ids;
  private List<Request> requests;

  public RequestsFragment() {
    // Required empty public constructor
  }

  public static RequestsFragment newInstance(int type, long... ids) {
    RequestsFragment fragment = new RequestsFragment();
    Bundle bundle = new Bundle();
    bundle.putInt("type", type);
    bundle.putLongArray("ids", ids);
    fragment.setArguments(bundle);
    return fragment;
  }

  public static RequestsFragment getInstance(int status) {
    return instance = instance == null ? newInstance(status) : instance;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if (getArguments() != null) {
      this.type = getArguments().getInt("type");
      this.ids = getArguments().getLongArray("ids");
    }
  }

  @Override
  public View onCreateView(
      @NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    return inflater.inflate(R.layout.fragment_requests, container, false);
  }

  public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

    if (getActivity() instanceof MainActivity) {
      addOnDataUpdatedListener((MainActivity) getActivity());
    } else if (getActivity() instanceof NotificationActivity) {
      addOnDataUpdatedListener((NotificationActivity) getActivity());
    }
    txtSearchCraftManRequests = view.findViewById(R.id.txtSearchCraftManRequests);
    bottomProgressBar = view.findViewById(R.id.bottomProgressBar);
    recyclerViewCraftManRequests = view.findViewById(R.id.recyclerViewCraftManRequests);

    requestsAdapter = new RequestsAdapter(new ArrayList<>(), type);

    requestsAdapter.setOnRequestClickedListener(this);
    requestsAdapter.setOnReceiverClickedListener(this::onCraftManClicked);

    recyclerViewCraftManRequests.setLayoutManager(
        new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
    recyclerViewCraftManRequests.setAdapter(requestsAdapter);
    initRequests();
    //        addOnDataUpdatedListener(RequestsContainerFragment.getInstance());
  }

  private void onCraftManClicked(Person person) {
    if (true) return;
    Bundle bundle = new Bundle();
    bundle.putString(
        "craftManString",
        new GsonBuilder()
            .serializeSpecialFloatingPointValues()
            .create()
            .toJson(person, Person.class));
    assert getParentFragment() != null;
    getParentFragment()
        .getChildFragmentManager()
        .beginTransaction()
        .add(
            R.id.frameRequestsContainer,
            CraftManProfileFragment.newInstance(bundle),
            "CraftManProfileFragment")
        .addToBackStack(null)
        .commit();
    // TODO return on back
  }

  private void initRequests() {
    Request request = new Request();
    final Person person = Person.getCurrentPerson();
    request.setSender(person);
    request.setReceiver(person);
    if (ids.length > 0) {
      request.setId(ids[0]);
      request.getOneByStatus(
          "",
          new OnCompleteListener<Request>() {
            @Override
            public void onComplete(List<Request> requests) {
              requestsAdapter.setRequests(requests);
              bottomProgressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError(String error) {
              System.out.println("error = " + error);
              bottomProgressBar.setVisibility(View.GONE);
            }
          });

    } else {
      request.setStatus(STATUS.ALL);
      if (type == Request.TYPE.SENT) {
        request.getSent(
            "",
            new OnCompleteListener<Request>() {
              @Override
              public void onComplete(List<Request> requests) {
                RequestsFragment.this.requests = requests;
                Collections.sort(
                    requests, (r1, r2) -> r1.getCreationDate() > r2.getCreationDate() ? 1 : 0);
                requestsAdapter.setRequests(requests);
                bottomProgressBar.setVisibility(View.GONE);
              }

              @Override
              public void onError(String error) {
                System.out.println("error = " + error);
                bottomProgressBar.setVisibility(View.GONE);
              }
            });
      } else {
        request.getReceived(
            "",
            new OnCompleteListener<Request>() {
              @Override
              public void onComplete(List<Request> requests) {
                RequestsFragment.this.requests = requests;
                Collections.sort(
                    requests, (r1, r2) -> r1.getCreationDate() > r2.getCreationDate() ? 1 : 0);
                requestsAdapter.setRequests(requests);
                bottomProgressBar.setVisibility(View.GONE);
              }

              @Override
              public void onError(String error) {
                System.out.println("error = " + error);
                bottomProgressBar.setVisibility(View.GONE);
              }
            });
      }
    }
  }

  @Override
  public void onRequestClicked(Request request) {}

  @Override
  public void onAcceptRequest(final Request request) {
    request.setStatus(STATUS.ACCEPTED);
    AcceptRequestBottomSheet acceptRequestBottomSheet = new AcceptRequestBottomSheet();
    Bundle bundle = new Bundle();
    bundle.putString(
        "requestString",
        new GsonBuilder()
            .serializeSpecialFloatingPointValues()
            .create()
            .toJson(request, Request.class));
    acceptRequestBottomSheet.setArguments(bundle);
    acceptRequestBottomSheet.setOnDoneListener(RequestsFragment.this::reload);

    acceptRequestBottomSheet.show(getActivity().getSupportFragmentManager(), null);
  }

  private void reload(boolean ok) {
    getFragmentManager()
        .beginTransaction()
        .detach(RequestsFragment.this)
        .attach(RequestsFragment.this)
        .commit();
  }

  @Override
  public void onRejectRequest(Request request) {
    request.setStatus(STATUS.REJECTED);
    AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
    alertDialog.setCancelable(true);
    LayoutInflater inflater =
        (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    View view = inflater.inflate(R.layout.dialog_reject_request_layout, null, false);
    final ProgressBar progressBarUpdating = view.findViewById(R.id.progressBarUpdating);
    Button btnOk = view.findViewById(R.id.btnOk);
    Button btnCancel = view.findViewById(R.id.btnCancel);
    btnCancel.setOnClickListener(btn -> alertDialog.dismiss());
    btnOk.setOnClickListener(
        view1 -> {
          progressBarUpdating.setVisibility(View.VISIBLE);
          request.update(
              "",
              new OnCompleteListener<Request>() {
                @Override
                public void onComplete(List<Request> requests) {
                  System.out.println("requests = [" + requests.get(0).getStatus() + "]");
                  progressBarUpdating.setVisibility(View.GONE);
                  alertDialog.dismiss();
                  if (onDataUpdatedListener != null) {
                    onDataUpdatedListener.onDataUpdated(0);
                  }
                }

                @Override
                public void onError(String error) {
                  progressBarUpdating.setVisibility(View.GONE);
                  alertDialog.dismiss();
                }
              });
        });

    alertDialog.setView(view);
    alertDialog.show();
  }

  @Override
  public void onReceiverEditRequest(Request request) {
    request.setStatus(STATUS.ACCEPTED);
    AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
    alertDialog.setCancelable(true);
    LayoutInflater inflater =
        (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    View view = inflater.inflate(R.layout.dialog_accept_request_layout, null, false);
    view.findViewById(R.id.frameDialog).setBackgroundColor(Color.argb(0, 0, 0, 0));
    Button btnOk = view.findViewById(R.id.btnOk);
    final EditText txtRequestNote = view.findViewById(R.id.txtRequestReceiverNote);
    txtRequestNote.setText(request.getReceiverNote());
    final ProgressBar progressBarUpdating = view.findViewById(R.id.progressBarUpdating);

    final EditText txtYear = view.findViewById(R.id.txtYear);
    final EditText txtMonth = view.findViewById(R.id.txtMonth);
    final EditText txtDay = view.findViewById(R.id.txtDay);
    final EditText txtPrice = view.findViewById(R.id.txtPrice);
    final Spinner spinnerUnit = view.findViewById(R.id.spinnerUnit);

    //        int day, month, year;
    int year = new Date(request.getWorkDate()).getYear() + 1900;
    int month = new Date(request.getWorkDate()).getMonth() + 1;
    int day = new Date(request.getWorkDate()).getDate();

    txtYear.setText(String.valueOf(year));
    txtMonth.setText(String.valueOf(month));
    txtDay.setText(String.valueOf(day));
    txtPrice.setText(String.valueOf(request.getPrice()));
    btnOk.setOnClickListener(
        view1 -> {
          if (txtRequestNote.getText().toString().trim().length() < 1) {
            Toast.makeText(getContext(), "الرجاء قم بادخال ملاحظه", Toast.LENGTH_SHORT).show();
            txtRequestNote.requestFocus();
            return;
          }
          request.setReceiverNote(txtRequestNote.getText().toString());
          if (txtPrice.getText().toString().trim().length() < 1) {
            Toast.makeText(getContext(), "الرجاء قم بادخال السعر", Toast.LENGTH_SHORT).show();
            txtPrice.requestFocus();
            return;
          }
          request.setPrice(Float.parseFloat(txtPrice.getText().toString()));
          request.setUnit(spinnerUnit.getSelectedItem().toString());

          int yearX = Integer.parseInt(txtYear.getText().toString());
          int monthX = Integer.parseInt(txtMonth.getText().toString());
          int dayX = Integer.parseInt(txtDay.getText().toString());

          Calendar todayCalendar = new GregorianCalendar();
          todayCalendar.set(yearX + 0, monthX - 1, dayX + 0);

          long date = todayCalendar.getTimeInMillis(); // new Date(year, month, day).getTime();
          request.setWorkDate(date);
          progressBarUpdating.setVisibility(View.VISIBLE);
          request.update(
              "",
              new OnCompleteListener<Request>() {
                @Override
                public void onComplete(List<Request> requests) {
                  System.out.println("requests = [" + requests.get(0).getStatus() + "]");
                  progressBarUpdating.setVisibility(View.GONE);
                  alertDialog.dismiss();
                  if (onDataUpdatedListener != null) {
                    onDataUpdatedListener.onDataUpdated(1);
                  }
                }

                @Override
                public void onError(String error) {
                  progressBarUpdating.setVisibility(View.GONE);
                  alertDialog.dismiss();
                }
              });
        });

    alertDialog.setView(view);
    alertDialog.show();
  }

  @Override
  public void onDoneRequest(Request request) {
    AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
    alertDialog.setCancelable(true);
    alertDialog.show();
    request.setStatus(STATUS.DONE);
    request.update(
        "",
        new OnCompleteListener<Request>() {
          @Override
          public void onComplete(List<Request> requests) {
            System.out.println("requests = [" + requests.get(0).getStatus() + "]");
            alertDialog.dismiss();
            if (onDataUpdatedListener != null) {
              onDataUpdatedListener.onDataUpdated(0);
            }
          }

          @Override
          public void onError(String error) {
            alertDialog.dismiss();
          }
        });
  }

  @Override
  public void onPostRequest(Request request) {
    Intent addWorkIntent = new Intent(getContext(), AddWorkActivity.class);
    addWorkIntent.putExtra(
        "requestString",
        new GsonBuilder()
            .serializeSpecialFloatingPointValues()
            .create()
            .toJson(request, Request.class));

    getContext().startActivity(addWorkIntent);
  }

  @Override
  public void onCancelRequest(Request request) {
    AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
    alertDialog.setCancelable(true);
    alertDialog.show();
    request.setStatus(-1);
    request.update(
        "",
        new OnCompleteListener<Request>() {
          @Override
          public void onComplete(List<Request> requests) {
            System.out.println("requests = [" + requests.get(0).getStatus() + "]");
            alertDialog.dismiss();
            if (onDataUpdatedListener != null) {
              onDataUpdatedListener.onDataUpdated(0);
            }
          }

          @Override
          public void onError(String error) {
            alertDialog.dismiss();
          }
        });
  }

  @Override
  public void onDealRequest(Request request) {
    request.setStatus(STATUS.DEAL);
    AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
    alertDialog.setCancelable(true);
    LayoutInflater inflater =
        (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    View view = inflater.inflate(R.layout.dialog_deal_request_layout, null, false);
    view.findViewById(R.id.frameDialog).setBackgroundColor(Color.argb(0, 0, 0, 0));
    Button btnOk = view.findViewById(R.id.btnOk);
    final ProgressBar progressBarUpdating = view.findViewById(R.id.progressBarUpdating);
    btnOk.setOnClickListener(
        view1 -> {
          progressBarUpdating.setVisibility(View.VISIBLE);
          request.update(
              "",
              new OnCompleteListener<Request>() {
                @Override
                public void onComplete(List<Request> requests) {
                  System.out.println("requests = [" + requests.get(0).getStatus() + "]");
                  progressBarUpdating.setVisibility(View.GONE);
                  alertDialog.dismiss();
                  if (onDataUpdatedListener != null) {
                    onDataUpdatedListener.onDataUpdated(0);
                  }
                }

                @Override
                public void onError(String error) {
                  progressBarUpdating.setVisibility(View.GONE);
                  alertDialog.dismiss();
                }
              });
        });
    alertDialog.setView(view);
    alertDialog.show();
  }

  @Override
  public void onAbortRequest(Request request) {
    request.setStatus(STATUS.ABORTED);
    AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
    alertDialog.setCancelable(true);
    LayoutInflater inflater =
        (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    View view = inflater.inflate(R.layout.dialog_aboart_request_layout, null, false);
    view.findViewById(R.id.frameDialog).setBackgroundColor(Color.argb(0, 0, 0, 0));
    Button btnOk = view.findViewById(R.id.btnOk);
    final ProgressBar progressBarUpdating = view.findViewById(R.id.progressBarUpdating);
    btnOk.setOnClickListener(
        view1 -> {
          progressBarUpdating.setVisibility(View.VISIBLE);
          request.update(
              "",
              new OnCompleteListener<Request>() {
                @Override
                public void onComplete(List<Request> requests) {
                  System.out.println("requests = [" + requests.get(0).getStatus() + "]");
                  progressBarUpdating.setVisibility(View.GONE);
                  alertDialog.dismiss();
                  if (onDataUpdatedListener != null) {
                    onDataUpdatedListener.onDataUpdated(1);
                  }
                }

                @Override
                public void onError(String error) {
                  progressBarUpdating.setVisibility(View.GONE);
                  alertDialog.dismiss();
                }
              });
        });
    alertDialog.setView(view);
    alertDialog.show();
  }

  @Override
  public void onSenderEditRequest(Request request) {
    request.setStatus(STATUS.INIT);
    AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
    alertDialog.setCancelable(true);
    LayoutInflater inflater =
        (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    View view = inflater.inflate(R.layout.dialog_user_edit_request_layout, null, false);
    view.findViewById(R.id.frameDialog).setBackgroundColor(Color.argb(0, 0, 0, 0));
    Button btnOk = view.findViewById(R.id.btnOk);
    final EditText txtRequestUserNote = view.findViewById(R.id.txtRequestSenderNote);
    final EditText txtAddress = view.findViewById(R.id.txtAddress);

    txtAddress.setText(request.getAddress());
    txtRequestUserNote.setText(request.getSenderNote());
    final ProgressBar progressBarUpdating = view.findViewById(R.id.progressBarUpdating);

    btnOk.setOnClickListener(
        view1 -> {
          progressBarUpdating.setVisibility(View.VISIBLE);
          request.setSenderNote(txtRequestUserNote.getText().toString());
          request.setAddress(txtAddress.getText().toString());
          request.update(
              "",
              new OnCompleteListener<Request>() {
                @Override
                public void onComplete(List<Request> requests) {
                  progressBarUpdating.setVisibility(View.GONE);
                  alertDialog.dismiss();
                  if (onDataUpdatedListener != null) {
                    onDataUpdatedListener.onDataUpdated(0);
                  }
                }

                @Override
                public void onError(String error) {
                  progressBarUpdating.setVisibility(View.GONE);
                  alertDialog.dismiss();
                }
              });
        });

    alertDialog.setView(view);
    alertDialog.show();
  }

  @Override
  public void onRateReceiverRequest(Request request) {
    RateReceiverBottomSheet rateReceiverBottomSheet = new RateReceiverBottomSheet();
    Bundle bundle = new Bundle();
    bundle.putString(
        "requestString",
        new GsonBuilder()
            .serializeSpecialFloatingPointValues()
            .create()
            .toJson(request, Request.class));
    rateReceiverBottomSheet.setArguments(bundle);
    rateReceiverBottomSheet.setOnDoneListener(RequestsFragment.this::reload);

    assert getParentFragment() != null;
    rateReceiverBottomSheet.show(getActivity().getSupportFragmentManager(), null);
  }

  @Override
  public void onRateWorkRequest(Request request) {
    RateWorkBottomSheet rateWorkBottomSheet = new RateWorkBottomSheet();
    Bundle bundle = new Bundle();
    bundle.putString(
        "requestString",
        new GsonBuilder()
            .serializeSpecialFloatingPointValues()
            .create()
            .toJson(request, Request.class));
    rateWorkBottomSheet.setArguments(bundle);
    rateWorkBottomSheet.setOnDoneListener(RequestsFragment.this::reload);

    rateWorkBottomSheet.show(getActivity().getSupportFragmentManager(), null);
  }

  @Override
  public void onShowWorkRequest(Request request) {
    Intent workActivityIntent = new Intent(getContext(), WorkDetailsActivity.class);
    workActivityIntent.putExtra(
        "workString",
        new GsonBuilder()
            .serializeSpecialFloatingPointValues()
            .create()
            .toJson(request.getWork(), Work.class));
    startActivity(workActivityIntent);
  }

  @Override
  public void onOpenChat(Request request) {
    Intent chatIntent = new Intent(getContext(), ChatActivity.class);
    chatIntent.putExtra("chatId", String.valueOf(request.getId()));
    chatIntent.putExtra(
        "currentRequestString",
        new GsonBuilder()
            .serializeSpecialFloatingPointValues()
            .create()
            .toJson(request, Request.class));
    getContext().startActivity(chatIntent);
  }

  private OnDataUpdatedListener onDataUpdatedListener;

  public interface OnDataUpdatedListener {

    void onDataUpdated(int tabIndex);
  }

  public RequestsFragment addOnDataUpdatedListener(OnDataUpdatedListener onDataUpdatedListener) {
    this.onDataUpdatedListener = onDataUpdatedListener;
    return this;
  }
}
