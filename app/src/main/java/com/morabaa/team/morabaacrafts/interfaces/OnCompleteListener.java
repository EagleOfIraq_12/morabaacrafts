package com.morabaa.team.morabaacrafts.interfaces;

import java.util.List;

public interface OnCompleteListener<T> {
      
      void onComplete(List<T> ts);
      
      void onError(String error);
}