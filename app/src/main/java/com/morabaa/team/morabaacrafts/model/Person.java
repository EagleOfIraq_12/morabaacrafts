package com.morabaa.team.morabaacrafts.model;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.morabaa.team.morabaacrafts.interfaces.ApiModel;
import com.morabaa.team.morabaacrafts.interfaces.OnCompleteListener;
import com.morabaa.team.morabaacrafts.utils.HttpRequest;
import com.morabaa.team.morabaacrafts.utils.LocalData;
import com.morabaa.team.morabaacrafts.utils.Utils;

import java.lang.reflect.Type;
import java.util.List;

import javax.annotation.Nullable;

@Entity
public class Person implements ApiModel<Person> {

  private static final String CLASS_ROOT = Utils.API_ROOT + "Person/";
  public static final String CURRENT_PERSON = "CurrentPerson";
  public static int CURRENT_USER_TYPE = 0;
  // region properties
  @PrimaryKey private long id;
  private String name;
  private String location;
  private String address;
  @Ignore private Governate governate;
  private String phoneNumber;
  private String password;
  private int age;
  @Ignore private MediaItem mediaItem;
  @Nullable private String mediaItemUrl;
  private boolean newRecord;
  private boolean approved;
  private boolean available;
  @Ignore private List<Rate> rates;
  @Ignore private Rate rate;
  @Nullable private float rating = 0.0F;
  @Ignore private List<Craft> crafts;
  @Ignore private List<Request> receivedRequests;
  @Ignore private List<Request> sentRequests;
  @Ignore private List<Work> works;

  // endregion

  public Person() {}

  public static Type getClassType() {
    return new TypeToken<List<Person>>() {}.getType();
  }

  // region getters and setters
  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getLocation() {
    return location;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  public Governate getGovernate() {
    return governate;
  }

  public void setGovernate(Governate governate) {
    this.governate = governate;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public MediaItem getMediaItem() {
    return mediaItem;
  }

  public void setMediaItem(MediaItem mediaItem) {
    this.mediaItem = mediaItem;
  }

  @Nullable
  public String getMediaItemUrl() {
    return mediaItemUrl == null ? getMediaItem().getUrl() : mediaItemUrl;
  }

  public void setMediaItemUrl(@Nullable String mediaItemUrl) {
    this.mediaItemUrl = mediaItemUrl == null ? getMediaItem().getUrl() : mediaItemUrl;
  }

  public List<Request> getSentRequests() {
    return sentRequests;
  }

  public void setSentRequests(List<Request> sentRequests) {
    this.sentRequests = sentRequests;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public boolean isNewRecord() {
    return newRecord;
  }

  public void setNewRecord(boolean newRecord) {
    this.newRecord = newRecord;
  }

  public List<Craft> getCrafts() {
    return crafts;
  }

  public void setCrafts(List<Craft> crafts) {
    this.crafts = crafts;
  }

  public List<Request> getReceivedRequests() {
    return receivedRequests;
  }

  public void setReceivedRequests(List<Request> receivedRequests) {
    this.receivedRequests = receivedRequests;
  }

  public boolean isApproved() {
    return approved;
  }

  public void setApproved(boolean approved) {
    this.approved = approved;
  }

  public boolean isAvailable() {
    return available;
  }

  public void setAvailable(boolean available) {
    this.available = available;
  }

  public float getRating() {
    return rate == null ? rating : rate.getRating();
  }

  public void setRating(float rating) {
    this.rating = rating;
  }

  public List<Work> getWorks() {
    return works;
  }

  public void setWorks(List<Work> works) {
    this.works = works;
  }

  public List<Rate> getRates() {
    return rates;
  }

  public void setRates(List<Rate> rates) {
    this.rates = rates;
  }

  public Rate getRate() {
    return rate;
  }

  public void setRate(Rate rate) {
    this.rate = rate;
  }

  public void login(String tag, OnCompleteListener<Person> onCompleteListener) {
    HttpRequest.getInstance()
        .addRequest(
            CLASS_ROOT + "login",
            new GsonBuilder()
                .serializeSpecialFloatingPointValues()
                .create()
                .toJson(this, Person.class),
            getClassType(),
            tag,
            onCompleteListener);
  }

  public void isRegistered(String tag, OnCompleteListener<Boolean> onCompleteListener) {
    HttpRequest.getInstance()
        .addRequest(
            CLASS_ROOT + "isRegistered",
            new GsonBuilder()
                .serializeSpecialFloatingPointValues()
                .create()
                .toJson(this, Person.class),
            new TypeToken<List<Boolean>>() {}.getType(),
            tag,
            onCompleteListener);
  }

  public void getFavorites(String tag, OnCompleteListener<Person> onCompleteListener) {
    HttpRequest.getInstance()
        .addRequest(
            CLASS_ROOT + "getFavorites",
            new GsonBuilder()
                .serializeSpecialFloatingPointValues()
                .create()
                .toJson(this, this.getClass()),
            Person.getClassType(),
            tag,
            onCompleteListener);
  }

  @Override
  public void getAll(String tag, OnCompleteListener<Person> onCompleteListener) {
    HttpRequest.getInstance()
        .addRequest(
            CLASS_ROOT + "getAll",
            new GsonBuilder()
                .serializeSpecialFloatingPointValues()
                .create()
                .toJson(this, Person.class),
            getClassType(),
            tag,
            onCompleteListener);
  }

  public void getById(String tag, OnCompleteListener<Person> onCompleteListener) {
    HttpRequest.getInstance()
        .addRequest(
            CLASS_ROOT + "getById",
            new GsonBuilder()
                .serializeSpecialFloatingPointValues()
                .create()
                .toJson(this, Person.class),
            getClassType(),
            tag,
            onCompleteListener);
  }

  public void getLast20NotIn(String tag, OnCompleteListener<Person> onCompleteListener) {
    HttpRequest.getInstance()
        .addRequest(
            CLASS_ROOT + "getLast20NotIn",
            new GsonBuilder()
                .serializeSpecialFloatingPointValues()
                .create()
                .toJson(this, Person.class),
            getClassType(),
            tag,
            onCompleteListener);
  }

  @Override
  public void push(String tag, OnCompleteListener<Person> onCompleteListener) {}

  @Override
  public void update(String tag, OnCompleteListener<Person> onCompleteListener) {
    HttpRequest.getInstance()
        .addRequest(
            CLASS_ROOT + "update",
            new GsonBuilder().serializeSpecialFloatingPointValues().create().toJson(this),
            getClassType(),
            tag,
            onCompleteListener);
  }

  @Override
  public void remove(String tag, OnCompleteListener<Person> onCompleteListener) {}

  public void favorite(
      String tag, Person favorited, OnCompleteListener<Person> onCompleteListener) {
    HttpRequest.getInstance()
        .addRequest(
            CLASS_ROOT + "favorite",
            "{\"favoriterId\":" + this.getId() + "," + "\"favoritedId\":" + favorited.getId() + "}",
            getClassType(),
            tag,
            onCompleteListener);
  }

  public void register(String tag, OnCompleteListener<Person> onCompleteListener) {
    HttpRequest.getInstance()
        .addRequest(
            CLASS_ROOT + "register",
            new GsonBuilder()
                .serializeSpecialFloatingPointValues()
                .create()
                .toJson(this, Person.class),
            getClassType(),
            tag,
            onCompleteListener);
  }

  public static Person fromJson(String personString) {
    return new GsonBuilder()
        .serializeSpecialFloatingPointValues()
        .create()
        .fromJson(personString, Person.class);
  }

  public static Person getCurrentPerson() {
    return new GsonBuilder()
        .serializeSpecialFloatingPointValues()
        .create()
        .fromJson(LocalData.getInstance().get(Person.CURRENT_PERSON), Person.class);
  }
  // endregion

  @Dao
  public interface PersonDuo {

    @Query("SELECT * FROM Person")
    List<Person> PEOPLE();

    @Query("SELECT * FROM Person WHERE id=:id")
    List<Person> byId(long id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Person... people);

    @Delete
    void delete(Person... people);

    @Update
    void Update(Person... people);

    @Query("SELECT COUNT(id) FROM Person")
    int count();
  }
}
