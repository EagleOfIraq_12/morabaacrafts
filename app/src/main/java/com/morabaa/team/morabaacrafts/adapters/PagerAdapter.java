package com.morabaa.team.morabaacrafts.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

/**
 * Created by eagle on 2/19/2018.
 */

public class PagerAdapter extends FragmentStatePagerAdapter {

    private List<Fragment> fragments;

    public PagerAdapter(FragmentManager fragmentManager,
                        List<Fragment> fragments) {
        super(fragmentManager);
        this.fragments = fragments;
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }
}