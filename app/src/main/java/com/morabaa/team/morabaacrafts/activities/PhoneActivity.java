package com.morabaa.team.morabaacrafts.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.FirebaseException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.gson.GsonBuilder;
import com.morabaa.team.morabaacrafts.R;
import com.morabaa.team.morabaacrafts.fragments.ConfirmPhoneFragment;
import com.morabaa.team.morabaacrafts.fragments.LoginFragment;
import com.morabaa.team.morabaacrafts.interfaces.OnCompleteListener;
import com.morabaa.team.morabaacrafts.model.Person;
import com.morabaa.team.morabaacrafts.utils.HttpRequest;
import com.morabaa.team.morabaacrafts.utils.LocalData;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class PhoneActivity extends AppCompatActivity
    implements LoginFragment.OnLoginListener, ConfirmPhoneFragment.OnPhoneConfirmListener {

  public static final String PHONE_NUMBER = "phoneNumber";

  private String verificationId;
  private ProgressBar progressBarWait;
  private String register;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_login);
    progressBarWait = findViewById(R.id.progressBarWait);
    String phoneNumber = getIntent().getStringExtra("phoneNumber");
    register = getIntent().getStringExtra("register");
    register = null;
    if (register != null) {
      onLogin(phoneNumber);
      return;
    }
    getSupportFragmentManager()
        .beginTransaction()
        .replace(R.id.loginFrame, LoginFragment.newInstance(phoneNumber))
        .addToBackStack(null)
        .commit();
  }

  public void signUp(View view) {
    startActivity(new Intent(this, SignUpActivity.class));
  }

  private void startPhoneNumberVerification(String phoneNumber) {
    progressBarWait.setVisibility(View.VISIBLE);
    if (phoneNumber.length() < 10) {
      Toast.makeText(getApplicationContext(), "يرجى ادخال رقم هاتف صالح...", Toast.LENGTH_LONG)
          .show();
      return;
    }
    phoneNumber = "+964" + phoneNumber.substring(phoneNumber.length() - 10, phoneNumber.length());
    String finalPhoneNumber = phoneNumber;
    PhoneAuthProvider.getInstance()
        .verifyPhoneNumber(
            phoneNumber, // Phone number to verify
            60, // Timeout duration
            TimeUnit.SECONDS, // Unit of timeout
            this, // Activity (for callback binding)
            new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
              @Override
              public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
                signInWithPhoneAuthCredential(phoneAuthCredential);
                progressBarWait.setVisibility(View.GONE);
              }

              @Override
              public void onVerificationFailed(FirebaseException e) {
                System.out.println("e = [" + e.getMessage() + "]");
                Toast.makeText(
                        getApplicationContext(),
                        "خطأ في العمليه الرجاء التأكد من اتصال الانترنت",
                        Toast.LENGTH_LONG)
                    .show();
                progressBarWait.setVisibility(View.GONE);
              }

              @Override
              public void onCodeSent(String vId, PhoneAuthProvider.ForceResendingToken token) {
                // The SMS verification code has been sent to the provided phone number, we
                // now need to ask the user to enter the code and then construct a credential
                // by combining the code with a verification ID.
                System.out.println("LoginonCodeSent:" + vId);
                // Save verification ID and resending token so we can use them later
                verificationId = vId;
                getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.loginFrame, ConfirmPhoneFragment.newInstance(finalPhoneNumber))
                    .addToBackStack(null)
                    .commit();
                progressBarWait.setVisibility(View.GONE);
                //        mResendToken = token;
              }
            }); // OnVerificationStateChangedCallbacks
    // [END start_phone_auth]
  }

  private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
    FirebaseAuth.getInstance()
        .signInWithCredential(credential)
        .addOnCompleteListener(
            this,
            task -> {
              if (task.isSuccessful()) {
                // Sign in success, update UI with the signed-in user's information
                FirebaseUser user = task.getResult().getUser();
                System.out.println("Login signInWithCredential:success UID= " + user.getUid());
                onConfirmCompleted();
              } else {
                Toast.makeText(
                        getBaseContext(), "خطاء في المصادقة حاول مرة اخرى", Toast.LENGTH_SHORT)
                    .show();
                System.out.println("Login signInWithCredential:failure " + task.getException());
                if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                  // The verification code entered was invalid
                  Toast.makeText(
                          getBaseContext(),
                          "رمز التاكيد الذي ادخلته خاطئ يرجى التاكد",
                          Toast.LENGTH_SHORT)
                      .show();
                }
              }
            });
  }

  Person currentPerson = null;

  private void onConfirmCompleted() {
    if (register != null) {
      Intent intent = new Intent(this, RegisterActivity.class);
      intent.putExtra("phoneNumber", currentPerson.getPhoneNumber());
      intent.putExtra("register", "register");
      startActivity(intent);
      finish();
      return;
    }
    LocalData.getInstance()
        .add(
            Person.CURRENT_PERSON,
            new GsonBuilder()
                .serializeSpecialFloatingPointValues()
                .create()
                .toJson(currentPerson, Person.class));
    Intent mainIntent = new Intent(PhoneActivity.this, MainActivity.class);
    startActivity(mainIntent);
    finish();
  }

  private void userLoginData(String phoneNumber) {
    Person person = new Person();
    person.setPhoneNumber(phoneNumber);
    if (register != null) {
      currentPerson = new Person();
      currentPerson.setPhoneNumber(phoneNumber);
      startPhoneNumberVerification(phoneNumber);
      return;
    }
    person.login(
        "PhoneActivity",
        new OnCompleteListener<Person>() {
          @Override
          public void onComplete(List<Person> people) {
            currentPerson = people.get(0);
            if (currentPerson.getName() != null) {
              if (test) {
                onConfirmCompleted();
              }
              startPhoneNumberVerification(currentPerson.getPhoneNumber());
            } else {
              Toast.makeText(
                      PhoneActivity.this,
                      "لا يوجد حساب مرتبط بالرقم " + currentPerson.getPhoneNumber(),
                      Toast.LENGTH_SHORT)
                  .show();
              getSupportFragmentManager()
                  .beginTransaction()
                  .replace(R.id.loginFrame, LoginFragment.newInstance(phoneNumber))
                  .addToBackStack(null)
                  .commit();
            }
          }

          @Override
          public void onError(String error) {
            System.out.println(error);
            getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.loginFrame, LoginFragment.newInstance(phoneNumber))
                .addToBackStack(null)
                .commit();
            Toast.makeText(PhoneActivity.this, "خطأ في العمليه ...", Toast.LENGTH_SHORT).show();
          }
        });
  }

  @Override
  public void onLogin(String phoneNumber) {
    if (phoneNumber.equals("1234")) {
      startActivity(new Intent(this, AdminActivity.class));
      finish();
    } else if (phoneNumber.endsWith(".")) {
      phoneNumber = phoneNumber.substring(0, phoneNumber.length() - 1);
      test = true;
    }
    userLoginData(phoneNumber);
  }

  private boolean test = false;

  @Override
  public void onConfirm(String confirmCode) {
    PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, confirmCode);
    signInWithPhoneAuthCredential(credential);
    progressBarWait.setVisibility(View.VISIBLE);
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    HttpRequest.getInstance().removeRequest("PhoneActivity");
  }
}
