package com.morabaa.team.morabaacrafts.activities;

import android.app.Activity;
import android.os.Bundle;

import com.morabaa.team.morabaacrafts.R;


public class FullScreenVideoActivity extends Activity /*implements EventListener*/ {

    // Saved instance state keys.
  /*  private static final String KEY_TRACK_SELECTOR_PARAMETERS = "track_selector_parameters";
    private static final String KEY_WINDOW = "window";
    private static final String KEY_POSITION = "position";
    private static final String KEY_AUTO_PLAY = "auto_play";
    public static String AD_TAG_URI_EXTRA = "url";
    private PlayerView playerView;
    private SimpleExoPlayer player;
    private MediaSource mediaSource;
    private long currentTime = 0;
    private boolean startAutoPlay;
    private int startWindow;
    private long startPosition;
    private DefaultTrackSelector.Parameters trackSelectorParameters;

    private String stringUrl;*/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_full_screen_video);
       /* stringUrl = getIntent().getStringExtra(AD_TAG_URI_EXTRA);
        playerView = findViewById(R.id.playerView);*/
//        if (savedInstanceState != null ) {
//            trackSelectorParameters = savedInstanceState
//                    .getParcelable(KEY_TRACK_SELECTOR_PARAMETERS);
//            startAutoPlay = savedInstanceState.getBoolean(KEY_AUTO_PLAY);
//            startWindow = savedInstanceState.getInt(KEY_WINDOW);
//            startPosition = savedInstanceState.getLong(KEY_POSITION);
//        } else {
//            trackSelectorParameters = new DefaultTrackSelector.ParametersBuilder()
//                    .build();
//            clearStartPosition();
//        }
//        initVideo();

    }

  /*  private boolean initVideo() {
        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory =
                new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);

        player = ExoPlayerFactory
                .newSimpleInstance(this, trackSelector);
        playerView.setPlayer(player);
        mediaSource = new ExtractorMediaSource.Factory(
                new CacheDataSourceFactory(
                        this,
                        1024L * 1024L * 1024L,
                        50L * 1024L * 1024L
                )
        ).createMediaSource(Uri.parse(
                AWSS3Storage.getReference(stringUrl)
        ));

        player.setPlayWhenReady(true);
        player.seekTo(currentTime);
        player.addListener(this);
        playerView.setOnLongClickListener(view -> initVideo());
        player.prepare(mediaSource);

        return true;
    }

    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {
    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups,
                                TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

        System.out.println("FullScreenVideoActivity" +
                playWhenReady + "  " + playbackState);
    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {
        System.out.println(error.getLocalizedMessage());
        initVideo();
        player.release();
        player.prepare(mediaSource);
    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {
        currentTime = player.getCurrentPosition();
    }

    private void releasePlayer() {
        if (player != null) {
            updateStartPosition();
            player.release();
            player = null;
            mediaSource = null;
        }
    }

    private void clearStartPosition() {
        startAutoPlay = true;
        startWindow = C.INDEX_UNSET;
        startPosition = C.TIME_UNSET;
    }

    private void updateStartPosition() {
        if (player != null) {
            startAutoPlay = player.getPlayWhenReady();
            startWindow = player.getCurrentWindowIndex();
            startPosition = Math.max(0, player.getContentPosition());
        }
    }
*/

//    @Override
//    public void onStart() {
//        super.onStart();
//        player.prepare(mediaSource);
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//
//    }
//
//    @Override
//    public void onPause() {
//        super.onPause();
//        releasePlayer();
//
//    }
//
//    @Override
//    public void onStop() {
//        super.onStop();
//        if (Util.SDK_INT > 23) {
//            releasePlayer();
//        }
//    }

//    @Override
//    public void onDestroy() {
//        releasePlayer();
//        super.onDestroy();
//    }

//    @Override
//    public void onSaveInstanceState(Bundle outState) {
//        updateStartPosition();
//        outState.putParcelable(KEY_TRACK_SELECTOR_PARAMETERS, trackSelectorParameters);
//        outState.putBoolean(KEY_AUTO_PLAY, startAutoPlay);
//        outState.putInt(KEY_WINDOW, startWindow);
//        outState.putLong(KEY_POSITION, startPosition);
//    }

}