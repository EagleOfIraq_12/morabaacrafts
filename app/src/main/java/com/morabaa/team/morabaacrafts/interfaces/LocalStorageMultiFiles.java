package com.morabaa.team.morabaacrafts.interfaces;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import java.util.List;

public class LocalStorageMultiFiles {
      
      private static LocalStorageMultiFiles instance;
      private OnFilesReceivedListener onFilesReceivedListener;
      
      public LocalStorageMultiFiles(Context ctx, String... fileTypes) {
            Intent intent = new Intent(ctx, LocalStorageMultiFilesActivity.class);
            intent.putExtra("reqCode", 77);
            intent.putExtra("types", fileTypes);
            ctx.startActivity(intent);
            instance = this;
      }
      
      public static LocalStorageMultiFiles getInstance() {
            return instance;
      }
      
      public static void releaseInstance() {
            LocalStorageMultiFilesActivity.getInstance().finish();
            instance = null;
      }
      
      public OnFilesReceivedListener getOnFilesReceivedListener() {
            return onFilesReceivedListener;
      }
      
      public LocalStorageMultiFiles setOnFilesReceivedListener(
            OnFilesReceivedListener onFilesReceivedListener) {
            this.onFilesReceivedListener = onFilesReceivedListener;
            return this;
      }
      
      public interface OnFilesReceivedListener {
            
            void onFileReceived(List<Uri> uris, List<String> names, String ext, String type);
            
            void onFileError(String error);
      }
      
      public static final class FILE_TYPES {
            
            public static final String ALL = "*/* ";
            public static final String IMAGES = "image/* ";
            public static final String VIDEOS = "video/* ";
      }
}
