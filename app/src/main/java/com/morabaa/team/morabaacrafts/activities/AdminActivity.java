package com.morabaa.team.morabaacrafts.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.asksira.bsimagepicker.BSImagePicker;
import com.bumptech.glide.Glide;
import com.morabaa.team.morabaacrafts.R;
import com.morabaa.team.morabaacrafts.adapters.CraftsAdapter;
import com.morabaa.team.morabaacrafts.interfaces.OnCompleteListener;
import com.morabaa.team.morabaacrafts.model.Craft;
import com.morabaa.team.morabaacrafts.model.MediaItem;
import com.morabaa.team.morabaacrafts.utils.AWSS3Storage;

import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.glide.transformations.BlurTransformation;

import static com.bumptech.glide.request.RequestOptions.bitmapTransform;

public class AdminActivity extends AppCompatActivity
        implements
        BSImagePicker.OnSingleImageSelectedListener {

    private ImageView imgCraftImage;
    private EditText txtCraftName;
    private RecyclerView recyclerViewCrafts;
    private CraftsAdapter craftsAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
        imgCraftImage = findViewById(R.id.imgCraftImage);
        txtCraftName = findViewById(R.id.txtCraftName);
        recyclerViewCrafts = findViewById(R.id.recyclerViewCrafts);

        craftsAdapter = new CraftsAdapter(this, new ArrayList<Craft>());
        craftsAdapter.setOnCraftClickedListener(craft -> {

        });
        recyclerViewCrafts.setLayoutManager(
                new GridLayoutManager(this, 3, GridLayoutManager.VERTICAL, false));
        recyclerViewCrafts.setAdapter(craftsAdapter);

        new Craft().getList(this.getClass().getName(),
                new OnCompleteListener<Craft>() {
                    @Override
                    public void onComplete(List<Craft> crafts) {
                        craftsAdapter.setCrafts(crafts);
                    }

                    @Override
                    public void onError(String error) {
                    }
                });

    }

    private Craft craft = new Craft();

    public void upload(View view) {
        craft.setName(txtCraftName.getText().toString());
        if (craft.getMediaItem() != null) {
            craft.push("", new OnCompleteListener<Craft>() {
                @Override
                public void onComplete(List<Craft> crafts) {
                    startActivity(
                            new Intent(AdminActivity.this, AdminActivity.class)
                    );
                    Toast.makeText(AdminActivity.this, "تمت العمليه بنجاح :) .", Toast.LENGTH_SHORT).show();
                    finish();
                }

                @Override
                public void onError(String error) {
                    if (error.toLowerCase().contains("boolean")) {
                        startActivity(
                                new Intent(AdminActivity.this, AdminActivity.class)
                        );
                        Toast.makeText(AdminActivity.this, "تمت العمليه بنجاح :) .", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }
            });
        } else {
            Toast.makeText(this, "الرجاء اختيار صوره.", Toast.LENGTH_SHORT).show();
        }

    }

    public void getImage(View view) {
        BSImagePicker singleSelectionPicker = new BSImagePicker.Builder(
                "com.morabaa.team.morabaacrafts.fileprovider")
//                .setMaximumDisplayingImages(
//                        240) //Default: Integer.MAX_VALUE. Don't worry about performance :)
                .setSpanCount(3) //Default: 3. This is the number of columns
                .setGridSpacing(com.asksira.bsimagepicker.Utils
                        .dp2px(2)) //Default: 2dp. Remember to pass in a value in pixel.
                .setPeekHeight(com.asksira.bsimagepicker.Utils.dp2px(
                        360)) //Default: 360dp. This is the initial height of the dialog.
                .hideCameraTile() //Default: show. Set this if you don't want user to take photo.
//                .hideGalleryTile() //Default: show. Set this if you don't want to further let user select from a gallery app. In such case, I suggest you to set maximum     displaying    images to Integer.MAX_VALUE.
                .build();
        singleSelectionPicker.show(getSupportFragmentManager(), "picker");


    }

    @Override
    public void onSingleImageSelected(Uri uri) {
        Glide.with(AdminActivity.this)
                .load(uri)
                .apply(bitmapTransform(
                        new BlurTransformation(10, 3)))
                .into(imgCraftImage);

        AWSS3Storage.getInstance(AdminActivity.this).uploadMini(
                uri, new AWSS3Storage.OnUploadProgressListener() {
                    @Override
                    public void onProgress(
                            double progress) {
                    }

                    @Override
                    public void onSuccess(String fileName) {
                        Glide.with(AdminActivity.this)
                                .load(uri)
                                .into(imgCraftImage);
                        MediaItem mediaItem = new MediaItem();
                        mediaItem.setUrl(fileName);
                        craft.setMediaItem(mediaItem);
                    }

                    @Override
                    public void onFailure(String error) {

                    }
                }
        );
    }

    @Override
    public void onBackPressed() {
        startActivity(
                new Intent(AdminActivity.this, PhoneActivity.class)
        );
        finish();
    }

}
