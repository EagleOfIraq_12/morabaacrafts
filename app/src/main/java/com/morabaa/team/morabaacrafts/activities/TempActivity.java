package com.morabaa.team.morabaacrafts.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.morabaa.team.morabaacrafts.R;

public class TempActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temp);
    }
}
