package com.morabaa.team.morabaacrafts.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.morabaa.team.morabaacrafts.interfaces.ApiModel;
import com.morabaa.team.morabaacrafts.interfaces.OnCompleteListener;
import com.morabaa.team.morabaacrafts.utils.HttpRequest;
import com.morabaa.team.morabaacrafts.utils.Utils;

import java.lang.reflect.Type;
import java.util.List;

@Entity
public class Notification implements ApiModel<Notification> {

    private static final String CLASS_ROOT = Utils.API_ROOT + "Notification/";

    private static Type getClassType(int... parms) {
        if (parms.length > 0) {
            return Notification.class;
        }
        return new TypeToken<List<Notification>>() {
        }.getType();
    }


    @PrimaryKey
    private long id;
    private String title;
    private String body;
    private String action;
    private int destination;
    @Ignore
    private Person person;
    private long timeStamp;
    private String parms;

    public Notification() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public int getDestination() {
        return destination;
    }

    public void setDestination(int destination) {
        this.destination = destination;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getParms() {
        return parms;
    }

    public void setParms(String parms) {
        this.parms = parms;
    }

    @Override
    public void getAll(String tag, OnCompleteListener<Notification> onCompleteListener) {

    }

    public void getAll(String tag, int pages, OnCompleteListener<Notification> onCompleteListener) {

        HttpRequest.getInstance()
                .addRequest(CLASS_ROOT + "getAll?destination=0&pages=" + pages,
                        new GsonBuilder()
                                .serializeSpecialFloatingPointValues()
                                .create()
                                .toJson(this.person, Person.class),
                        getClassType(),
                        tag, onCompleteListener);
    }

    public void getNewCount(String tag, OnCompleteListener<Integer> onCompleteListener) {

        HttpRequest.getInstance()
                .addRequest(CLASS_ROOT + "getNewCount",
                        new GsonBuilder()
                                .serializeSpecialFloatingPointValues()
                                .create()
                                .toJson(this.person, Person.class),
                        new TypeToken<List<Integer>>() {
                        }.getType(),
                        tag, onCompleteListener);
    }

    public void setSeen(String tag, OnCompleteListener<Integer> onCompleteListener) {
        HttpRequest.getInstance()
                .addRequest(CLASS_ROOT + "setSeen",
                        new GsonBuilder()
                                .serializeSpecialFloatingPointValues()
                                .create()
                                .toJson(this, Notification.class),
                        new TypeToken<List<Integer>>() {
                        }.getType(),
                        tag, onCompleteListener);
    }

    @Override
    public void push(String tag, OnCompleteListener<Notification> onCompleteListener) {

    }

    @Override
    public void update(String tag, OnCompleteListener<Notification> onCompleteListener) {

    }

    @Override
    public void remove(String tag, OnCompleteListener<Notification> onCompleteListener) {
        HttpRequest.getInstance()
                .addRequest(CLASS_ROOT + "remove",
                        new GsonBuilder()
                                .serializeSpecialFloatingPointValues()
                                .create()
                                .toJson(this, Notification.class),
                        getClassType(),
                        tag, onCompleteListener);
    }

    public class Destination {
        public static final int android = 0;
        public static final int ios = 1;
    }
}
