package com.morabaa.team.morabaacrafts.utils;

import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.Nullable;

import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;

import java.io.File;

public class FireStorage {

    private static FireStorage instance;
    private Context ctx;
    private StorageReference storageRef;
    public static String root = Environment.getExternalStorageDirectory().toString() + "/images/";

    public FireStorage(Context ctx) {
        this.ctx = ctx;
//            databaseRef = FirebaseDatabase.getInstance().getReference();
        storageRef = FirebaseStorage.getInstance().getReference();
        instance = this;
    }

    public static FireStorage getInstance(@Nullable Context ctx) {
        instance = instance == null ? new FireStorage(ctx) : instance;
        return instance;
    }


    public StorageTask<UploadTask.TaskSnapshot> upload(Uri uri,
                                                       final OnUploadProgressListener onUploadProgressListener) {

        String path = ImageUtils.saveLocalFile(ctx, uri);
        path = ImageUtils.compress(path, 1024L * 300L);
        uri = Uri.fromFile(new File(path));

        return  storageRef.child("images/" + uri.getLastPathSegment()).putFile(uri)
                .addOnSuccessListener(taskSnapshot ->
                        onUploadProgressListener.onSuccess(
                                taskSnapshot.getMetadata().getName()
                        ))
                .addOnFailureListener(exception ->
                        onUploadProgressListener.onFailure(exception.getMessage())).addOnProgressListener(
                        taskSnapshot -> {
                            long bytesTransferred = taskSnapshot.getBytesTransferred();
                            long totalByteCount = taskSnapshot.getTotalByteCount();
                            double p = (
                                    100.0D * bytesTransferred /
                                            totalByteCount
                            );
                            onUploadProgressListener.onProgress(p);
                            System.out.println(
                                    "BytesTransferred : " + bytesTransferred + "\t" +
                                            "TotalByteCount : " + totalByteCount + "\t" + p +
                                            " %"
                            );
                        });

    }

    public StorageTask<FileDownloadTask.TaskSnapshot> download(String fileName,
                                                               final OnDownloadProgressListener onDownloadProgressListener) {
        File localFile;
        try {
            localFile = File.createTempFile("images", "jpg");
            final File finalLocalFile = localFile;
            StorageReference child = storageRef.child("images/" + fileName);
            return child.getFile(localFile)
                    .addOnSuccessListener(
                            taskSnapshot -> {
                                Uri uri = Uri.fromFile(finalLocalFile);
                                onDownloadProgressListener.onSuccess(uri);
                            })
                    .addOnFailureListener(exception -> onDownloadProgressListener.onFailure(exception.getMessage())).addOnProgressListener(
                            taskSnapshot -> {
                                long bytesTransferred = taskSnapshot
                                        .getBytesTransferred();
                                long totalByteCount = taskSnapshot.getTotalByteCount();
                                double p = (
                                        100.0d * bytesTransferred /
                                                totalByteCount
                                );
                                onDownloadProgressListener.onProgress(p);
                            });
        } catch (Exception e) {
            onDownloadProgressListener.onFailure(e.getMessage());
        }
        return null;
    }

    public interface OnUploadProgressListener {

        void onProgress(double progress);

        void onSuccess(String fileName);

        void onFailure(String error);
    }

    public interface OnDownloadProgressListener {

        void onProgress(double progress);

        void onSuccess(Uri uri);

        void onFailure(String error);
    }

    public static StorageReference getReference(String fileName) {
        return FirebaseStorage.getInstance().getReference()
                .child("images/" + fileName);
    }
}
