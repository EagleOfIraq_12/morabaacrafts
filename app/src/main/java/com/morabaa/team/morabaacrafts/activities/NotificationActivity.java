package com.morabaa.team.morabaacrafts.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.morabaa.team.morabaacrafts.R;
import com.morabaa.team.morabaacrafts.fragments.RequestsFragment;
import com.morabaa.team.morabaacrafts.model.Request;

public class NotificationActivity extends AppCompatActivity
    implements RequestsFragment.OnDataUpdatedListener {

  private static RequestsFragment.OnDataUpdatedListener instance;

  public static RequestsFragment.OnDataUpdatedListener getInstance() {
    return instance;
  }

  public static void setInstance(RequestsFragment.OnDataUpdatedListener instance) {
    NotificationActivity.instance = instance;
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_notification);
    setInstance(this);
    if (getIntent().getStringExtra("parms") != null) {
      responseToNotification(getIntent().getStringExtra("parms"));
    }
  }

  public void responseToNotification(String parms) {
    long id = Integer.parseInt(parms.split(",")[0]);
    int type = Integer.parseInt(parms.split(",")[1]);
    Request request = new Request();
    request.setId(id);
    getSupportFragmentManager()
        .beginTransaction()
        .add(R.id.frameNotification, RequestsFragment.newInstance(type, id))
        .addToBackStack(null)
        .commit();
  }

  @Override
  public void onBackPressed() {
    if (MainActivity.getInstance() == null) {
      startActivity(new Intent(this, MainActivity.class));
    }
    finish();
  }

  @Override
  public void onDataUpdated(int tabIndex) {
    startActivity(new Intent(this, MainActivity.class));
    finish();
  }
}
