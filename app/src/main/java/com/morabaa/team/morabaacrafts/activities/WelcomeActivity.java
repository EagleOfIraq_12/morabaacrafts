package com.morabaa.team.morabaacrafts.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

import com.morabaa.team.morabaacrafts.R;
import com.morabaa.team.morabaacrafts.model.Person;

import java.util.ArrayList;
import java.util.List;

public class WelcomeActivity extends AppCompatActivity {

  public static final int MULTIPLE_PERMISSIONS = 10; // code you want.

  @SuppressLint("NewApi")
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    requestWindowFeature(Window.FEATURE_NO_TITLE);
    getWindow()
        .setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    setContentView(R.layout.activity_welcome);
    setTitle("");
    if(true){
      Intent loginIntent = new Intent(WelcomeActivity.this, TempActivity.class);
      startActivity(loginIntent);
      finish();
    }
    openMain();

    //        if (checkPermissions()) {
    //        } else {
    //                  startActivity(
    //                        new Intent(this, WelcomeActivity.class)
    //                  );
    //        }
  }

  private void openMain() {
    new Handler()
        .postDelayed(
            () -> {
              if (Person.getCurrentPerson() != null) {
                Intent mainIntent = new Intent(WelcomeActivity.this, MainActivity.class);
                startActivity(mainIntent);
              } else {
                Intent loginIntent = new Intent(WelcomeActivity.this, PhoneActivity.class);
                startActivity(loginIntent);
              }
              finish();
            },
            100);
  }

  private boolean checkPermissions() {
    int result;
    List<String> listPermissionsNeeded = new ArrayList<>();
    String[] permissions = {
      Manifest.permission.CALL_PHONE,
      Manifest.permission.INTERNET,
      Manifest.permission.GET_ACCOUNTS,
      Manifest.permission.READ_CONTACTS,
      Manifest.permission.ACCESS_COARSE_LOCATION,
      Manifest.permission.ACCESS_FINE_LOCATION,
      Manifest.permission.WRITE_EXTERNAL_STORAGE,
      Manifest.permission.READ_PHONE_NUMBERS,
      Manifest.permission.READ_PHONE_STATE,
      Manifest.permission.READ_EXTERNAL_STORAGE,
      Manifest.permission.READ_PHONE_STATE,
      Manifest.permission.CAMERA,
      Manifest.permission.READ_SMS
    };

    //        CameraHelper.getInstance(WelcomeActivity.this, permissions[11], granted -> {
    //            System.out.println("Permission " + permissions[0] + " isGranted = [" + granted +
    // "]");
    //            openMain();
    //        });
    //

    return false;

    //        for (String p : permissions) {
    //            result = ContextCompat.checkSelfPermission(WelcomeActivity.this, p);
    //            if (result != PackageManager.PERMISSION_GRANTED) {
    //                listPermissionsNeeded.add(p);
    //            }
    //        }
    //
    //        if (!listPermissionsNeeded.isEmpty()) {
    //            ActivityCompat.requestPermissions(this,
    //                    listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),
    //                    MULTIPLE_PERMISSIONS);
    //            return false;
    //        }
    //        return true;
  }

  @Override
  public void onRequestPermissionsResult(
      int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
    switch (requestCode) {
      case MULTIPLE_PERMISSIONS:
        {
          if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // permissions granted.
            openMain();
          } else {
            StringBuilder permission = new StringBuilder();
            for (String per : permissions) {
              permission.append("\n").append(per);
            }
            openMain();
          }
        }
    }
  }

  @Override
  protected void onPostCreate(Bundle savedInstanceState) {
    super.onPostCreate(savedInstanceState);
  }
}
