package com.morabaa.team.morabaacrafts.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

public class LocalStorageFile {
      
      private static LocalStorageFile instance;
      private OnFileReceivedListener onFileReceivedListener;
      
      public LocalStorageFile(Context ctx, String... fileTypes) {
            Intent intent = new Intent(ctx, LocalStorageFileActivity.class);
            intent.putExtra("reqCode", 77);
            intent.putExtra("types", fileTypes);
            ctx.startActivity(intent);
            instance = this;
      }
      
      public static LocalStorageFile getInstance() {
            return instance;
      }
      
      public static void releaseInstance() {
            LocalStorageFileActivity.getInstance().finish();
            instance = null;
      }
      
      public OnFileReceivedListener getOnFileReceivedListener() {
            return onFileReceivedListener;
      }
      
      public LocalStorageFile setOnFileReceivedListener(
            OnFileReceivedListener onFileReceivedListener) {
            this.onFileReceivedListener = onFileReceivedListener;
            return this;
      }
      
      public interface OnFileReceivedListener {
            
            void onFileReceived(Uri uri, String name, String ext, String type);
            
            void onFileError(String error);
      }
      
      public static final class FILE_TYPES {
            
            public static final String ALL = "*/* ";
            public static final String IMAGES = "image/* ";
            public static final String VIDEOS = "video/* ";
      }
}
