package com.morabaa.team.morabaacrafts.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.google.gson.GsonBuilder;
import com.morabaa.team.morabaacrafts.R;
import com.morabaa.team.morabaacrafts.adapters.CraftMenAdapter;
import com.morabaa.team.morabaacrafts.interfaces.OnCompleteListener;
import com.morabaa.team.morabaacrafts.model.Person;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class FavoritesFragment extends Fragment {
    private ProgressBar bottomProgressBar;
    private CraftMenAdapter favoritesAdapter;

    public FavoritesFragment() {
        // Required empty public constructor
    }

    public static FavoritesFragment newInstance() {
        FavoritesFragment fragment = new FavoritesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_favorites, container, false);
    }

    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        RecyclerView recyclerViewFavorites = view.findViewById(R.id.recyclerViewFavorites);
        bottomProgressBar = view.findViewById(R.id.bottomProgressBar);

        bottomProgressBar.setVisibility(View.GONE);

        favoritesAdapter = new CraftMenAdapter(getContext(), new ArrayList<>());

        favoritesAdapter.setOnCraftManClickedListener(this::onCraftManClicked);

        recyclerViewFavorites.setLayoutManager(
                new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerViewFavorites.setAdapter(favoritesAdapter);
        load();

    }

    private void load() {
        bottomProgressBar.setVisibility(View.VISIBLE);
        Person.getCurrentPerson().getFavorites("", new OnCompleteListener<Person>() {
            @Override
            public void onComplete(List<Person> people) {
                favoritesAdapter.setPeople(people);
                bottomProgressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError(String error) {
                bottomProgressBar.setVisibility(View.GONE);
            }
        });
    }

    private void onCraftManClicked(Person person) {
        Bundle bundle = new Bundle();
        bundle.putString("craftManString",
                new GsonBuilder()
                        .serializeSpecialFloatingPointValues()
                        .create()
                        .toJson(person, Person.class)
        );
        assert getParentFragment() != null;
        getParentFragment().
                getChildFragmentManager()
                .beginTransaction()
                .add(
                        R.id.frameFavoritesContainer,
                        CraftManProfileFragment.newInstance(bundle),
                        "CraftManProfileFragment"
                )
                .addToBackStack(null)
                .commit();
    }

}
