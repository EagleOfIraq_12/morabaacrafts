package com.morabaa.team.morabaacrafts.bottomSheets;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RatingBar;

import com.google.gson.GsonBuilder;
import com.morabaa.team.morabaacrafts.R;
import com.morabaa.team.morabaacrafts.interfaces.OnCompleteListener;
import com.morabaa.team.morabaacrafts.model.Person;
import com.morabaa.team.morabaacrafts.model.Rate;
import com.morabaa.team.morabaacrafts.model.Request;

import java.util.List;

public class RateWorkBottomSheet extends BottomSheetDialogFragment {

  private Request request;
  private OnDoneListener onDoneListener;

  public void setOnDoneListener(OnDoneListener onDoneListener) {
    this.onDoneListener = onDoneListener;
  }

  @Nullable
  @Override
  public View onCreateView(
      @NonNull LayoutInflater inflater,
      @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    assert getArguments() != null;
    String requestString = getArguments().getString("requestString");
    request =
        new GsonBuilder()
            .serializeSpecialFloatingPointValues()
            .create()
            .fromJson(requestString, Request.class);
    View view = inflater.inflate(R.layout.dialog_rate_work_request_layout, container, false);
    view.findViewById(R.id.frameDialog).setBackgroundColor(Color.argb(0, 0, 0, 0));
    Button btnOk = view.findViewById(R.id.btnOk);
    Button btnCancel = view.findViewById(R.id.btnCancel);
    final EditText txtRequestCraftManRate = view.findViewById(R.id.txtRequestCraftManRate);
    final RatingBar ratingCraftManRating = view.findViewById(R.id.ratingReceiverRating);
    final ProgressBar progressBarUpdating = view.findViewById(R.id.progressBarUpdating);

    btnCancel.setOnClickListener(v -> dismiss());
    btnOk.setOnClickListener(
        view1 -> {
          progressBarUpdating.setVisibility(View.VISIBLE);
          Rate rate = new Rate();
          rate.setWork(request.getWork());
          rate.setComment(txtRequestCraftManRate.getText().toString());
          rate.setRatingAccuracy(ratingCraftManRating.getRating());
          rate.setRatingDate(ratingCraftManRating.getRating());
          rate.setRatingSpeed(ratingCraftManRating.getRating());
          rate.setRated(request.getReceiver());
          rate.setRater(Person.getCurrentPerson());
          rate.push(
              "",
              0,
              new OnCompleteListener<Rate>() {
                @Override
                public void onComplete(List<Rate> rates) {
                  progressBarUpdating.setVisibility(View.GONE);
                  onDoneListener.onDone(true);
                  dismiss();
                }

                @Override
                public void onError(String error) {
                  progressBarUpdating.setVisibility(View.GONE);
                  onDoneListener.onDone(false);
                  dismiss();
                }
              });
        });

    return view;
  }
}
