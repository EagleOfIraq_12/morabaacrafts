package com.morabaa.team.morabaacrafts.model;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.morabaa.team.morabaacrafts.interfaces.ApiModel;
import com.morabaa.team.morabaacrafts.interfaces.OnCompleteListener;
import com.morabaa.team.morabaacrafts.utils.HttpRequest;
import com.morabaa.team.morabaacrafts.utils.Utils;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.List;

@Entity
public class Work implements ApiModel<Work> {

    private static final String CLASS_ROOT = Utils.API_ROOT + "Work/";

    //region properties
    @PrimaryKey
    private long id;
    private String title;
    private String description;
    @Ignore
    private Person person;
    @Ignore
    private Request request;
    private String date;
    private long timeSpan;
    @Ignore
    private List<MediaItem> mediaItems;
    private long timeStamp;
    @Ignore
    private Rate rate;
    //endregion

    public Work() {
    }

    public static Type getClassType(int... parms) {
        if (parms.length > 0) {
            return Work.class;
        }
        return new TypeToken<List<Work>>() {
        }.getType();
    }

    //region getters and setters
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public long getTimeSpan() {
        date = "2018-07-16T18:24:06.6130181+03:00";
        //"012345678901234567890123456789012"
        int y = Integer.parseInt(date.substring(0, 3));
        int m = Integer.parseInt(date.substring(5, 6));
        int d = Integer.parseInt(date.substring(8, 9));

        int h = Integer.parseInt(date.substring(11, 12));
        int M = Integer.parseInt(date.substring(14, 15));
        int s = Integer.parseInt(date.substring(17, 18));

        int ms = Integer.parseInt(date.substring(20, 26));
        Date dateC = new Date(y - 1900, m - 1, d + 0, h + 0, M + 0, s + 0);
        System.out.println("cParsedDate: " + dateC.toString() + "\t" +
                "cDate: " + new Date().toString()
        );
        System.out.println(
                "cParsedDate: " + dateC.getYear() + " " + dateC.getMonth() + " " + dateC.getDay()
                        + " " + dateC.getHours() + " " + dateC.getMinutes() + " " + dateC
                        .getSeconds() + " " + "\t" +
                        "cDate: " + new Date().getYear() + " " + new Date().getMonth() + " "
                        + new Date().getDay() + " " + new Date().getHours() + " " + new Date()
                        .getMinutes() + " " + new Date().getSeconds()
        );

        return timeSpan;
    }

    public void setTimeSpan(long timeSpan) {
        this.timeSpan = timeSpan;
    }

    public List<MediaItem> getMediaItems() {
        return mediaItems;
    }

    public void setMediaItems(List<MediaItem> mediaItems) {
        this.mediaItems = mediaItems;
    }

    public Rate getRate() {
        return rate;
    }

    public void setRate(Rate rate) {
        this.rate = rate;
    }

    @Override
    public void getAll(String tag, OnCompleteListener<Work> onCompleteListener) {
        HttpRequest.getInstance()
                .addRequest(CLASS_ROOT + "getAll",
                        new GsonBuilder()
                                .serializeSpecialFloatingPointValues()
                                .create()
                                .toJson(this, getClassType(0)),
                        getClassType(), tag, onCompleteListener);
    }

    public void getById(String tag, OnCompleteListener<Work> onCompleteListener) {
        HttpRequest.getInstance()
                .addRequest(CLASS_ROOT + "getById",
                        new GsonBuilder()
                                .serializeSpecialFloatingPointValues()
                                .create()
                                .toJson(this, getClassType(0)),
                        getClassType(), tag, onCompleteListener);
    }

    public void getMostRecent(String tag, OnCompleteListener<Work> onCompleteListener) {
        HttpRequest.getInstance()
                .addRequest(CLASS_ROOT + "getMostRecent",
                        new GsonBuilder()
                                .serializeSpecialFloatingPointValues()
                                .create()
                                .toJson(this, getClassType(0)),
                        getClassType(), tag, onCompleteListener);
    }

    @Override
    public void push(String tag, OnCompleteListener<Work> onCompleteListener) {
        String gsn = new GsonBuilder()
                .serializeSpecialFloatingPointValues()
                .create().toJson(this, getClassType(0));
        HttpRequest.getInstance()
                .addRequest(CLASS_ROOT + "push",
                        gsn,
                        getClassType(), tag, onCompleteListener);
    }

    @Override
    public void update(String tag, OnCompleteListener<Work> onCompleteListener) {

    }

    @Override
    public void remove(String tag, OnCompleteListener<Work> onCompleteListener) {

    }


//endregion


    @Dao
    public interface WorkDuo {

        @Query("SELECT * FROM Work")
        List<Work> WORKS();

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        void insert(Work... works);

        @Delete
        void delete(Work... works);

        @Update
        void Update(Work... works);

        @Query("SELECT COUNT(id) FROM Work")
        int count();
    }
}