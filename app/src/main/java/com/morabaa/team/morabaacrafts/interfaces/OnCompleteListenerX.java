package com.morabaa.team.morabaacrafts.interfaces;

import java.util.List;

public interface OnCompleteListenerX<T> {
      
      void onComplete(List<T> ts);
     
      void onComplete(T t);
      
      void onError(String error);
}