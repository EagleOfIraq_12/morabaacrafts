package com.morabaa.team.morabaacrafts.activities;

import android.content.Intent;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;

import com.google.gson.GsonBuilder;
import com.morabaa.team.morabaacrafts.R;
import com.morabaa.team.morabaacrafts.adapters.WorksAdapter;
import com.morabaa.team.morabaacrafts.interfaces.OnCompleteListener;
import com.morabaa.team.morabaacrafts.model.Person;
import com.morabaa.team.morabaacrafts.model.MediaItem;
import com.morabaa.team.morabaacrafts.model.Work;

import java.util.ArrayList;
import java.util.List;

public class WorksActivity extends AppCompatActivity {

    private Person person;
    private RecyclerView recyclerCraftManWorks;
    private LinearLayout layoutAddRequest;

    @RequiresApi(api = VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_works);
        layoutAddRequest = findViewById(R.id.layoutAddRequest);
        layoutAddRequest = findViewById(R.id.layoutAddRequest);
        recyclerCraftManWorks = findViewById(R.id.recyclerCraftManWorks);
        String craftManString = getIntent().getStringExtra("craftManString");
        person = new GsonBuilder()
                .serializeSpecialFloatingPointValues()
                .create().fromJson(craftManString, Person.class);
        person = Person.fromJson(craftManString);
        setTitle(person.getName());

        android.support.v7.widget.Toolbar toolbar = findViewById(
                R.id.craftManActionBar);

        setSupportActionBar(toolbar);
        findViewById(R.id.btnAddRequestExit).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                layoutAddRequest.setVisibility(View.GONE);
            }
        });
        layoutAddRequest.setOnClickListener(view -> layoutAddRequest.setVisibility(View.GONE));


        final Work work = new Work();
        work.setPerson(person);

        work.getAll("", new OnCompleteListener<Work>() {
            @Override
            public void onComplete(List<Work> works) {
                initWorks(works);
            }

            @Override
            public void onError(String error) {
                List<Work> works = new ArrayList<>();
                for (int i = 0; i < 29; i++) {
                    final int finalI = i;
                    works.add(new Work() {{
                        setPerson(person);
                        setMediaItems(new ArrayList<>());
                        for (int i = 0; i <= 2; i++) {
                            final int finalI = i;
                            getMediaItems().add(
                                    new MediaItem() {{
                                        setId(finalI);
                                        setUrl(
                                                "https://dummyimage.com/2650x2000/a2fea3/8f24e6&text="
                                                        + work.getTitle() + finalI);
                                    }}
                            );
                        }
                        setTitle("Title " + finalI);
                    }});
                }

                initWorks(works);
            }
        });


    }

    private void initWorks(List<Work> works) {
        recyclerCraftManWorks.setLayoutManager(new GridLayoutManager(
                WorksActivity.this,
                2, GridLayoutManager.VERTICAL, false
        ));
        WorksAdapter worksAdapter = new WorksAdapter(
                WorksActivity.this,
                new ArrayList<Work>(),
                R.layout.work_layout
        );
        worksAdapter.setOnWorkClickedListener(work -> {
            Intent workActivityIntent = new Intent(
                    WorksActivity.this,
                    WorkDetailsActivity.class
            );
            workActivityIntent.putExtra("workString",
                    new GsonBuilder()
                            .serializeSpecialFloatingPointValues()
                            .create().toJson(work, Work.class));
            startActivity(workActivityIntent);
        });
        recyclerCraftManWorks.setAdapter(worksAdapter);

        worksAdapter.setWorks(works);
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
