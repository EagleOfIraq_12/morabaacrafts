package com.morabaa.team.morabaacrafts.adapters;


import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build.VERSION_CODES;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.morabaa.team.morabaacrafts.R;
import com.morabaa.team.morabaacrafts.model.Person;
import com.morabaa.team.morabaacrafts.utils.AWSS3Storage;

import java.util.List;

/**
 * Created by eagle on 10/21/2017.
 */

public class CraftMenAdapter extends RecyclerView.Adapter<CraftManHolder> {

    private List<Person> people;
    private Context ctx;
    private OnCraftManClickedListener onCraftManClickedListener;

    public CraftMenAdapter(Context ctx, List<Person> people) {
        this.people = people;
        this.ctx = ctx;

    }

    @Override
    public CraftManHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CardView view = (CardView) LayoutInflater.from(parent.getContext()).inflate(
                R.layout.craft_man_layout, parent, false);
        return new CraftManHolder(view);
    }

    @RequiresApi(api = VERSION_CODES.LOLLIPOP)
    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final CraftManHolder craftManHolder,
                                 @SuppressLint("RecyclerView") final int position) {

        craftManHolder.txtCraftManName.setText(people.get(position).getName());
        craftManHolder.txtCraftManAddress.setText(people.get(position).getAddress());
        craftManHolder.ratingCraftManRating
                .setRating(people.get(position).getRating());

        try {
            Glide.with(ctx)
                    .load(AWSS3Storage.getReference(
                            people.get(position).getMediaItem().getUrl()
                            )
                    )
                    .thumbnail(0.1f)
                    .into(craftManHolder.imgCraftMan);
        } catch (Exception e) {
        }


        craftManHolder.view.setOnClickListener(view -> onCraftManClickedListener.onCraftManClicked(
                people.get(craftManHolder.getAdapterPosition())
        ));
    }

    public void setOnCraftManClickedListener(
            OnCraftManClickedListener onCraftManClickedListener) {
        this.onCraftManClickedListener = onCraftManClickedListener;
    }

    @Override
    public int getItemCount() {
        return people.size();
    }

    public List<Person> getPeople() {
        return people;
    }

    public void setPeople(List<Person> people) {
        this.people = people;
        notifyDataSetChanged();
    }

    public interface OnCraftManClickedListener {

        void onCraftManClicked(Person person);
    }
}


class CraftManHolder extends RecyclerView.ViewHolder {

    CardView view;

    ImageView imgCraftMan;
    TextView txtCraftManName;
    TextView txtCraftManAddress;
    RatingBar ratingCraftManRating;

    CraftManHolder(View itemView) {
        super(itemView);
        this.view = (CardView) itemView;
        imgCraftMan = view.findViewById(R.id.imgReceiverImage);
        txtCraftManName = view.findViewById(R.id.txtReceiverName);
        txtCraftManAddress = view.findViewById(R.id.txtReceiverAddress);
        ratingCraftManRating = view.findViewById(R.id.ratingReceiverRating);


    }
}
