package com.morabaa.team.morabaacrafts.adapters;


import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build.VERSION_CODES;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import com.morabaa.team.morabaacrafts.R;
import com.morabaa.team.morabaacrafts.model.Request;
import java.util.List;

/**
 * Created by eagle on 10/21/2017.
 */

public class CraftManRequestsAdapter extends RecyclerView.Adapter<CraftManRequestHolder> {
      
      private List<Request> requests;
      private Context ctx;
      private OnRequestClickedListener onRequestClickedListener;
      
      public CraftManRequestsAdapter(Context ctx, List<Request> requests) {
            this.requests = requests;
            this.ctx = ctx;
      }
      
      @Override
      public CraftManRequestHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            CardView view = (CardView) LayoutInflater.from(parent.getContext()).inflate(
                  R.layout.requests_layout, parent, false);
            return new CraftManRequestHolder(view);
      }
      
      @RequiresApi(api = VERSION_CODES.LOLLIPOP)
      @SuppressLint("SetTextI18n")
      @Override
      public void onBindViewHolder(@NonNull final CraftManRequestHolder craftManRequestHolder,
            @SuppressLint("RecyclerView") final int position) {
            
            craftManRequestHolder.txtRequestPrice.setText(requests.get(position).getPrice() + "");
            
            craftManRequestHolder.view.setOnClickListener(new OnClickListener() {
                  @SuppressLint("RestrictedApi")
                  @Override
                  public void onClick(View view) {
                        onRequestClickedListener.onRequestClicked(
                              requests.get(craftManRequestHolder.getAdapterPosition())
                        );
                  }
            });
      }
      
      public void setOnRequestClickedListener(
            OnRequestClickedListener onRequestClickedListener) {
            this.onRequestClickedListener = onRequestClickedListener;
      }
      
      @Override
      public int getItemCount() {
            return requests.size();
      }
      
      public List<Request> getRequests() {
            return requests;
      }
      
      public void setRequests(List<Request> requests) {
            this.requests = requests;
            notifyDataSetChanged();
      }
      
      public interface OnRequestClickedListener {
            
            void onRequestClicked(Request request);
      }
}


class CraftManRequestHolder extends RecyclerView.ViewHolder {
      
      CardView view;
      
      TextView txtRequestPrice;
      
      CraftManRequestHolder(View itemView) {
            super(itemView);
            this.view = (CardView) itemView;
            
            txtRequestPrice = view.findViewById(R.id.txtRequestPrice);
//
      }
}
