package com.morabaa.team.morabaacrafts.adapters;

/**
 * Created by eagle on 1/22/2018.
 */

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.morabaa.team.morabaacrafts.R;
import com.morabaa.team.morabaacrafts.activities.FullScreenImageActivity;
import com.morabaa.team.morabaacrafts.activities.VideoPlayerActivity;
import com.morabaa.team.morabaacrafts.model.MediaItem;
import com.morabaa.team.morabaacrafts.utils.AWSS3Storage;

import java.util.List;


public class ImagesPagerAdapter extends PagerAdapter {

    Context ctx;
    LayoutInflater layoutInflater;
    List<MediaItem> mediaItems;

    public ImagesPagerAdapter(Context ctx, List<MediaItem> mediaItems) {
        this.ctx = ctx;
        this.mediaItems = mediaItems;
        layoutInflater = (LayoutInflater) this.ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return mediaItems.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        CardView page = (CardView) layoutInflater
                .inflate(R.layout.image_with_titel_layout, null);
        page.setOnClickListener(v -> {

            if (mediaItems.get(position).getUrl().toLowerCase().contains(".mp4")) {
                Intent imageIntent = new Intent(ctx,
                        VideoPlayerActivity.class);
                imageIntent.putExtra("url", mediaItems.get(position).getUrl());
                ctx.startActivity(imageIntent);
            } else {
                Intent intent = new Intent(ctx, FullScreenImageActivity.class);
                intent.putExtra("url", mediaItems.get(position).getUrl());
                ctx.startActivity(intent);

            }
        });
        ImageView imageView = page.findViewById(R.id.img);
        String url = mediaItems.get(position).getUrl();

        Glide.with(ctx)
                .load(AWSS3Storage.getReference(url))
                .thumbnail(0.1f)
                .into(imageView);


        container.addView(page);
        return page;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object page) {
        container.removeView((CardView) page);
    }
}