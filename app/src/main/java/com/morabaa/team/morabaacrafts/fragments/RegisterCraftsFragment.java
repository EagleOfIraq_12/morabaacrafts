package com.morabaa.team.morabaacrafts.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.morabaa.team.morabaacrafts.R;
import com.morabaa.team.morabaacrafts.activities.PhoneActivity;

public class RegisterCraftsFragment extends Fragment {

  private String phoneNumber;

  public RegisterCraftsFragment() {}

  public static RegisterCraftsFragment newInstance(String phoneNumber) {
    RegisterCraftsFragment fragment = new RegisterCraftsFragment();
    Bundle args = new Bundle();
    args.putString(PhoneActivity.PHONE_NUMBER, phoneNumber);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if (getArguments() != null) {
      phoneNumber = getArguments().getString(PhoneActivity.PHONE_NUMBER);
    }
  }

  @Override
  public View onCreateView(
      LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    return inflater.inflate(R.layout.fragment_register_crafts, container, false);
  }

  @Override
  public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {}

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
  }

  @Override
  public void onDetach() {
    super.onDetach();
  }
}
