package com.morabaa.team.morabaacrafts.adapters;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.morabaa.team.morabaacrafts.R;
import com.morabaa.team.morabaacrafts.model.Person;
import com.morabaa.team.morabaacrafts.model.message.Message;
import com.morabaa.team.morabaacrafts.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/** Created by eagle on 10/21/2017. */
public class MessagesAdapter extends RecyclerView.Adapter<MessageHolder> {

  private List<Message> messages;
  private OnMessageClickedListener onMessageClickedListener;

  public MessagesAdapter(List<Message> messages) {
    this.messages = messages;
  }

  @Override
  public MessageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    CardView view =
        (CardView) LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
    return new MessageHolder(view);
  }

  @Override
  public int getItemViewType(int position) {
    if (messages.get(position).getSenderId() == Person.getCurrentPerson().getId())
      return R.layout.message_layout_out;
    else return R.layout.message_layout_in;
  }

  @SuppressLint("SetTextI18n")
  @Override
  public void onBindViewHolder(
      @NonNull final MessageHolder messageHolder,
      @SuppressLint("RecyclerView") final int position) {

    messageHolder.txtMessageBody.setText(messages.get(position).getBody());
    messageHolder.txtMessageDate.setText(
        Utils.getRelativeTimeSpan(messages.get(position).getDate()));

    messageHolder.view.setOnClickListener(
        view ->
            onMessageClickedListener.onMessageClicked(
                messages.get(messageHolder.getAdapterPosition())));
  }

  public MessagesAdapter setOnMessageClickedListener(
      OnMessageClickedListener onMessageClickedListener) {
    this.onMessageClickedListener = onMessageClickedListener;
    return this;
  }

  @Override
  public int getItemCount() {
    return messages.size();
  }

  public List<Message> getMessages() {
    return messages;
  }

  public void setMessages(List<Message> messages) {
    this.messages = messages;
    notifyDataSetChanged();
  }

  public void addMessages(List<Message> messages) {
    this.messages.addAll(0, messages);
    notifyDataSetChanged();
  }

  public void addMessage(Message message) {
    this.messages.add(message);
    notifyDataSetChanged();
  }

  public void clearMessages() {
    this.messages = new ArrayList<>();
    notifyDataSetChanged();
  }

  public interface OnMessageClickedListener {

    void onMessageClicked(Message message);
  }

  private OnMessageRemovedListener onMessageRemovedListener;

  public MessagesAdapter setOnMessageRemovedListener(
      OnMessageRemovedListener onMessageRemovedListener) {
    this.onMessageRemovedListener = onMessageRemovedListener;
    return this;
  }

  public interface OnMessageRemovedListener {

    void onMessageRemoved(Message message);
  }
}

class MessageHolder extends RecyclerView.ViewHolder {

  CardView view;
  ImageView imgUserImage;
  TextView txtMessageBody;
  TextView txtMessageDate;

  MessageHolder(View itemView) {
    super(itemView);
    this.view = (CardView) itemView;
    imgUserImage = view.findViewById(R.id.imgUserImage);
    txtMessageBody = view.findViewById(R.id.txtMessageBody);
    txtMessageDate = view.findViewById(R.id.txtMessageDate);
  }
}
