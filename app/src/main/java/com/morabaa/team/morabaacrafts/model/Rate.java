package com.morabaa.team.morabaacrafts.model;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.morabaa.team.morabaacrafts.interfaces.OnCompleteListener;
import com.morabaa.team.morabaacrafts.utils.HttpRequest;
import com.morabaa.team.morabaacrafts.utils.Utils;

import java.util.List;

@Entity
public class Rate {

  private static final String CLASS_ROOT = Utils.API_ROOT + "Rate/";
  // region properties
  @PrimaryKey private long id;
  private String comment;
  private int type;
  private float rating;
  private float ratingSpeed;
  private float ratingDate;
  private float ratingAccuracy;
  @Ignore private Person rated;
  @Ignore private Person rater;
  private long timeStamp;
  @Ignore private Work work;

  // endregion

  public Rate() {}

  private static java.lang.reflect.Type getClassType(int... parms) {
    if (parms.length > 0) {
      return Rate.class;
    }
    return new TypeToken<List<Rate>>() {}.getType();
  }

  // region getters and setters
  public int getType() {
    return type;
  }

  public void setType(int type) {
    this.type = type;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public float getRating() {
    return rating;
  }

  public void setRating(float rating) {
    this.rating = rating;
  }

  public float getRatingSpeed() {
    return ratingSpeed;
  }

  public void setRatingSpeed(float ratingSpeed) {
    this.ratingSpeed = 0.01F + ratingSpeed;
  }

  public float getRatingDate() {
    return ratingDate;
  }

  public void setRatingDate(float ratingDate) {
    this.ratingDate = 0.01F + ratingDate;
  }

  public float getRatingAccuracy() {
    return ratingAccuracy;
  }

  public void setRatingAccuracy(float ratingAccuracy) {
    this.ratingAccuracy = 0.01F + ratingAccuracy;
  }

  public Person getRater() {
    return rater;
  }

  public void setRater(Person rater) {
    this.rater = rater;
  }

  public Person getRated() {
    return rated;
  }

  public void setRated(Person rated) {
    this.rated = rated;
  }

  public long getTimeStamp() {
    return timeStamp;
  }

  public void setTimeStamp(long timeStamp) {
    this.timeStamp = timeStamp;
  }

  public Work getWork() {
    return work;
  }

  public void setWork(Work work) {
    this.work = work;
  }

  public void getAll(String tag, OnCompleteListener onCompleteListener) {
    HttpRequest.getInstance()
        .addRequest(
            CLASS_ROOT + "getAll",
            new GsonBuilder()
                .serializeSpecialFloatingPointValues()
                .create()
                .toJson(this, getClassType(0)),
            getClassType(),
            tag,
            onCompleteListener);
  }

  public void getPersonRates(String tag, OnCompleteListener onCompleteListener) {
    HttpRequest.getInstance()
        .addRequest(
            CLASS_ROOT + "getPersonRates",
            new GsonBuilder()
                .serializeSpecialFloatingPointValues()
                .create()
                .toJson(this, getClassType(0)),
            getClassType(),
            tag,
            onCompleteListener);
  }

  public void getWorkRates(String tag, OnCompleteListener onCompleteListener) {
    HttpRequest.getInstance()
        .addRequest(
            CLASS_ROOT + "getWorkRates",
            new GsonBuilder()
                .serializeSpecialFloatingPointValues()
                .create()
                .toJson(this, getClassType(0)),
            getClassType(),
            tag,
            onCompleteListener);
  }

  public void push(String tag, long requestId, OnCompleteListener onCompleteListener) {
    HttpRequest.getInstance()
        .addRequest(
            CLASS_ROOT + "push?requestId=" + requestId,
            new GsonBuilder()
                .serializeSpecialFloatingPointValues()
                .create()
                .toJson(this, Rate.class),
            getClassType(),
            tag,
            onCompleteListener);
  }

  public void update(String tag, OnCompleteListener onCompleteListener) {
    HttpRequest.getInstance()
        .addRequest(
            CLASS_ROOT + "update",
            new GsonBuilder()
                .serializeSpecialFloatingPointValues()
                .create()
                .toJson(this, Rate.class),
            getClassType(),
            tag,
            onCompleteListener);
  }

  public void remove(String tag, OnCompleteListener onCompleteListener) {}

  public void updatePersonRate(String tag, OnCompleteListener<Rate> onCompleteListener) {
    HttpRequest.getInstance()
        .addRequest(
            CLASS_ROOT + "updatePersonRate",
            new GsonBuilder()
                .serializeSpecialFloatingPointValues()
                .create()
                .toJson(this, Rate.class),
            getClassType(),
            tag,
            onCompleteListener);
  }

  public void updateWorkRate(String tag, OnCompleteListener<Rate> onCompleteListener) {
    HttpRequest.getInstance()
        .addRequest(
            CLASS_ROOT + "updateWorkRate",
            new GsonBuilder()
                .serializeSpecialFloatingPointValues()
                .create()
                .toJson(this, Rate.class),
            getClassType(),
            tag,
            onCompleteListener);
  }

  // endregion

  @Dao
  public interface RateDuo {

    @Query("SELECT * FROM Rate")
    List<Rate> RATES();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Rate... rates);

    @Delete
    void delete(Rate... rates);

    @Update
    void Update(Rate... rates);

    @Query("SELECT COUNT(id) FROM Rate")
    int count();
  }

  public static class Type {

    public static final int speed = 1, aqurecy = 2, timeRespect = 3;
  }
}
