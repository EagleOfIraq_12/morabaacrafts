package com.morabaa.team.morabaacrafts.adapters;


import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.os.Build.VERSION_CODES;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask.TaskSnapshot;
import com.morabaa.team.morabaacrafts.R;
import com.morabaa.team.morabaacrafts.adapters.MediaItemsAdapter.MediaItemHolder;
import com.morabaa.team.morabaacrafts.model.MediaItem;
import com.morabaa.team.morabaacrafts.utils.AWSS3Storage;
import com.morabaa.team.morabaacrafts.utils.AWSS3Storage.OnUploadProgressListener;

import java.util.List;

import jp.wasabeef.glide.transformations.BlurTransformation;

import static com.bumptech.glide.request.RequestOptions.bitmapTransform;

/**
 * Created by eagle on 10/21/2017.
 */

public class MediaItemsAdapter extends RecyclerView.Adapter<MediaItemHolder> {

    List<MediaItem> mediaItems;
    private Context ctx;
    private OnMediaItemClickedListener onMediaItemClickedListener;

    public MediaItemsAdapter(Context ctx, List<MediaItem> mediaItems) {
        this.mediaItems = mediaItems;
        this.ctx = ctx;

    }


    @Override
    public MediaItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CardView view = (CardView) LayoutInflater.from(parent.getContext()).inflate(
                R.layout.media_layout, parent, false);
        return new MediaItemHolder(view);
    }

    @RequiresApi(api = VERSION_CODES.LOLLIPOP)
    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final MediaItemHolder mediaItemHolder,
                                 @SuppressLint("RecyclerView") final int position) {
        mediaItemHolder.view.setOnClickListener(new OnClickListener() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onClick(View view) {
                onMediaItemClickedListener.onMediaItemClicked(
                        mediaItems.get(position)
                );
            }
        });
        Uri uri = Uri.parse(mediaItems.get(position).getUrl());
        Glide.with(ctx)
                .load(uri)
                .apply(bitmapTransform(
                        new BlurTransformation(12, 3)))
                .into(mediaItemHolder.imgMediaImage);
        AWSS3Storage.getInstance(ctx).upload(
                uri, new OnUploadProgressListener() {
                    @Override
                    public void onProgress(
                            double progress) {
                        mediaItemHolder.seekMediaProgress
                                .setProgress(
                                        (int) Math
                                                .ceil(progress));
                    }

                    @Override
                    public void onSuccess(String fileName) {
                        mediaItemHolder.seekMediaProgress
                                .setVisibility(View.GONE);
                        Glide.with(ctx)
                                .load(AWSS3Storage.ROOT+fileName)
//                                .load(uri)
                                .into(mediaItemHolder.imgMediaImage);
                        try {
                            mediaItems.get(position).setUrl(fileName);
                        } catch (Exception ignored) {
                        }
                    }

                    @Override
                    public void onFailure(String error) {
                        mediaItemHolder.seekMediaProgress
                                .setVisibility(View.GONE);
                    }
                }
        );
        mediaItemHolder.btnCancelMediaUpload.setTag(position);

    }

    public void setOnMediaItemClickedListener(
            OnMediaItemClickedListener onMediaItemClickedListener) {
        this.onMediaItemClickedListener = onMediaItemClickedListener;
    }

    @Override
    public int getItemCount() {
        return mediaItems.size();
    }

    public List<MediaItem> getMediaItems() {
        return mediaItems;
    }

    public void setMediaItems(List<MediaItem> mediaItems) {
        this.mediaItems = mediaItems;
        notifyDataSetChanged();
    }

    public void addMediaItem(MediaItem mediaItem) {
        this.mediaItems.add(mediaItem);
//            notifyItemInserted(mediaItems.size() - 1);
        notifyDataSetChanged();
    }

    public interface OnMediaItemClickedListener {

        void onMediaItemClicked(MediaItem mediaItem);
    }


    class MediaItemHolder extends RecyclerView.ViewHolder {

        CardView view;
        Button btnCancelMediaUpload;
        SeekBar seekMediaProgress;
        ImageView imgMediaImage;
        TextView txtMediaName;
        StorageTask<TaskSnapshot> storageTask;


        MediaItemHolder(View itemView) {
            super(itemView);
            this.view = (CardView) itemView;

            btnCancelMediaUpload = view.findViewById(R.id.btnCancelMediaUpload);
            seekMediaProgress = view.findViewById(R.id.seekMediaProgress);
            imgMediaImage = view.findViewById(R.id.imgMediaImage);
            txtMediaName = view.findViewById(R.id.txtMediaName);
            btnCancelMediaUpload.setOnClickListener(view -> {
                int p = (int) view.getTag();
                storageTask.cancel();
                mediaItems.remove(p);
                notifyItemRemoved(p);
                notifyItemRangeChanged(p, mediaItems.size());
            });
        }
    }

}

