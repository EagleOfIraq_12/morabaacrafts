package com.morabaa.team.morabaacrafts.interfaces;

public interface OnSingleCompleteListener<T> {
      
      void onComplete(T t);
      
      void onError(String error);
}