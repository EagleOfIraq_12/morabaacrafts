package com.morabaa.team.morabaacrafts.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.morabaa.team.morabaacrafts.R;
import com.morabaa.team.morabaacrafts.activities.FullScreenImageActivity;
import com.morabaa.team.morabaacrafts.model.MediaItem;
import java.util.List;

public class MediaAdapter {
      
      private Context ctx;
      private FrameLayout root;
      private List<MediaItem> mediaItems;
      
      public MediaAdapter(Context ctx, FrameLayout root,
            List<MediaItem> mediaItems) {
            this.ctx = ctx;
            this.root = root;
            this.mediaItems = mediaItems;
      }
      
      public void bind() {
            int mediaLayout = 0;
            FrameLayout frameLayout = null;
            switch (mediaItems.size()) {
                  case 1: {
                        mediaLayout = R.layout.media_1_layout;
                        frameLayout = (FrameLayout) LayoutInflater
                              .from(ctx)
                              .inflate(
                                    mediaLayout,
                                    null,
                                    false
                              );
                        ImageView img1 = frameLayout.findViewById(R.id.img1);
                        
                        Glide.with(ctx)
                              .load(mediaItems.get(0).getUrl())
                              .thumbnail(0.1f)
                              .into(img1);
                        img1.setOnClickListener(
                              new OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                          Intent imageIntent = new Intent(ctx,
                                                FullScreenImageActivity.class);
                                          imageIntent.putExtra("title", "title");
                                          imageIntent.putExtra("url", mediaItems.get(0).getUrl());
                                          ctx.startActivity(imageIntent);
                                          Toast.makeText(ctx,
                                                "img  " + mediaItems.get(0).getUrl(),
                                                Toast.LENGTH_SHORT)
                                                .show();
                                    }
                              });
                        break;
                  }
                  case 2: {
                        mediaLayout = R.layout.media_2_layout;
                        frameLayout = (FrameLayout) LayoutInflater
                              .from(ctx)
                              .inflate(
                                    mediaLayout,
                                    null,
                                    false
                              );
                        ImageView img1 = frameLayout.findViewById(R.id.img1);
                        Glide.with(ctx)
                              .load(mediaItems.get(0).getUrl())
                              .thumbnail(0.1f)
                              .into(img1);
                        img1.setOnClickListener(
                              new OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                          Intent imageIntent = new Intent(ctx,
                                                FullScreenImageActivity.class);
                                          imageIntent.putExtra("title", "title");
                                          imageIntent.putExtra("url", mediaItems.get(0).getUrl());
                                          ctx.startActivity(imageIntent);
                                          Toast.makeText(ctx,
                                                "img  " + mediaItems.get(0).getUrl(),
                                                Toast.LENGTH_SHORT)
                                                .show();
                                    }
                              });
                        ImageView img2 = frameLayout.findViewById(R.id.img2);
                        Glide.with(ctx)
                              .load(mediaItems.get(1).getUrl())
                              .thumbnail(0.1f)
                              .into(img2);
                        img2.setOnClickListener(
                              new OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                          Intent imageIntent = new Intent(ctx,
                                                FullScreenImageActivity.class);
                                          imageIntent.putExtra("title", "title");
                                          imageIntent.putExtra("url", mediaItems.get(1).getUrl());
                                          ctx.startActivity(imageIntent);
                                          Toast.makeText(ctx,
                                                "img  " + mediaItems.get(1).getUrl(),
                                                Toast.LENGTH_SHORT)
                                                .show();
                                    }
                              });
                        break;
                  }
                  case 3: {
                        mediaLayout = R.layout.media_3_layout;
                        frameLayout = (FrameLayout) LayoutInflater
                              .from(ctx)
                              .inflate(
                                    mediaLayout,
                                    null,
                                    false
                              );
                        ImageView img1 = frameLayout.findViewById(R.id.img1);
                        Glide.with(ctx)
                              .load(mediaItems.get(0).getUrl())
                              .thumbnail(0.1f)
                              .into(img1);
                        img1.setOnClickListener(
                              new OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                          Intent imageIntent = new Intent(ctx,
                                                FullScreenImageActivity.class);
                                          imageIntent.putExtra("title", "title");
                                          imageIntent.putExtra("url", mediaItems.get(0).getUrl());
                                          ctx.startActivity(imageIntent);
                                          Toast.makeText(ctx,
                                                "img  " + mediaItems.get(0).getUrl(),
                                                Toast.LENGTH_SHORT)
                                                .show();
                                    }
                              });
                        ImageView img2 = frameLayout.findViewById(R.id.img2);
                        Glide.with(ctx)
                              .load(mediaItems.get(1).getUrl())
                              .thumbnail(0.1f)
                              .into(img2);
                        img2.setOnClickListener(
                              new OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                          Intent imageIntent = new Intent(ctx,
                                                FullScreenImageActivity.class);
                                          imageIntent.putExtra("title", "title");
                                          imageIntent.putExtra("url", mediaItems.get(1).getUrl());
                                          ctx.startActivity(imageIntent);
                                          Toast.makeText(ctx,
                                                "img  " + mediaItems.get(1).getUrl(),
                                                Toast.LENGTH_SHORT)
                                                .show();
                                    }
                              });
                        ImageView img3 = frameLayout.findViewById(R.id.img3);
                        Glide.with(ctx)
                              .load(mediaItems.get(2).getUrl())
                              .thumbnail(0.1f)
                              .into(img3);
                        img3.setOnClickListener(
                              new OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                          Intent imageIntent = new Intent(ctx,
                                                FullScreenImageActivity.class);
                                          imageIntent.putExtra("title", "title");
                                          imageIntent.putExtra("url", mediaItems.get(2).getUrl());
                                          ctx.startActivity(imageIntent);
                                          Toast.makeText(ctx,
                                                "img  " + mediaItems.get(2).getUrl(),
                                                Toast.LENGTH_SHORT)
                                                .show();
                                    }
                              });
                        break;
                  }
                  case 4: {
                        mediaLayout = R.layout.media_4_layout;
                        frameLayout = (FrameLayout) LayoutInflater
                              .from(ctx)
                              .inflate(
                                    mediaLayout,
                                    null,
                                    false
                              );
                        ImageView img1 = frameLayout.findViewById(R.id.img1);
                        Glide.with(ctx)
                              .load(mediaItems.get(0).getUrl())
                              .thumbnail(0.1f)
                              .into(img1);
                        img1.setOnClickListener(
                              new OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                          Intent imageIntent = new Intent(ctx,
                                                FullScreenImageActivity.class);
                                          imageIntent.putExtra("title", "title");
                                          imageIntent.putExtra("url", mediaItems.get(0).getUrl());
                                          ctx.startActivity(imageIntent);
                                          Toast.makeText(ctx,
                                                "img  " + mediaItems.get(0).getUrl(),
                                                Toast.LENGTH_SHORT)
                                                .show();
                                    }
                              });
                        ImageView img2 = frameLayout.findViewById(R.id.img2);
                        Glide.with(ctx)
                              .load(mediaItems.get(1).getUrl())
                              .thumbnail(0.1f)
                              .into(img2);
                        img2.setOnClickListener(
                              new OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                          Intent imageIntent = new Intent(ctx,
                                                FullScreenImageActivity.class);
                                          imageIntent.putExtra("title", "title");
                                          imageIntent.putExtra("url", mediaItems.get(1).getUrl());
                                          ctx.startActivity(imageIntent);
                                          Toast.makeText(ctx,
                                                "img  " + mediaItems.get(1).getUrl(),
                                                Toast.LENGTH_SHORT)
                                                .show();
                                    }
                              });
                        ImageView img3 = frameLayout.findViewById(R.id.img3);
                        Glide.with(ctx)
                              .load(mediaItems.get(2).getUrl())
                              .thumbnail(0.1f)
                              .into(img3);
                        img3.setOnClickListener(
                              new OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                          Intent imageIntent = new Intent(ctx,
                                                FullScreenImageActivity.class);
                                          imageIntent.putExtra("title", "title");
                                          imageIntent.putExtra("url", mediaItems.get(2).getUrl());
                                          ctx.startActivity(imageIntent);
                                          Toast.makeText(ctx,
                                                "img  " + mediaItems.get(2).getUrl(),
                                                Toast.LENGTH_SHORT)
                                                .show();
                                    }
                              });
                        ImageView img4 = frameLayout.findViewById(R.id.img4);
                        Glide.with(ctx)
                              .load(mediaItems.get(3).getUrl())
                              .thumbnail(0.1f)
                              .into(img4);
                        img4.setOnClickListener(
                              new OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                          Intent imageIntent = new Intent(ctx,
                                                FullScreenImageActivity.class);
                                          imageIntent.putExtra("title", "title");
                                          imageIntent.putExtra("url", mediaItems.get(3).getUrl());
                                          ctx.startActivity(imageIntent);
                                          Toast.makeText(ctx,
                                                "img  " + mediaItems.get(3).getUrl(),
                                                Toast.LENGTH_SHORT)
                                                .show();
                                    }
                              });
                        break;
                  }
                  
                  default: {
                        mediaLayout = R.layout.media_more_layout;
                        frameLayout = (FrameLayout) LayoutInflater
                              .from(ctx)
                              .inflate(
                                    mediaLayout,
                                    null,
                                    false
                              );
                        ImageView img1 = frameLayout.findViewById(R.id.img1);
                        Glide.with(ctx)
                              .load(mediaItems.get(0).getUrl())
                              .thumbnail(0.1f)
                              .into(img1);
                        img1.setOnClickListener(
                              new OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                          Intent imageIntent = new Intent(ctx,
                                                FullScreenImageActivity.class);
                                          imageIntent.putExtra("title", "title");
                                          imageIntent.putExtra("url", mediaItems.get(0).getUrl());
                                          ctx.startActivity(imageIntent);
                                          Toast.makeText(ctx,
                                                "img  " + mediaItems.get(0).getUrl(),
                                                Toast.LENGTH_SHORT)
                                                .show();
                                    }
                              });
                        ImageView img2 = frameLayout.findViewById(R.id.img2);
                        Glide.with(ctx)
                              .load(mediaItems.get(1).getUrl())
                              .thumbnail(0.1f)
                              .into(img2);
                        img2.setOnClickListener(
                              new OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                          Intent imageIntent = new Intent(ctx,
                                                FullScreenImageActivity.class);
                                          imageIntent.putExtra("title", "title");
                                          imageIntent.putExtra("url", mediaItems.get(1).getUrl());
                                          ctx.startActivity(imageIntent);
                                          Toast.makeText(ctx,
                                                "img  " + mediaItems.get(1).getUrl(),
                                                Toast.LENGTH_SHORT)
                                                .show();
                                    }
                              });
                        ImageView img3 = frameLayout.findViewById(R.id.img3);
                        Glide.with(ctx)
                              .load(mediaItems.get(2).getUrl())
                              .thumbnail(0.1f)
                              .into(img3);
                        img3.setOnClickListener(
                              new OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                          Intent imageIntent = new Intent(ctx,
                                                FullScreenImageActivity.class);
                                          imageIntent.putExtra("title", "title");
                                          imageIntent.putExtra("url", mediaItems.get(2).getUrl());
                                          ctx.startActivity(imageIntent);
                                          Toast.makeText(ctx,
                                                "img  " + mediaItems.get(2).getUrl(),
                                                Toast.LENGTH_SHORT)
                                                .show();
                                    }
                              });
                        ImageView img4 = frameLayout.findViewById(R.id.img4);
                        Glide.with(ctx)
                              .load(mediaItems.get(3).getUrl())
                              .thumbnail(0.1f)
                              .into(img4);
                        img4.setOnClickListener(
                              new OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                          Intent imageIntent = new Intent(ctx,
                                                FullScreenImageActivity.class);
                                          imageIntent.putExtra("title", "title");
                                          imageIntent.putExtra("url", mediaItems.get(3).getUrl());
                                          ctx.startActivity(imageIntent);
                                          Toast.makeText(ctx,
                                                "img  " + mediaItems.get(3).getUrl(),
                                                Toast.LENGTH_SHORT)
                                                .show();
                                    }
                              });
                        
                        TextView txtMoreMedia = frameLayout.findViewById(R.id.btnAddNewMedia);
                        txtMoreMedia.setText("+" + (mediaItems.size() - 4));
                        ImageView imgMore = frameLayout.findViewById(R.id.imgMore);
                        imgMore.setOnClickListener(
                              new OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                          Toast.makeText(ctx,
                                                "img  All",
                                                Toast.LENGTH_SHORT)
                                                .show();
                                    }
                              });
                        break;
                  }
            }
            
            root.addView(frameLayout);
      }
}
