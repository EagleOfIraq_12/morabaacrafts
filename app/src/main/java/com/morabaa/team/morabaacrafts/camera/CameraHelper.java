package com.morabaa.team.morabaacrafts.camera;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.morabaa.team.morabaacrafts.utils.CheckPermissionActivity;

public class CameraHelper {

    private static CameraHelper instance;

    public static CameraHelper getInstance() {
        return instance;
    }

    public static CameraHelper newInstance(Context ctx, int sessionType, OnCameraJobListener onCameraJobListener) {
        return new CameraHelper(ctx, sessionType, onCameraJobListener);
    }

    public static void releaseInstance() {
        try {
            CheckPermissionActivity.getInstance().finish();
        } catch (Exception ignored) {
        }
    }

    public CameraHelper(Context ctx, int sessionType, OnCameraJobListener onCameraJobListener) {
        instance = this;
        Intent intent = new Intent(ctx, CameraActivity.class);
        intent.putExtra("sessionType", sessionType);
        ctx.startActivity(intent);
        this.onCameraJobListener = onCameraJobListener;
    }

    private OnCameraJobListener onCameraJobListener;

    public OnCameraJobListener getOnCameraJobListener() {
        return onCameraJobListener;
    }

    public void setOnCameraJobListener(OnCameraJobListener onCameraJobListener) {
        this.onCameraJobListener = onCameraJobListener;
    }

    public interface OnCameraJobListener {
        void onCameraJobDone(Uri uri);
    }

    public static final class SessionType {
        public static final int IMAGE = 1;
        public static final int VIDEO = 2;
    }

}
