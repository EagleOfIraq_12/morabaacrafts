package com.morabaa.team.morabaacrafts.utils;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.webkit.MimeTypeMap;
import android.widget.Toast;
import java.io.File;

public class LocalStorageFileActivity extends Activity {
      
      private static LocalStorageFileActivity instance;
      private int reqCode;
      
      public static LocalStorageFileActivity getInstance() {
            return instance;
      }
      
      public static void setInstance(LocalStorageFileActivity instance) {
            LocalStorageFileActivity.instance = instance;
      }
      
      public static void end() {
            instance.finish();
      }
      
      @Override
      protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            String[] types = getIntent().getStringArrayExtra("types");
            this.reqCode = getIntent().getIntExtra("reqCode", 0);
            setInstance(this);
            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
            StringBuilder typesStrings = new StringBuilder();
            for (String type : types) {
                  typesStrings.append(type);
            }
            photoPickerIntent.setType(typesStrings.toString().trim());
            startActivityForResult(photoPickerIntent, reqCode);
      }
      
      @Override
      protected void onActivityResult(int reqCode, int resultCode, Intent data) {
            if (resultCode == RESULT_OK && reqCode == this.reqCode) {
                  Uri imageUri;
                  imageUri = data.getData();
                  String fileName = getName(imageUri);
                  String fileExt = getExtension(imageUri);
                  String imgPath = getPath(imageUri);
                  String fileType = imageUri.getEncodedPath().toLowerCase()
                        .contains("image".toLowerCase()) ?
                        "image" : "video";
                  if (LocalStorageFile.getInstance() != null) {
                        LocalStorageFile.getInstance()
                              .getOnFileReceivedListener()
                              .onFileReceived(imageUri, fileName, fileExt, fileType);
                  } else {
                        finish();
                  }
                  
            } else {
                  Toast.makeText(LocalStorageFileActivity.this, "You haven't picked file",
                        Toast.LENGTH_LONG)
                        .show();
                  if (LocalStorageFile.getInstance() != null) {
                        LocalStorageFile.getInstance()
                              .getOnFileReceivedListener()
                              .onFileError("You haven't picked file");
                  } else {
                        finish();
                  }
            }
      }
      
      @Override
      protected void onResume() {
            super.onResume();
      }
      
      public String getPath(Uri uri) {
            String[] projection = {MediaStore.Images.Media.DATA};
            Cursor cursor = LocalStorageFileActivity.this.getContentResolver()
                  .query(uri, projection, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(projection[0]);
            String filePath = cursor.getString(columnIndex);
            String path = cursor.getString(column_index);
            cursor.close();
            Bitmap yourSelectedImage = BitmapFactory.decodeFile(filePath);
            File file = new File(filePath);
            return path;
      }
      
      public String getName(Uri uri) {
            ///external/images/media/7131 <- uri.getEncodedPath()
            String path = uri.getEncodedPath();
            String[] parts = path.split("/");
            return parts[parts.length - 1];
      }
      
      public String getExtension(Uri uri) {
            String extension;
            
            //Check uri format to avoid null
            if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
                  //If scheme is a content
                  final MimeTypeMap mime = MimeTypeMap.getSingleton();
                  extension = mime
                        .getExtensionFromMimeType(
                              LocalStorageFileActivity.this.getContentResolver().getType(uri));
            } else {
                  //If scheme is a File
                  //This will replace white spaces with %20 and also other special characters. This will avoid returning null values on file name with spaces and special characters.
                  extension = MimeTypeMap
                        .getFileExtensionFromUrl(Uri.fromFile(new File(uri.getPath())).toString());
                  
            }
            
            return extension;
      }
}
