package com.morabaa.team.morabaacrafts.adapters;

import android.annotation.SuppressLint;
import android.os.Build.VERSION_CODES;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.morabaa.team.morabaacrafts.App;
import com.morabaa.team.morabaacrafts.R;
import com.morabaa.team.morabaacrafts.model.Person;
import com.morabaa.team.morabaacrafts.model.Request;
import com.morabaa.team.morabaacrafts.model.Request.STATUS;
import com.morabaa.team.morabaacrafts.utils.AWSS3Storage;
import com.morabaa.team.morabaacrafts.utils.Utils;

import java.util.List;

/** Created by eagle on 10/21/2017. */
public class RequestsAdapter extends RecyclerView.Adapter<RequestHolder> {

  private List<Request> requests;
  private OnRequestClickedListener onRequestClickedListener;
  private int requestsType;

  public RequestsAdapter(List<Request> requests, int requestsType) {
    this.requests = requests;
    this.requestsType = requestsType;
  }

  @Override
  public int getItemViewType(int position) {
    if (requestsType == RequestsAdapter.TYPE.SENT) {
      switch (requests.get(position).getStatus()) {
        case STATUS.INIT:
          return R.layout.request_init_sent_layout;
        case STATUS.ACCEPTED:
          return R.layout.request_accepted_sent_layout;
        case STATUS.DEAL:
          return R.layout.request_deal_sent_layout;
        case STATUS.REJECTED:
          return R.layout.request_rejected_sent_layout;
        case STATUS.DONE:
          return R.layout.request_done_sent_layout;
        default:
          return R.layout.request_error_layout;
      }
    } else if (requestsType == RequestsAdapter.TYPE.RECEIVED) {
      switch (requests.get(position).getStatus()) {
        case STATUS.INIT:
          return R.layout.request_init_received_layout;
        case STATUS.ACCEPTED:
          return R.layout.request_accepted_received_layout;
        case STATUS.DEAL:
          return R.layout.request_deal_received_layout;
        case STATUS.ABORTED:
          return R.layout.request_aborted_received_layout;
        case STATUS.DONE:
          return R.layout.request_done_received_layout;
        default:
          return R.layout.request_error_layout;
      }
    } else {
      return R.layout.request_error_layout;
    }
  }

  @NonNull
  @Override
  public RequestHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    // todo edit done view
    return new RequestHolder(
        LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false));
  }

  @RequiresApi(api = VERSION_CODES.LOLLIPOP)
  @SuppressLint("SetTextI18n")
  @Override
  public void onBindViewHolder(
      @NonNull final RequestHolder requestHolder,
      @SuppressLint("RecyclerView") final int position) {
    if (requests.get(position).getStatus() == STATUS.DONE && requestsType == Request.TYPE.SENT) {
      if (!requests.get(position).isPosted()) {
        requestHolder.btnShowWork.setVisibility(View.GONE);
      }
      if (requests.get(position).isWorkRated()) {
        requestHolder.btnRateWork.setVisibility(View.GONE);
      }
      if (requests.get(position).isReceiverRated()) {
        requestHolder.btnRateReceiver.setVisibility(View.GONE);
      }
    }

    requestHolder.txtRequestStartDate.setText(
        Utils.getRelativeTimeSpan(requests.get(position).getCreationDate()));
    requestHolder.txtRequestAddress.setText(requests.get(position).getAddress());
    requestHolder.txtRequestSenderNote.setText(requests.get(position).getSenderNote());
    requestHolder.txtRequestPrice.setText(String.valueOf(requests.get(position).getPrice()));
    requestHolder.txtRequestUnit.setText(requests.get(position).getUnit());

    try {
      Glide.with(App.getInstance().getApplicationContext())
          .load(
              AWSS3Storage.getReference(
                  requests.get(position).getReceiver().getMediaItem().getUrl()))
          .into(requestHolder.imgReceiverImage);
    } catch (Exception e) {
      e.printStackTrace();
    }

    try {
      requestHolder.txtSenderAddress.setText(requests.get(position).getSender().getAddress());
      requestHolder.txtSenderName.setText(requests.get(position).getSender().getName());
      Glide.with(App.getInstance().getApplicationContext())
          .load(
              AWSS3Storage.getReference(requests.get(position).getSender().getMediaItem().getUrl()))
          .into(requestHolder.imgSenderImage);
    } catch (Exception e) {
      System.out.println("e.getMessage() = " + e.getMessage());
    }

    requestHolder.txtRequestReceiverNote.setText(requests.get(position).getReceiverNote());
    try {
      requestHolder.ratingReceiverRating.setRating(
          requests.get(position).getReceiver().getRating());
      requestHolder.txtReceiverName.setText(requests.get(position).getReceiver().getName());
      requestHolder.txtReceiverAddress.setText(requests.get(position).getReceiver().getAddress());
    } catch (Exception e) {
    }

    requestHolder.txtRequestWorkDate.setText(
        Utils.getRelativeTimeSpan(requests.get(position).getWorkDate()));

    requestHolder.view.setOnClickListener(
        view -> onRequestClickedListener.onRequestClicked(requests.get(position)));

    requestHolder.btnReject.setOnClickListener(
        view -> onRequestClickedListener.onRejectRequest(requests.get(position)));

    requestHolder.btnAccept.setOnClickListener(
        view -> onRequestClickedListener.onAcceptRequest(requests.get(position)));

    requestHolder.btnReceiverEdit.setOnClickListener(
        view -> onRequestClickedListener.onReceiverEditRequest(requests.get(position)));

    requestHolder.btnDone.setOnClickListener(
        view -> onRequestClickedListener.onDoneRequest(requests.get(position)));

    requestHolder.btnPost.setOnClickListener(
        view -> onRequestClickedListener.onPostRequest(requests.get(position)));

    requestHolder.btnCancel.setOnClickListener(
        view -> onRequestClickedListener.onCancelRequest(requests.get(position)));

    requestHolder.btnDeal.setOnClickListener(
        view -> onRequestClickedListener.onDealRequest(requests.get(position)));

    requestHolder.btnAbort.setOnClickListener(
        view -> onRequestClickedListener.onAbortRequest(requests.get(position)));

    requestHolder.btnSenderEdit.setOnClickListener(
        view -> onRequestClickedListener.onSenderEditRequest(requests.get(position)));

    requestHolder.btnRateReceiver.setOnClickListener(
        view -> onRequestClickedListener.onRateReceiverRequest(requests.get(position)));

    requestHolder.btnRateWork.setOnClickListener(
        view -> onRequestClickedListener.onRateWorkRequest(requests.get(position)));

    requestHolder.btnChat.setOnClickListener(
        view -> onRequestClickedListener.onOpenChat(requests.get(position)));

    try {
      requestHolder.btnShowWork.setOnClickListener(
          view -> onRequestClickedListener.onShowWorkRequest(requests.get(position)));
      requestHolder.layoutReceiverInfo.setOnClickListener(
          view ->
              onReceiverClickedListener.onReceiverClicked(requests.get(position).getReceiver()));
    } catch (Exception ignored) {
    }
  }

  public void setOnRequestClickedListener(OnRequestClickedListener onRequestClickedListener) {
    this.onRequestClickedListener = onRequestClickedListener;
  }

  @Override
  public int getItemCount() {
    return requests.size();
  }

  public List<Request> getRequests() {
    return requests;
  }

  public void setRequests(List<Request> requests) {
    this.requests = requests;
    notifyDataSetChanged();
  }

  private class TYPE {
    public static final int SENT = 1;
    public static final int RECEIVED = 2;
  }

  public interface OnRequestClickedListener {

    void onRequestClicked(Request request);

    void onAcceptRequest(Request request);

    void onRejectRequest(Request request);

    void onReceiverEditRequest(Request request);

    void onDoneRequest(Request request);

    void onPostRequest(Request request);

    void onCancelRequest(Request request);

    void onDealRequest(Request request);

    void onAbortRequest(Request request);

    void onSenderEditRequest(Request request);

    void onRateReceiverRequest(Request request);

    void onRateWorkRequest(Request request);

    void onShowWorkRequest(Request request);

    void onOpenChat(Request request);
  }

  private OnReceiverClickedListener onReceiverClickedListener;

  public void setOnReceiverClickedListener(OnReceiverClickedListener onReceiverClickedListener) {
    this.onReceiverClickedListener = onReceiverClickedListener;
  }

  public interface OnReceiverClickedListener {

    void onReceiverClicked(Person person);
  }
}

class RequestHolder extends RecyclerView.ViewHolder {

  CardView view;
  TextView txtRequestStartDate;
  TextView txtRequestSenderNote;
  TextView txtRequestAddress;
  TextView txtRequestPrice;
  TextView txtRequestUnit;
  TextView txtRequestReceiverNote;
  TextView txtRequestWorkDate;
  TextView txtRequestType;

  RatingBar ratingReceiverRating;
  TextView txtReceiverName;
  TextView txtReceiverAddress;

  TextView txtSenderName;
  TextView txtSenderAddress;

  Button btnChat;

  Button btnAccept;
  Button btnReject;
  Button btnSenderEdit;
  Button btnPost;
  Button btnReceiverEdit;
  Button btnDone;
  Button btnCancel;
  Button btnDeal;
  Button btnAbort;
  Button btnRateReceiver;
  Button btnRateWork;
  Button btnShowWork;

  ImageView imgReceiverImage;
  ImageView imgSenderImage;

  LinearLayout layoutReceiverInfo;
  LinearLayout layoutSenderInfo;

  RequestHolder(View itemView) {
    super(itemView);
    this.view = (CardView) itemView;
    layoutReceiverInfo = view.findViewById(R.id.layoutReceiverInfo);
    layoutSenderInfo = view.findViewById(R.id.layoutSenderInfo);
    txtRequestStartDate = view.findViewById(R.id.txtRequestStartDate);
    txtRequestAddress = view.findViewById(R.id.txtRequestAddress);
    txtRequestSenderNote = view.findViewById(R.id.txtRequestSenderNote);
    txtRequestPrice = view.findViewById(R.id.txtRequestPrice);
    txtRequestUnit = view.findViewById(R.id.txtRequestUnit);
    txtRequestReceiverNote = view.findViewById(R.id.txtRequestReceiverNote);
    txtRequestWorkDate = view.findViewById(R.id.txtRequestWorkDate);
    txtRequestType = view.findViewById(R.id.txtRequestType);

    txtReceiverAddress = view.findViewById(R.id.txtReceiverAddress);
    txtReceiverName = view.findViewById(R.id.txtReceiverName);
    ratingReceiverRating = view.findViewById(R.id.ratingReceiverRating);

    txtSenderName = view.findViewById(R.id.txtSenderName);
    txtSenderAddress = view.findViewById(R.id.txtSenderAddress);

    imgReceiverImage = view.findViewById(R.id.imgReceiverImage);
    imgSenderImage = view.findViewById(R.id.imgSenderImage);

    btnChat = view.findViewById(R.id.btnChat);

    btnAccept = view.findViewById(R.id.btnAccept);
    btnReject = view.findViewById(R.id.btnReject);
    btnRateWork = view.findViewById(R.id.btnRateWork);
    btnShowWork = view.findViewById(R.id.btnShowWork);
    btnRateReceiver = view.findViewById(R.id.btnRateReceiver);
    btnAbort = view.findViewById(R.id.btnAbort);
    btnDeal = view.findViewById(R.id.btnDeal);
    btnCancel = view.findViewById(R.id.btnCancel);
    btnDone = view.findViewById(R.id.btnDone);
    btnReceiverEdit = view.findViewById(R.id.btnReceiverEdit);
    btnPost = view.findViewById(R.id.btnPost);
    btnSenderEdit = view.findViewById(R.id.btnSenderEdit);
  }
}
