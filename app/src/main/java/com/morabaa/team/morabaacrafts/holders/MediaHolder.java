package com.morabaa.team.morabaacrafts.holders;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;

import com.bumptech.glide.Glide;
import com.morabaa.team.morabaacrafts.R;
import com.morabaa.team.morabaacrafts.model.MediaItem;
import com.morabaa.team.morabaacrafts.utils.AWSS3Storage;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import jp.wasabeef.glide.transformations.BlurTransformation;

import static com.bumptech.glide.request.RequestOptions.bitmapTransform;

public class MediaHolder {

    private List<MediaItem> mediaItems = new ArrayList<>();
    private LinearLayout container;
    private Context ctx;

    public MediaHolder(Context ctx, LinearLayout container) {
        this.container = container;
        this.ctx = ctx;
    }

    public MediaHolder build() {
        return this;
    }

    public List<MediaItem> getMediaItems() {
        List<MediaItem> media = new ArrayList<>();
        for (MediaItem mediaItem : mediaItems) {
            if (mediaItem != null && mediaItem.getId() != 0) {
                media.add(mediaItem);
                mediaItem.setId(0);
            }
        }
        return media;
    }

    public boolean isCanPost() {
        for (MediaItem mediaItem : mediaItems) {
            if (mediaItem.getId() == 0) continue;
            if (mediaItem.getUrl() == null) return false;
        }
        return true;
    }

    public void addMedia(MediaItem mediaItem) {
        int id = new Random().nextInt();
        mediaItem.setId(id);
        mediaItems.add(mediaItem);
        LayoutInflater inflater = (LayoutInflater) container.getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View mediaLayout = inflater.inflate(R.layout.media_layout,
                null, false);
        ImageView imgMediaImage = mediaLayout.findViewById(R.id.imgMediaImage);
        SeekBar seekMediaProgress = mediaLayout.findViewById(R.id.seekMediaProgress);
        Button btnCancelMediaUpload = mediaLayout.findViewById(R.id.btnCancelMediaUpload);

        btnCancelMediaUpload.setOnClickListener(view -> {
                    mediaItem.setId(0);
                    container.removeView(mediaLayout);
                }
        );

        Glide.with(container.getContext())
                .load(mediaItem.getUri())
                .apply(bitmapTransform(
                        new BlurTransformation(10, 3)))
                .into(imgMediaImage);

        seekMediaProgress.setVisibility(View.VISIBLE);

        AWSS3Storage.getInstance(ctx).upload(
                mediaItem.getUri(), new AWSS3Storage.OnUploadProgressListener() {
                    @Override
                    public void onProgress(
                            double progress) {
                        seekMediaProgress
                                .setProgress(
                                        (int) Math
                                                .ceil(progress));
                    }

                    @Override
                    public void onSuccess(String fileName) {
                        seekMediaProgress
                                .setVisibility(View.GONE);
                        Glide.with(ctx)
                                .load(AWSS3Storage.getReference(fileName))
                                .into(imgMediaImage);

                        mediaItem.setUrl(fileName);
                    }

                    @Override
                    public void onFailure(String error) {
                        seekMediaProgress
                                .setVisibility(View.GONE);
                    }
                }
        );
        container.addView(mediaLayout);
    }

}
