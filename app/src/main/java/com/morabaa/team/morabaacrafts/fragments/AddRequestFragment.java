package com.morabaa.team.morabaacrafts.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.morabaa.team.morabaacrafts.R;
import com.morabaa.team.morabaacrafts.interfaces.OnCompleteListener;
import com.morabaa.team.morabaacrafts.model.Person;
import com.morabaa.team.morabaacrafts.model.Request;
import com.morabaa.team.morabaacrafts.model.Request.STATUS;
import com.morabaa.team.morabaacrafts.utils.Utils;

import java.util.Date;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddRequestFragment extends Fragment {

    private Button btnSendRequest;
    private EditText txtAddress;
    private EditText txtRequestUserNote;

    public AddRequestFragment() {
        // Required empty public constructor
    }

    public static AddRequestFragment newInstance(Bundle args) {
        AddRequestFragment fragment = new AddRequestFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static long ConvertToUnixTimestamp(/*DateTime currentDate*/) {
//            DateTime UnixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
//            TimeSpan diff = currentDate.ToUniversalTime() - UnixEpoch;
//            return diff.TotalMilliseconds;
        return 0L;
    }

    public static long getCurrentUnixTimestamp() {
//            DateTime UnixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
//            TimeSpan currentDate = (DateTime.Now.ToUniversalTime() - UnixEpoch);
//            return currentDate.TotalMilliseconds;
        return 0L;
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_add_request, container, false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        btnSendRequest = view.findViewById(R.id.btnSendRequest);
        txtAddress = view.findViewById(R.id.txtAddress);
        txtRequestUserNote = view.findViewById(R.id.txtRequestSenderNote);
        assert getArguments() != null;
        String craftManString = getArguments().getString("craftManString");

        final Person person = Person.fromJson(craftManString);
        final Person sender = Person.getCurrentPerson();
        btnSendRequest.setOnClickListener(view1 -> {
            Request request = new Request();
            request.setAddress(txtAddress.getText().toString());
            request.setSenderNote(txtRequestUserNote.getText().toString());
            request.setReceiver(person);
            request.setSender(sender);
            request.setCreationDate(new Date().getTime());
            request.setStatus(STATUS.INIT);
            txtAddress.setEnabled(false);
            txtRequestUserNote.setEnabled(false);
            view1.setEnabled(false);
            request.push("", new OnCompleteListener<Request>() {
                @Override
                public void onComplete(List<Request> requests) {
                    txtAddress.setText("");
                    txtRequestUserNote.setText("");
                    txtAddress.setEnabled(true);
                    txtRequestUserNote.setEnabled(true);
                    view1.setEnabled(true);


                    System.out.println("getTimeStamp: "
                            + Utils.getRelativeTimeSpan(request.getCreationDate()));
                    getActivity().getSupportFragmentManager()
                            .beginTransaction()
                            .add(
                                    R.id.frameMainContainer,
                                    RequestsContainerFragment.newInstance(true, 0))
                            .commit();
//                    ((MainActivity) getActivity()).navMain.setCurrentItem(1);
                }

                @Override
                public void onError(String error) {
                    txtAddress.setEnabled(true);
                    txtRequestUserNote.setEnabled(true);
                    view1.setEnabled(true);
                    System.out.println("error = " + error);
                }
            });
        });
    }
}
