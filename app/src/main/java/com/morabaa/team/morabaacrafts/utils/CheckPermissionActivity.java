package com.morabaa.team.morabaacrafts.utils;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;

import com.morabaa.team.morabaacrafts.R;

public class CheckPermissionActivity extends AppCompatActivity {

    private int reqCode;

    private static CheckPermissionActivity instance;

    public static CheckPermissionActivity getInstance() {
        return instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        instance = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_permission);
        String permission = getIntent().getStringExtra("permission");
        reqCode = getIntent().getIntExtra("reqCode", 0);
        ActivityCompat.requestPermissions(this, new String[]{permission}, reqCode);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        if (requestCode == reqCode) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                CheckPermission.getInstance().getOnPermissionGrantedListener().onPermissionGranted(true);
                // permissions granted.
            } else {
                CheckPermission.getInstance().getOnPermissionGrantedListener().onPermissionGranted(false);
                // permissions not granted.
            }
            finish();
        }
    }
}
