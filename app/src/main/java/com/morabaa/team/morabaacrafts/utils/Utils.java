package com.morabaa.team.morabaacrafts.utils;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.Nullable;

import com.morabaa.team.morabaacrafts.App;
import com.morabaa.team.morabaacrafts.R;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class Utils {

  public static final String API_ROOT =
      "http://morabaacraftsapi-dev.us-east-2.elasticbeanstalk.com/Api/";
//        public static final String API_ROOT = "http://192.168.10.56:1360/Api/";
  //    public static final String API_ROOT = "http://DESKTOP-O748JE5:1360/Api/";
/*
* Configuration on demand is not supported by the current version of the Android Gradle plugin since you are using Gradle version 4.6 or above. Suggestion: disable configuration on demand by setting org.gradle.configureondemand=false in your gradle.properties file or use a Gradle version less than 4.6.
 */
  public static void exportDB(Context ctx) {
    try {
      File sd = Environment.getExternalStorageDirectory();
      File data = Environment.getDataDirectory();

      if (sd.canWrite()) {
        String currentDBPath = "//data//" + ctx.getPackageName() + "//databases//" + "crafts.db";
        String backupDBPath = "/BackupFolder/crafts.db";
        File currentDB = new File(data, currentDBPath);
        File backupDB = new File(sd, backupDBPath);
        boolean success = true;
        if (!backupDB.exists()) {
          success = backupDB.createNewFile();
        }
        if (success) {
          try {
            FileUtils.copyFile(currentDB, backupDB);
            sendFile(ctx, Uri.fromFile(backupDB), new String[] {"Eagleofiraq.12@gmail.com"});

          } catch (IOException e) {
            e.printStackTrace();
          }

          FileChannel src = new FileInputStream(currentDB).getChannel();
          FileChannel dst = new FileOutputStream(backupDB).getChannel();

          dst.transferFrom(src, 0, src.size());
          src.close();
          dst.close();
        }
      }
    } catch (Exception e) {
      System.out.print(e.getMessage());
    }
  }

  public static File copyToLocalVideoFile(File file) {
    try {
      File outputDir = App.getInstance().getCacheDir(); // context being the Activity pointer
      File outputFile =
          File.createTempFile("video" + System.currentTimeMillis(), ".mp4", outputDir);
      FileChannel src = new FileInputStream(file).getChannel();
      FileChannel dst = new FileOutputStream(outputFile).getChannel();
      dst.transferFrom(src, 0, src.size());
      src.close();
      dst.close();
      return outputFile;
    } catch (Exception e) {
      System.out.print(e.getMessage());
    }
    return file;
  }

  private static void sendFile(Context ctx, Uri uri, @Nullable String[] to) {
    Intent emailIntent = new Intent(Intent.ACTION_SEND);
    emailIntent.putExtra(Intent.EXTRA_EMAIL, to);
    emailIntent.putExtra(Intent.EXTRA_SUBJECT, "My Subject");
    emailIntent.putExtra(Intent.EXTRA_TEXT, "My Body");
    emailIntent.setType("application/db");
    emailIntent.putExtra(Intent.EXTRA_STREAM, uri);
    System.out.println(uri);
    ctx.startActivity(Intent.createChooser(emailIntent, "Send email using:"));
  }

  public static String getRelativeTimeSpan(long timeSpan) {
    return getRelativeTimeSpanString(App.getInstance(), new Date(timeSpan));
    /* return DateUtils.getRelativeTimeSpanString(
        timeSpan,
        System.currentTimeMillis(),
        DateUtils.MINUTE_IN_MILLIS,
        DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_YEAR)
    .toString();*/
  }

  public static String getRelativeTimeSpanString(Context context, Date fromdate) {

    long then;
    then = fromdate.getTime();
    Date date = new Date(then);

    StringBuffer dateStr = new StringBuffer();

    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);
    Calendar now = Calendar.getInstance();

    int days = daysBetween(calendar.getTime(), now.getTime());
    int weeks = days / 7;
    int minutes = hoursBetween(calendar.getTime(), now.getTime());
    int hours = minutes / 60;
    if (days == 0) {
      int second = minuteBetween(calendar.getTime(), now.getTime());
      if (minutes > 60) {
        if (hours >= 1 && hours <= 24) {
          dateStr
              .append("منذ ")
              .append(
                  String.format(
                      "%s %s %s",
                      hours, context.getString(R.string.hour), context.getString(R.string.ago)));
        }
      } else {
        if (second <= 10) {
          dateStr.append(context.getString(R.string.now));
        } else if (second > 10 && second <= 30) {
          dateStr.append("منذ ").append(context.getString(R.string.few_seconds));
        } else if (second > 30 && second <= 60) {
          dateStr
              .append("منذ ")
              .append(
                  String.format(
                      "%s %s %s",
                      second,
                      context.getString(R.string.seconds),
                      context.getString(R.string.ago)));
        } else if (second >= 60 && minutes <= 60) {
          dateStr
              .append("منذ ")
              .append(
                  String.format(
                      "%s %s %s",
                      minutes,
                      context.getString(R.string.minutes),
                      context.getString(R.string.ago)));
        }
      }
    } else if (hours > 24 && days < 7) {
      dateStr
          .append("منذ ")
          .append(
              String.format(
                  "%s %s %s",
                  days, context.getString(R.string.days), context.getString(R.string.ago)));
    } else if (weeks == 1) {
      dateStr
          .append("منذ ")
          .append(
              String.format(
                  "%s %s %s",
                  weeks, context.getString(R.string.weeks), context.getString(R.string.ago)));
    } else {
      /**
       * make formatted createTime by languageTag("ar")
       *
       * @return
       */
      return SimpleDateFormat.getDateInstance(SimpleDateFormat.LONG, new Locale("ar"))
          .format(date)
          .toUpperCase();
    }

    return dateStr.toString();
  }

  public static int minuteBetween(Date d1, Date d2) {
    return (int) ((d2.getTime() - d1.getTime()) / android.text.format.DateUtils.SECOND_IN_MILLIS);
  }

  public static int hoursBetween(Date d1, Date d2) {
    return (int) ((d2.getTime() - d1.getTime()) / android.text.format.DateUtils.MINUTE_IN_MILLIS);
  }

  public static int daysBetween(Date d1, Date d2) {
    return (int) ((d2.getTime() - d1.getTime()) / android.text.format.DateUtils.DAY_IN_MILLIS);
  }

  public static double distanceX(
      double lat1, double lon1, double lat2, double lon2, char... units) {
    double theta = lon1 - lon2;
    double dist =
        Math.sin((lat1 * Math.PI / 180.0)) * Math.sin((lat2 * Math.PI / 180.0))
            + Math.cos((lat1 * Math.PI / 180.0))
                * Math.cos((lat2 * Math.PI / 180.0))
                * Math.cos((theta * Math.PI / 180.0));
    dist = Math.acos(dist);
    dist = (dist * 180.0 / Math.PI);
    dist = dist * 60 * 1.1515;
    if (units.length < 1) {
      dist = dist * 1609.344;
    } else {
      if (units[0] == 'K') {
        dist = dist * 1.609344;
      } else if (units[0] == 'M') {
        dist = dist * 1609.344;
      } else if (units[0] == 'N') {
        dist = dist * 0.8684;
      }
    }
    return (dist);
  }
  /**
   * Calculate distance between two points in latitude and longitude taking into account height
   * difference. If you are not interested in height difference pass 0.0. Uses Haversine method as
   * its base.
   *
   * <p>lat1, lon1 Start point lat2, lon2 End point el1 Start altitude in meters el2 End altitude in
   * meters
   *
   * @returns Distance in Meters
   */
  public static double distance(
      double lat1, double lat2, double lon1, double lon2, double el1, double el2) {

    final int R = 6371; // Radius of the earth

    double latDistance = Math.toRadians(lat2 - lat1);
    double lonDistance = Math.toRadians(lon2 - lon1);
    double a =
        Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
            + Math.cos(Math.toRadians(lat1))
                * Math.cos(Math.toRadians(lat2))
                * Math.sin(lonDistance / 2)
                * Math.sin(lonDistance / 2);
    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    double distance = R * c * 1000; // convert to meters
    double height = el1 - el2;

    distance = Math.pow(distance, 2) + Math.pow(height, 2);

    double fDist = Math.sqrt(distance);
    return fDist;
  }

  public Drawable mergeDrawables(Drawable... drawables) {
    Drawable first = drawables[0];
    Drawable second = drawables[1];

    int horizontalInset = (first.getIntrinsicWidth() - second.getIntrinsicWidth()) / 2;

    LayerDrawable finalDrawable = new LayerDrawable(drawables);
    for (Drawable drawable : drawables) {}

    finalDrawable.setLayerInset(0, 0, 0, 0, second.getIntrinsicHeight());
    finalDrawable.setLayerInset(1, horizontalInset, first.getIntrinsicHeight(), horizontalInset, 0);
    return finalDrawable;
  }

  public static long getTimeStampFromDotNetDateTime(String dotNetDateTime) {

    int day = 0, month = 0, year = 0;
    try {
      year = Integer.parseInt(dotNetDateTime.substring(0, 1));
      month = Integer.parseInt(dotNetDateTime.substring(1, 2));
      day = Integer.parseInt(dotNetDateTime.substring(2, 3));
    } catch (NumberFormatException ignored) {
    }
    return new Date(year, month, day).getTime();
  }
}
