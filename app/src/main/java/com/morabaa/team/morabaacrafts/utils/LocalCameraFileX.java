package com.morabaa.team.morabaacrafts.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

public class LocalCameraFileX {
      
      private static LocalCameraFileX instance;
      private OnFileReceivedListener onFileReceivedListener;
      
      public LocalCameraFileX(Context ctx, int cameraAction) {
            Intent intent = new Intent(ctx, LocalCameraFileActivity.class);
            intent.putExtra("reqCode", 88);
            intent.putExtra("cameraAction", cameraAction);
            ctx.startActivity(intent);
            instance = this;
      }
      
      public static LocalCameraFileX getInstance() {
            return instance;
      }
      
      public static void releaseInstance() {
            LocalCameraFileActivity.getInstance().finish();
            instance = null;
      }
      
      public static LocalCameraFileX newInstance(Context ctx, int cameraAction) {
            return new LocalCameraFileX(ctx, cameraAction);
      }
      
      public static LocalCameraFileX captureImage(Context ctx) {
            return new LocalCameraFileX(ctx, CAMERA_ACTION.CAPTURE_IMAGE);
      }
      
      public static LocalCameraFileX recordVideo(Context ctx) {
            return new LocalCameraFileX(ctx, CAMERA_ACTION.RECORD_VIDEO);
      }
      
      
      public OnFileReceivedListener getOnFileReceivedListener() {
            return onFileReceivedListener;
      }
      
      public LocalCameraFileX setOnFileReceivedListener(
            OnFileReceivedListener onFileReceivedListener) {
            this.onFileReceivedListener = onFileReceivedListener;
            return this;
      }
      
      public interface OnFileReceivedListener {
            
            void onFileReceived(Uri uri);
            
            void onFileError(String error);
      }
      
      public static final class CAMERA_ACTION {
            
            public static final int CAPTURE_IMAGE = 1;
            public static final int RECORD_VIDEO = 2;
      }
}
