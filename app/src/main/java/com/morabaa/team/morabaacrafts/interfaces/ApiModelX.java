package com.morabaa.team.morabaacrafts.interfaces;

import com.morabaa.team.morabaacrafts.utils.HttpRequest;

import java.lang.reflect.Type;

public interface ApiModelX<T> {

    /**
     * @param onCompleteListener code
     *                           {
     *                           HttpRequest.getInstance()
     *                           .addRequest(CLASS_ROOT + "getAll",
     *                           "",
     *                           classType(), tag ,onCompleteListener
     *                           });
     *                           }
     */
    void getAll(String tag, final OnCompleteListener<T> onCompleteListener);


    void push(String tag, final OnCompleteListener<T> onCompleteListener);

    void update(String tag, final OnCompleteListener<T> onCompleteListener);

    void remove(String tag, final OnCompleteListener<T> onCompleteListener);

    String CLASS_ROOT = "";
    Type classType = null;

    default void getAllX(String tag, final OnCompleteListener<T> onCompleteListener) {
        HttpRequest.getInstance()
                .addRequest(CLASS_ROOT + "getAll",
                        "",
                        classType, tag, onCompleteListener);
    }
//    String getMainUrl();
//
//    default void add(String tag, final OnCompleteListener<T> onCompleteListener) {
//        HttpRequest.getInstance().addRequest(
//                getMainUrl()+"add", "", getClass(), tag, onCompleteListener);
//    }


     /* @Dao
      public interface databaseDuo<T>{
            
            @Insert(onConflict = OnConflictStrategy.REPLACE)
            void insert(T... ts);
      
            @Delete
            void delete(T... ts);
      
            @Update
            void Update(T... ts);
      
      }*/
}
