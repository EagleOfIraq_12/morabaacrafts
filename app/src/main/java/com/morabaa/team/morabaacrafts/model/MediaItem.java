package com.morabaa.team.morabaacrafts.model;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import android.net.Uri;
import com.google.gson.reflect.TypeToken;
import com.morabaa.team.morabaacrafts.utils.Utils;
import java.lang.reflect.Type;
import java.util.List;

@Entity
public class MediaItem {
      
      private static final String CLASS_ROOT = Utils.API_ROOT + "MediaItem/";
      
      //region properties
      @PrimaryKey
      private long id;
      private String url;
      @Ignore
      private Uri uri;
      //endregion
      
      public MediaItem() {
      }
      
      public static Type getClassType() {
            return new TypeToken<List<MediaItem>>() {
            }.getType();
      }
      
      //region getters and setters
      
      public long getId() {
            return id;
      }
      
      public void setId(long id) {
            this.id = id;
      }
      
      public String getUrl() {
            return url;
      }
      
      public void setUrl(String url) {
            this.url = url;
      }
      
      public Uri getUri() {
            return uri;
      }
      
      public void setUri(Uri uri) {
            this.uri = uri;
      }
      
      //endregion
      
      
      @Dao
      public interface MediaItemDuo {
            
            @Query("SELECT * FROM MediaItem")
            List<MediaItem> MEDIA_ITEMS();
            
            @Insert(onConflict = OnConflictStrategy.REPLACE)
            void insert(MediaItem... mediaItems);
            
            @Delete
            void delete(MediaItem... mediaItems);
            
            @Update
            void Update(MediaItem... mediaItems);
            
            @Query("SELECT COUNT(id) FROM MediaItem")
            int count();
      }
}
