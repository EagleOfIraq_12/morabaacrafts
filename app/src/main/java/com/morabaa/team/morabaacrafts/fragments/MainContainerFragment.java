package com.morabaa.team.morabaacrafts.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.morabaa.team.morabaacrafts.R;


public class MainContainerFragment extends Fragment {


    public MainContainerFragment() {
        // Required empty public constructor
    }

    public static MainContainerFragment newInstance() {
        MainContainerFragment fragment = new MainContainerFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main_container, container, false);
    }

    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        Fragment fragment = CraftsFragment.newInstance(new Bundle());
//        if (Person.CURRENT_USER_TYPE == Person.PERSON_TYPE.USER) {
//            fragment = CraftsFragment.newInstance(new Bundle());
//        } else {
//            Bundle bundle = new Bundle();
//            bundle.putString("craftManString", new GsonBuilder()
//                    .serializeSpecialFloatingPointValues()
//                    .create()
//                    .toJson(Person.getCurrentPerson(), Person.class));
//            fragment = CraftManProfileFragment.newInstance(new Bundle());
//
//        }

        getChildFragmentManager()
                .beginTransaction()
                .add(
                        R.id.frameMainContainer, fragment
                )
                .addToBackStack(null)
                .commit();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
