package com.morabaa.team.morabaacrafts.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

import com.bumptech.glide.Glide;
import com.github.chrisbanes.photoview.PhotoView;
import com.morabaa.team.morabaacrafts.R;
import com.morabaa.team.morabaacrafts.utils.AWSS3Storage;

public class FullScreenImageActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_full_screen_image);

        String title = getIntent().getStringExtra("title");
        String url = getIntent().getStringExtra("url");

        setTitle(title);
        PhotoView photoView = findViewById(R.id.photo_view);

        Glide.with(FullScreenImageActivity.this)
                .load(AWSS3Storage.getReference(url))
                .thumbnail(0.1f)
                .into(photoView);


    }

}