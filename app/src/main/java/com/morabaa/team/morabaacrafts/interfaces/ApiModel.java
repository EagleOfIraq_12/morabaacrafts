package com.morabaa.team.morabaacrafts.interfaces;

import com.morabaa.team.morabaacrafts.model.Rate;

public interface ApiModel<T> {

    /**
     * @param onCompleteListener code
     *                           {
     *                           HttpRequest.getInstance()
     *                           .addRequest(CLASS_ROOT + "getAll",
     *                           "",
     *                           getClassType(), tag ,onCompleteListener
     *                           });
     *                           }
     */
    void getAll(String tag, final OnCompleteListener<T> onCompleteListener);

    void push(String tag, final OnCompleteListener<T> onCompleteListener);

    void update(String tag, final OnCompleteListener<T> onCompleteListener);

    void remove(String tag, final OnCompleteListener<T> onCompleteListener);


//    String getMainUrl();
//
//    default void add(String tag, final OnCompleteListener<T> onCompleteListener) {
//        HttpRequest.getInstance().addRequest(
//                getMainUrl()+"add", "", getClass(), tag, onCompleteListener);
//    }


     /* @Dao
      public interface databaseDuo<T>{
            
            @Insert(onConflict = OnConflictStrategy.REPLACE)
            void insert(T... ts);
      
            @Delete
            void delete(T... ts);
      
            @Update
            void Update(T... ts);
      
      }*/
}
