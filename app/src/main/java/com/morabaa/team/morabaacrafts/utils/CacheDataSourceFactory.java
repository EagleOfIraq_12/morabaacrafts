package com.morabaa.team.morabaacrafts.utils;

public class CacheDataSourceFactory /*implements DataSource.Factory */{
      
      /*private final Context context;
      private final DefaultDataSourceFactory defaultDatasourceFactory;
      private final long maxFileSize, maxCacheSize;
      
      public CacheDataSourceFactory(Context context, long maxCacheSize, long maxFileSize) {
            super();
            this.context = context;
            this.maxCacheSize = maxCacheSize;
            this.maxFileSize = maxFileSize;
            String userAgent = Util.getUserAgent(context, context.getString(R.string.app_name));
            DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
            defaultDatasourceFactory = new DefaultDataSourceFactory(this.context,
                  bandwidthMeter,
                  new DefaultHttpDataSourceFactory(userAgent, bandwidthMeter));
      }
      
      @Override
      public DataSource createDataSource() {
            LeastRecentlyUsedCacheEvictor evictor = new LeastRecentlyUsedCacheEvictor(maxCacheSize);
            SimpleCache simpleCache = new SimpleCache(new File(context.getCacheDir(), "media"),
                  evictor);
            return new CacheDataSource(simpleCache, defaultDatasourceFactory.createDataSource(),
                  new FileDataSource(), new CacheDataSink(simpleCache, maxFileSize),
                  CacheDataSource.FLAG_BLOCK_ON_CACHE | CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR,
                  null);
      }*/
}