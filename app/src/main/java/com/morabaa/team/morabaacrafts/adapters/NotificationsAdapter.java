package com.morabaa.team.morabaacrafts.adapters;

import android.annotation.SuppressLint;
import android.os.Build.VERSION_CODES;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.morabaa.team.morabaacrafts.R;
import com.morabaa.team.morabaacrafts.model.Notification;
import com.morabaa.team.morabaacrafts.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/** Created by eagle on 10/21/2017. */
public class NotificationsAdapter extends RecyclerView.Adapter<NotificationHolder> {

  private List<Notification> notifications;
  private OnNotificationClickedListener onNotificationClickedListener;

  public NotificationsAdapter(List<Notification> notifications) {
    this.notifications = notifications;
  }

  @Override
  public NotificationHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    CardView view =
        (CardView)
            LayoutInflater.from(parent.getContext())
                .inflate(R.layout.notification_layout, parent, false);
    return new NotificationHolder(view);
  }

  @RequiresApi(api = VERSION_CODES.LOLLIPOP)
  @SuppressLint("SetTextI18n")
  @Override
  public void onBindViewHolder(
      @NonNull final NotificationHolder notificationHolder,
      @SuppressLint("RecyclerView") final int position) {

    notificationHolder.txtTitle.setText(notifications.get(position).getTitle());
    notificationHolder.txtBody.setText(notifications.get(position).getBody());
    notificationHolder.txtDate.setText(
        Utils.getRelativeTimeSpan(notifications.get(position).getTimeStamp()));

    notificationHolder.view.setOnClickListener(
        view ->
            onNotificationClickedListener.onNotificationClicked(
                notifications.get(notificationHolder.getAdapterPosition())));
    notificationHolder.txtRemove.setOnClickListener(
        view ->
            onNotificationRemovedListener.onNotificationRemoved(
                notifications.get(notificationHolder.getAdapterPosition())));
  }

  public NotificationsAdapter setOnNotificationClickedListener(
      OnNotificationClickedListener onNotificationClickedListener) {
    this.onNotificationClickedListener = onNotificationClickedListener;
    return this;
  }

  @Override
  public int getItemCount() {
    return notifications.size();
  }

  public List<Notification> getNotifications() {
    return notifications;
  }

  public void setNotifications(List<Notification> notifications) {
    this.notifications = notifications;
    notifyDataSetChanged();
  }

  public void addNotifications(List<Notification> notifications) {
    this.notifications.addAll(notifications);
    notifyDataSetChanged();
  }

  public void clearNotifications() {
    this.notifications = new ArrayList<>();
    notifyDataSetChanged();
  }

  public interface OnNotificationClickedListener {

    void onNotificationClicked(Notification notification);
  }

  private OnNotificationRemovedListener onNotificationRemovedListener;

  public NotificationsAdapter setOnNotificationRemovedListener(
      OnNotificationRemovedListener onNotificationRemovedListener) {
    this.onNotificationRemovedListener = onNotificationRemovedListener;
    return this;
  }

  public interface OnNotificationRemovedListener {

    void onNotificationRemoved(Notification notification);
  }
}

class NotificationHolder extends RecyclerView.ViewHolder {

  CardView view;

  TextView txtTitle;
  TextView txtBody;
  TextView txtDate;
  TextView txtRemove;

  NotificationHolder(View itemView) {
    super(itemView);
    this.view = (CardView) itemView;

    txtTitle = view.findViewById(R.id.txtTitle);
    txtBody = view.findViewById(R.id.txtBody);
    txtDate = view.findViewById(R.id.txtDate);
    txtRemove = view.findViewById(R.id.txtRemove);
  }
}
