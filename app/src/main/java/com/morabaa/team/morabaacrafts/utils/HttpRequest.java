package com.morabaa.team.morabaacrafts.utils;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.security.ProviderInstaller;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.morabaa.team.morabaacrafts.interfaces.OnCompleteListener;
import com.morabaa.team.morabaacrafts.interfaces.OnCompleteListenerX;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/** Created by eagle on 6/12/2018. */
public class HttpRequest {

  /**
   * //Gson implementation "com.google.code.gson:gson:2.8.4"
   *
   * <p>//volley implementation 'com.android.volley:volley:1.0.0'
   */
  private static HttpRequest instance;

  public static int TIME_OUT = 10000;
  private RequestQueue requestQueue;
  private Context ctx;

  public HttpRequest(Context ctx) {
    this.ctx = ctx;
    this.requestQueue = Volley.newRequestQueue(ctx);
    setInstance(this);
    updateAndroidSecurityProvider(ctx);
  }

  public static void init(Context ctx) {
    new HttpRequest(ctx);
  }

  public static HttpRequest getInstance() {
    return instance;
  }

  private static void setInstance(HttpRequest instance) {
    HttpRequest.instance = instance;
  }

  @SuppressWarnings("unchecked")
  public void addRequest(
      final String url,
      final String jsonBody,
      final Type type,
      final String tag,
      final OnCompleteListener onCompleteListener) {
    try {
      RetryPolicy retryPolicy =
          new DefaultRetryPolicy(TIME_OUT, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

      StringRequest stringRequest =
          new StringRequest(
              Request.Method.POST,
              url,
              s -> {
                try {
                  if (!s.trim().substring(0, 1).equals("[")) {
                    s = "[" + s + "]";
                  }
                  System.out.println("HttpRequest download[]: " + s);

                  onCompleteListener.onComplete(
                      new GsonBuilder()
                          .serializeSpecialFloatingPointValues()
                          .create()
                          .fromJson(s, type));
                  //                            if (type instanceof List) {
                  //                                System.out.println("HttpRequest type[] = " +
                  // type.getClass().toString());
                  //                            }else {
                  //                                System.out.println("HttpRequest type = " +
                  // type.getClass().toString());
                  //                            }
                } catch (JsonSyntaxException e) {
                  onCompleteListener.onError(url + "\t" + e.toString());
                }
              },
              error -> {
                onCompleteListener.onError(url + "\t" + error.toString());
              }) {
            @Override
            public String getBodyContentType() {
              return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() {
              try {
                return jsonBody == null ? "".getBytes() : jsonBody.getBytes("utf-8");
              } catch (UnsupportedEncodingException uee) {
                onCompleteListener.onError(url + "\t jsonBody " + uee.getMessage());
                return null;
              }
            }

            @Override
            public Map<String, String> getHeaders() {
              return new HashMap<>();
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {

              String parsed;
              try {
                parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
              } catch (UnsupportedEncodingException e) {
                parsed = new String(response.data);
                onCompleteListener.onError(parsed);
              }
              return Response.success(parsed, HttpHeaderParser.parseCacheHeaders(response));
            }
          };
      stringRequest.setRetryPolicy(retryPolicy);
      stringRequest.setTag(tag);
      this.requestQueue.add(stringRequest);
    } catch (Exception e) {
      onCompleteListener.onError(url + "\t" + e.toString());
    }
  }

  public void addRequest(
      final String url,
      final String jsonBody,
      final Type type,
      final String tag,
      final OnCompleteListenerX onCompleteListener) {

    try {
      RetryPolicy retryPolicy =
          new DefaultRetryPolicy(TIME_OUT, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

      StringRequest stringRequest =
          new StringRequest(
              Request.Method.POST,
              url,
              s -> {
                System.out.println("HttpRequest download: " + s);
                try {
                  onCompleteListener.onComplete(Integer.parseInt(s));
                } catch (Exception e) {
                  if (s.equals("true")) {
                    onCompleteListener.onComplete(true);
                  } else if (s.equals("false")) {
                    onCompleteListener.onComplete(false);
                  } else if (s.trim().substring(0, 1).equals("{")) {
                    onCompleteListener.onComplete(
                        new GsonBuilder()
                            .serializeSpecialFloatingPointValues()
                            .create()
                            .fromJson(s, type));
                  } else if (s.trim().substring(0, 1).equals("[")) {
                    onCompleteListener.onComplete(
                        (List)
                            new GsonBuilder()
                                .serializeSpecialFloatingPointValues()
                                .create()
                                .fromJson(s, type));
                  } else {
                    onCompleteListener.onComplete(s);
                  }
                }
              },
              error -> {
                Log.e("VOLLEY", error.toString());
                onCompleteListener.onError(url + "\t" + error.toString());
              }) {
            @Override
            public String getBodyContentType() {
              return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() {
              try {
                return jsonBody == null ? null : jsonBody.getBytes("utf-8");
              } catch (UnsupportedEncodingException uee) {
                VolleyLog.wtf(
                    "Unsupported Encoding while trying to get the bytes of %s using %s",
                    jsonBody, "utf-8");
                return null;
              }
            }

            @Override
            public Map<String, String> getHeaders() {
              return new HashMap<>();
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {

              String parsed;
              try {
                parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
              } catch (UnsupportedEncodingException e) {
                parsed = new String(response.data);
                onCompleteListener.onError(parsed);
              }
              return Response.success(parsed, HttpHeaderParser.parseCacheHeaders(response));
            }
          };
      stringRequest.setRetryPolicy(retryPolicy);
      stringRequest.setTag(tag);
      this.requestQueue.add(stringRequest);
    } catch (Exception e) {
      onCompleteListener.onError(url + "\t" + e.toString());
    }
  }

  @SuppressWarnings("unchecked")
  public void createRequest(
      final String url,
      final String jsonBody,
      final Type type,
      final String tag,
      final OnCompleteListener onCompleteListener) {
    RequestQueue requestQueue = Volley.newRequestQueue(ctx);
    try {
      RetryPolicy retryPolicy =
          new DefaultRetryPolicy(25000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

      StringRequest stringRequest =
          new StringRequest(
              Request.Method.POST,
              url,
              s -> {
                System.out.println("HttpRequest download: " + s);
                if (!s.trim().substring(0, 1).equals("[")) {
                  s = "[" + s + "]";
                  System.out.println("HttpRequest download[]: " + s);
                }
                onCompleteListener.onComplete(
                    (List)
                        new GsonBuilder()
                            .serializeSpecialFloatingPointValues()
                            .create()
                            .fromJson(s, type));
              },
              error -> {
                Log.e("VOLLEY", error.toString());
                onCompleteListener.onError(url + "\t" + error.toString());
              }) {
            @Override
            public String getBodyContentType() {
              return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() {
              try {
                return jsonBody == null ? null : jsonBody.getBytes("utf-8");
              } catch (UnsupportedEncodingException uee) {
                VolleyLog.wtf(
                    "Unsupported Encoding while trying to get the bytes of %s using %s",
                    jsonBody, "utf-8");
                return null;
              }
            }

            @Override
            public Map<String, String> getHeaders() {
              return new HashMap<>();
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {

              String parsed;
              try {
                parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
              } catch (UnsupportedEncodingException e) {
                parsed = new String(response.data);
                onCompleteListener.onError(parsed);
              }
              return Response.success(parsed, HttpHeaderParser.parseCacheHeaders(response));
            }
          };
      stringRequest.setRetryPolicy(retryPolicy);
      stringRequest.setTag(tag);
      requestQueue.add(stringRequest);
    } catch (Exception e) {
      onCompleteListener.onError(url + "\t" + e.toString());
    }
  }

  public void removeRequest(String tag) {
    this.requestQueue.cancelAll(tag);
  }

  private void updateAndroidSecurityProvider(Context ctx) {
    try {
      ProviderInstaller.installIfNeeded(ctx);
    } catch (GooglePlayServicesRepairableException e) {
      // Thrown when Google Play Services is not installed, up-to-date, or enabled
      // Show dialog to allow users to install, update, or otherwise enable Google Play services.
      GooglePlayServicesUtil.getErrorDialog(e.getConnectionStatusCode(), (Activity) ctx, 0);
    } catch (GooglePlayServicesNotAvailableException e) {
      Log.e("SecurityException", "Google Play Services not available.");
    }
  }

  public abstract static class CheckSubscription {
    Context ctx;
    long personId;

    public CheckSubscription(Context ctx, long personId) {
      this.ctx = ctx;
      this.personId = personId;
      checkSubscription();
    }

    private void checkSubscription() {
      try {
        final String requestBody = "";
        RequestQueue requestQueue = Volley.newRequestQueue(ctx);
        RetryPolicy retryPolicy =
            new DefaultRetryPolicy(5000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        String url =
            "https://iid.googleapis.com/iid/info/"
                + FirebaseInstanceId.getInstance().getToken()
                + "?details=true";
        StringRequest stringRequest =
            new StringRequest(
                Request.Method.POST,
                url,
                s -> {
                  try {
                    JSONObject base = new JSONObject(s);
                    JSONObject rel = base.getJSONObject("rel");
                    JSONObject topics = rel.getJSONObject("topics");
                    Iterator<String> iter = topics.keys();
                    boolean b = false;
                    while (iter.hasNext()) {
                      String key = iter.next();
                      System.out.println(key);
                      if (key.equals("Shop" + personId)) {
                        b = true;
                      }
                    }
                    status(b);
                  } catch (JSONException e) {
                    e.printStackTrace();
                  }
                },
                error -> Log.e("VOLLEY", error.toString())) {
              @Override
              public String getBodyContentType() {
                return "application/json; charset=utf-8";
              }

              @Override
              public byte[] getBody() throws AuthFailureError {
                try {
                  return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                  VolleyLog.wtf(
                      "Unsupported Encoding while trying to get the bytes of %s using %s",
                      requestBody, "utf-8");
                  return null;
                }
              }

              @Override
              public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put(
                    "Authorization",
                    "Key=AAAAkOfjRu0:APA91bGN8_9kUxAvbezkHcgsWemhgo7Q4eNRTGHnpfdjApNzYGaWIFn-bmVZpbTXgX2mLj-yxrDDImDBSwnjBrSzb0wOvB0YnJLOSWOXYQTp9Syc60cDIP3V95g33dtPL-Lc29arnju6");
                headers.put("Content-Type", "application/json");
                return headers;
              }

              @Override
              protected Response<String> parseNetworkResponse(NetworkResponse response) {

                String parsed;
                try {
                  parsed =
                      new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                } catch (UnsupportedEncodingException e) {
                  parsed = new String(response.data);
                }
                return Response.success(parsed, HttpHeaderParser.parseCacheHeaders(response));
              }
            };
        stringRequest.setRetryPolicy(retryPolicy);
        requestQueue.add(stringRequest);
      } catch (Exception e) {

      }
    }

    public abstract void status(boolean subscribed);
  }

  public abstract static class MySubscriptions {

    Context ctx;

    public MySubscriptions(Context ctx) {
      this.ctx = ctx;
      getSubscriptions();
    }

    private void getSubscriptions() {
      try {
        final String requestBody = "";
        RequestQueue requestQueue = Volley.newRequestQueue(ctx);
        RetryPolicy retryPolicy =
            new DefaultRetryPolicy(5000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        String url =
            "https://iid.googleapis.com/iid/info/"
                + FirebaseInstanceId.getInstance().getToken()
                + "?details=true";
        StringRequest stringRequest =
            new StringRequest(
                Request.Method.POST,
                url,
                s -> {
                  try {
                    JSONObject base = new JSONObject(s);
                    JSONObject rel = base.getJSONObject("rel");
                    JSONObject topics = rel.getJSONObject("topics");
                    Iterator<String> iter = topics.keys();
                    List<String> subscriptions = new ArrayList<>();

                    while (iter.hasNext()) {
                      String key = iter.next();
                      System.out.println(key);
                      if (key.contains("Shop")) {
                        subscriptions.add(key);
                      }
                    }

                    onSubscriptionsReceived(subscriptions);
                  } catch (JSONException e) {
                    e.printStackTrace();
                  }
                },
                error -> Log.e("VOLLEY", error.toString())) {
              @Override
              public String getBodyContentType() {
                return "application/json; charset=utf-8";
              }

              @Override
              public byte[] getBody() {
                try {
                  return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                  VolleyLog.wtf(
                      "Unsupported Encoding while trying to get the bytes of %s using %s",
                      requestBody, "utf-8");
                  return null;
                }
              }

              @Override
              public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put(
                    "Authorization",
                    "Key=AAAAkOfjRu0:APA91bGN8_9kUxAvbezkHcgsWemhgo7Q4eNRTGHnpfdjApNzYGaWIFn-bmVZpbTXgX2mLj-yxrDDImDBSwnjBrSzb0wOvB0YnJLOSWOXYQTp9Syc60cDIP3V95g33dtPL-Lc29arnju6");
                headers.put("Content-Type", "application/json");
                return headers;
              }

              @Override
              protected Response<String> parseNetworkResponse(NetworkResponse response) {

                String parsed;
                try {
                  parsed =
                      new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                } catch (UnsupportedEncodingException e) {
                  parsed = new String(response.data);
                }
                return Response.success(parsed, HttpHeaderParser.parseCacheHeaders(response));
              }
            };
        stringRequest.setRetryPolicy(retryPolicy);
        requestQueue.add(stringRequest);
      } catch (Exception ignored) {

      }
    }

    public abstract void onSubscriptionsReceived(List<String> subscriptions);
  }
}
