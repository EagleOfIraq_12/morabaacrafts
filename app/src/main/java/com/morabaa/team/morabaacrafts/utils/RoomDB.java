package com.morabaa.team.morabaacrafts.utils;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.morabaa.team.morabaacrafts.model.Craft;
import com.morabaa.team.morabaacrafts.model.Craft.CraftDuo;
import com.morabaa.team.morabaacrafts.model.Governate;
import com.morabaa.team.morabaacrafts.model.Governate.GovernateDuo;
import com.morabaa.team.morabaacrafts.model.MediaItem;
import com.morabaa.team.morabaacrafts.model.MediaItem.MediaItemDuo;
import com.morabaa.team.morabaacrafts.model.Person;
import com.morabaa.team.morabaacrafts.model.Person.PersonDuo;
import com.morabaa.team.morabaacrafts.model.Rate;
import com.morabaa.team.morabaacrafts.model.Rate.RateDuo;
import com.morabaa.team.morabaacrafts.model.Request;
import com.morabaa.team.morabaacrafts.model.Request.RequestDuo;
import com.morabaa.team.morabaacrafts.model.Work;
import com.morabaa.team.morabaacrafts.model.Work.WorkDuo;
import com.morabaa.team.morabaacrafts.model.message.Message;
import com.morabaa.team.morabaacrafts.model.message.MessageDAO;

/** Created by eagle on 2/6/2018. */
@Database(
    entities = {
      Craft.class,
      Person.class,
      Governate.class,
      MediaItem.class,
      Rate.class,
      Request.class,
      Work.class,
      Message.class
    },
    version = 34,
    exportSchema = false)
public abstract class RoomDB extends RoomDatabase {
  public static final String DB_NAME = "crafts.db";
  private static RoomDB instance;

  public static RoomDB getInstance() {
    return instance;
  }

  public static void init(Context ctx) {
    instance =
        (instance == null)
            ? Room.databaseBuilder(ctx, RoomDB.class, DB_NAME)
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build()
            : instance;
  }

  public abstract CraftDuo craftDuo();

  public abstract GovernateDuo governateDuo();

  public abstract MediaItemDuo mediaItemDuo();

  public abstract PersonDuo personDuo();

  public abstract RateDuo rateDuo();

  public abstract RequestDuo requestDuo();

  public abstract WorkDuo workDuo();

  public abstract MessageDAO messageDAO();
}
