package com.morabaa.team.morabaacrafts.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.GsonBuilder;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.morabaa.team.morabaacrafts.R;
import com.morabaa.team.morabaacrafts.adapters.PagerAdapter;
import com.morabaa.team.morabaacrafts.fragments.FavoritesContainerFragment;
import com.morabaa.team.morabaacrafts.fragments.MainContainerFragment;
import com.morabaa.team.morabaacrafts.fragments.NotificationsFragment;
import com.morabaa.team.morabaacrafts.fragments.RequestsContainerFragment;
import com.morabaa.team.morabaacrafts.fragments.RequestsFragment;
import com.morabaa.team.morabaacrafts.interfaces.OnCompleteListener;
import com.morabaa.team.morabaacrafts.model.Notification;
import com.morabaa.team.morabaacrafts.model.Person;
import com.morabaa.team.morabaacrafts.utils.AWSS3Storage;
import com.morabaa.team.morabaacrafts.utils.HttpRequest;
import com.morabaa.team.morabaacrafts.utils.LocalData;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import q.rorbin.badgeview.Badge;
import q.rorbin.badgeview.QBadgeView;

/** Created by eagle on 6/12/2018. */
public class MainActivity extends AppCompatActivity
    implements RequestsFragment.OnDataUpdatedListener {

  private static MainActivity instance;

  @SuppressLint("StaticFieldLeak")
  private LinearLayout layoutActivityToolbar;

  private int backCount = 0;
  public BottomNavigationViewEx navMain;
  public ViewPager mainPager;
  private Badge badge;
  private PagerAdapter pagerAdapter;
  private Stack<Integer> mainPagerBackStack = new Stack<>();
  private Notification notification = new Notification();

  public static MainActivity getInstance() {
    return instance;
  }

  public void setLayoutActivityToolbar(LinearLayout view) {
    layoutActivityToolbar.removeAllViews();
    layoutActivityToolbar.addView(view);
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    //    try {
    //      PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
    //      if (pInfo.versionCode < 111) {
    //        signOut(new View(this));
    //        return;
    //      }
    //    } catch (PackageManager.NameNotFoundException e) {
    //      e.printStackTrace();
    //    }

    //                  requestWindowFeature(Window.FEATURE_SWIPE_TO_DISMISS);

    StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
    StrictMode.setVmPolicy(builder.build());
    setContentView(R.layout.activity_main);
    FirebaseMessaging.getInstance().subscribeToTopic("All");
    try {
      FirebaseMessaging.getInstance()
          .subscribeToTopic("Person" + Person.getCurrentPerson().getId())
          .addOnCompleteListener(
              task -> {
                if (task.isSuccessful()) {
                  //       Toast.makeText(instance, "subscribeToTopic done ",
                  // Toast.LENGTH_SHORT).show();
                }
              });
      fillCurrentPersonInfo();
    } catch (Exception e) {
    }

    instance = this;
    layoutActivityToolbar = findViewById(R.id.layoutActivityToolbar);
    navMain = findViewById(R.id.navMain);
    mainPager = findViewById(R.id.mainPager);

    findViewById(R.id.btnSignOut).setOnClickListener(this::signOut);
    findViewById(R.id.btnUpdate).setOnClickListener(this::updateUserInfo);
    Toolbar toolbar = findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    setTitle(" ");

    DrawerLayout drawer = findViewById(R.id.drawer_layout);
    ActionBarDrawerToggle toggle =
        new ActionBarDrawerToggle(
            this,
            drawer,
                toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close);
    drawer.addDrawerListener(toggle);
    toggle.syncState();

    navMain.getMenu().clear();
    navMain.inflateMenu(R.menu.main_bottom_nav_menu);

    navMain.enableAnimation(false);
    navMain.enableShiftingMode(false);
    navMain.enableItemShiftingMode(false);

    initPager();
    notification.setPerson(Person.getCurrentPerson());
    notification.getNewCount(
        "",
        new OnCompleteListener<Integer>() {
          @Override
          public void onComplete(List<Integer> integers) {
            badge =
                new QBadgeView(MainActivity.this)
                    .setBadgeNumber(integers.get(0))
                    .setGravityOffset(12, 2, true)
                    .bindTarget(navMain.getBottomNavigationItemView(2))
                    .setOnDragStateChangedListener(
                        (dragState, badge, targetView) -> {
                          if (Badge.OnDragStateChangedListener.STATE_SUCCEED == dragState)
                            Toast.makeText(
                                    MainActivity.this, "tips_badge_removed", Toast.LENGTH_SHORT)
                                .show();
                        });
          }

          @Override
          public void onError(String error) {}
        });
  }

  private void fillCurrentPersonInfo() {
    Person person = Person.getCurrentPerson();
    ImageView imgCurrentPersonImage = findViewById(R.id.imgCurrentPersonImage);
    TextView txtCurrentPersonName = findViewById(R.id.txtCurrentPersonName);
    TextView txtCurrentPersonAddress = findViewById(R.id.txtCurrentPersonAddress);
    TextView txtCurrentPersonAge = findViewById(R.id.txtCurrentPersonAge);
    Glide.with(this)
        .load(AWSS3Storage.getReference(person.getMediaItem().getUrl()))
        .into(imgCurrentPersonImage);
    txtCurrentPersonName.setText(person.getName());
    txtCurrentPersonAddress.setText(person.getAddress());
    txtCurrentPersonAge.setText(String.valueOf(person.getAge()));
  }

  private void updateUserInfo(View view) {
    Intent updateIntent = new Intent(MainActivity.this, SignUpActivity.class);
    if (LocalData.getInstance().get(Person.CURRENT_PERSON) != null) {
      updateIntent.putExtra(
          "personString",
          new GsonBuilder()
              .serializeSpecialFloatingPointValues()
              .create()
              .toJson(Person.getCurrentPerson(), Person.class));
    }
    startActivity(updateIntent);
  }

  private void signOut(View view) {
    FirebaseMessaging.getInstance()
        .unsubscribeFromTopic("Person" + Person.getCurrentPerson().getId());
    LocalData.getInstance().remove(Person.CURRENT_PERSON);

    new Handler(Looper.getMainLooper())
        .postDelayed(
            () -> {
              Intent loginIntent = new Intent(MainActivity.this, PhoneActivity.class);
              startActivity(loginIntent);
              finish();
            },
            2000);
  }

  public void initPager() {
    List<Fragment> fragments = new ArrayList<>();
    fragments.add(MainContainerFragment.newInstance());
    //        if (Person.CURRENT_USER_TYPE == Person.PERSON_TYPE.CRAFT_MAN) {
    //            fragments.add(MainContainerFragment.newInstance());
    //        } else if (Person.CURRENT_USER_TYPE == Person.PERSON_TYPE.USER) {
    //            Fragment fragment = MainContainerFragment.newInstance();
    //            fragments.add(fragment);
    //            Bundle bundle = new Bundle();
    //            bundle.putString("craftManString",
    //                    new GsonBuilder()
    //                            .serializeSpecialFloatingPointValues()
    //                            .create()
    //                            .toJson(
    //                                    Person.getCurrentPerson(), Person.class
    //                            ));
    //
    //        } else {
    //            Intent loginIntent = new Intent(
    //                    MainActivity.this,
    //                    PhoneActivity.class);
    //            startActivity(loginIntent);
    //            finish();
    //        }
    fragments.add(RequestsContainerFragment.newInstance(false, RequestsPageIndex));
    fragments.add(NotificationsFragment.newInstance());
    fragments.add(FavoritesContainerFragment.newInstance());

    pagerAdapter = new PagerAdapter(getSupportFragmentManager(), fragments);
    mainPager.setAdapter(pagerAdapter);

    mainPager.setOnPageChangeListener(
        new ViewPager.OnPageChangeListener() {
          @Override
          public void onPageScrolled(
              int position, float positionOffset, int positionOffsetPixels) {}

          @Override
          public void onPageSelected(int position) {
            //                        if (position == 1) {
            //                            navMain.setBackgroundColor(
            //
            // getResources().getColor(R.color.colorButtonPrimary));
            //                        } else {
            //                            navMain.setBackgroundColor(
            //                                    getResources().getColor(R.color.colorBackground));
            //                        }
            if (mainPagerBackStack.contains(position)) {
              mainPagerBackStack.removeElement(position);
            }
            mainPagerBackStack.push(position);
            if (position == 2 && badge != null) {
              notification.setSeen(
                  "",
                  new OnCompleteListener<Integer>() {
                    @Override
                    public void onComplete(List<Integer> integers) {
                      badge.hide(true);
                    }

                    @Override
                    public void onError(String error) {}
                  });
            }
          }

          @Override
          public void onPageScrollStateChanged(int state) {}
        });

    navMain.setupWithViewPager(mainPager, true);
    mainPagerBackStack.push(0);
  }

  private int RequestsPageIndex = 0;

  @Override
  public void onBackPressed() {
    System.out.println("Back : ");

    DrawerLayout drawer = findViewById(R.id.drawer_layout);
    if (drawer.isDrawerOpen(GravityCompat.START)) {
      drawer.closeDrawer(GravityCompat.START);
    } else if (mainPagerBackStack.empty()) {
      if (backCount > 0) {
        super.onBackPressed();
        HttpRequest.getInstance().removeRequest("");
        finish();
      } else {
        Toast.makeText(MainActivity.this, "اضغط مره اخرى للخروج.", Toast.LENGTH_SHORT).show();
        backCount++;
        new Handler().postDelayed(() -> backCount = 0, 3000);
      }
    } else {
      mainPager.setCurrentItem(mainPagerBackStack.lastElement());
      int last = mainPagerBackStack.lastElement();
      try {
        if (pagerAdapter.getItem(last).getChildFragmentManager().getBackStackEntryCount() > 1) {
          pagerAdapter.getItem(last).getChildFragmentManager().popBackStack();
        } else {
          mainPagerBackStack.pop();
          if (mainPagerBackStack.empty()) {
            if (last != 0) {
              mainPagerBackStack.push(0);
            }
          }
        }
      } catch (Exception e) {
        System.out.println("Back : " + e.getMessage());
      }
    }
  }

  //  public void onBackPressed() {
  //    DrawerLayout drawer = findViewById(R.id.drawer_layout);
  //    if (drawer.isDrawerOpen(GravityCompat.START)) {
  //      drawer.closeDrawer(GravityCompat.START);
  //
  //    } else if (!mainPagerBackStack.empty()) {
  //      mainPager.setCurrentItem(mainPagerBackStack.lastElement());
  //      int last = mainPagerBackStack.lastElement();
  //
  //      try {
  //        if (pagerAdapter.getItem(last).getChildFragmentManager().getBackStackEntryCount() > 1) {
  //          pagerAdapter.getItem(last).getChildFragmentManager().popBackStack();
  //        } else {
  //          mainPagerBackStack.pop();
  //          if (mainPagerBackStack.empty()) {
  //            if (last != 0) {
  //              mainPagerBackStack.push(0);
  //            }
  //          }
  //        }
  //      } catch (Exception e) {
  //      }
  //
  //    } else {
  //      if (backCount > 0) {
  //        super.onBackPressed();
  //        HttpRequest.getInstance().removeRequest("");
  //        finish();
  //      } else {
  //        Toast.makeText(MainActivity.this, "اضغط مره اخرى للخروج.", Toast.LENGTH_SHORT).show();
  //        backCount++;
  //        new Handler().postDelayed(() -> backCount = 0, 3000);
  //      }
  //    }
  //  }

  @Override
  public void onDataUpdated(int tabIndex) {
    assert mainPager.getAdapter() != null;
    RequestsPageIndex = tabIndex;
    initPager();
    navMain.setCurrentItem(1);
  }
}
