package com.morabaa.team.morabaacrafts.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.morabaa.team.morabaacrafts.R;
import com.morabaa.team.morabaacrafts.activities.NotificationActivity;
import com.morabaa.team.morabaacrafts.adapters.NotificationsAdapter;
import com.morabaa.team.morabaacrafts.interfaces.OnCompleteListener;
import com.morabaa.team.morabaacrafts.model.Notification;
import com.morabaa.team.morabaacrafts.model.Person;

import java.util.ArrayList;
import java.util.List;

public class NotificationsFragment extends Fragment {
  public NotificationsFragment() {
    // Required empty public constructor
  }

  private boolean waiting = true;
  private RecyclerView recyclerViewNotifications;
  private NotificationsAdapter notificationsAdapter;
  private List<Notification> notifications = new ArrayList<>();
  private ProgressBar bottomProgressBar;
  private int pagesCount = 0;

  public static NotificationsFragment newInstance() {
    NotificationsFragment fragment = new NotificationsFragment();
    Bundle args = new Bundle();
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if (getArguments() != null) {}
  }

  @Override
  public View onCreateView(
      @NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    return inflater.inflate(R.layout.fragment_notifications, container, false);
  }

  public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    recyclerViewNotifications = view.findViewById(R.id.recyclerViewNotifications);
    bottomProgressBar = view.findViewById(R.id.bottomProgressBar);

    Notification notification = new Notification();
    notification.setPerson(
        Person.getCurrentPerson() == null ? Person.getCurrentPerson() : Person.getCurrentPerson());
    bottomProgressBar.setVisibility(View.GONE);

    notificationsAdapter = new NotificationsAdapter(notifications);

    notificationsAdapter
        .setOnNotificationClickedListener(this::showNotification)
        .setOnNotificationRemovedListener(this::removeNotification);

    recyclerViewNotifications.setLayoutManager(
        new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));

    recyclerViewNotifications.setAdapter(notificationsAdapter);
    load(notification);
    recyclerViewNotifications.addOnScrollListener(
        new RecyclerView.OnScrollListener() {
          @Override
          public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);

            if (!recyclerView.canScrollVertically(1) && waiting) {
              waiting = false;
              load(notification);
            }
          }
        });
  }

  private void removeNotification(Notification notification) {
    notification.setPerson(
        Person.getCurrentPerson() == null ? Person.getCurrentPerson() : Person.getCurrentPerson());
    notification.remove(
        "",
        new OnCompleteListener<Notification>() {
          @Override
          public void onComplete(List<Notification> notifications) {
            pagesCount = 0;
            notificationsAdapter.clearNotifications();
            load(notification);
          }

          @Override
          public void onError(String error) {
            System.out.println("error = " + error);
            Toast.makeText(getContext(), "خطأ في العمليه", Toast.LENGTH_SHORT).show();
          }
        });
  }

  private void showNotification(Notification notification) {
    Intent intent = new Intent(getContext(), NotificationActivity.class);
    intent.putExtra("parms", notification.getParms());
    getContext().startActivity(intent);
  }

  private void load(Notification notification) {
    bottomProgressBar.setVisibility(View.VISIBLE);
    notification.getAll(
        "",
        pagesCount++,
        new OnCompleteListener<Notification>() {
          @Override
          public void onComplete(List<Notification> notifications) {
            notificationsAdapter.addNotifications(notifications);
            waiting = true;
            bottomProgressBar.setVisibility(View.GONE);
          }

          @Override
          public void onError(String error) {

            bottomProgressBar.setVisibility(View.GONE);
          }
        });
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
  }

  @Override
  public void onDetach() {
    super.onDetach();
  }
}
