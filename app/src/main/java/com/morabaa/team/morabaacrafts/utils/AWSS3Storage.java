package com.morabaa.team.morabaacrafts.utils;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.Log;

import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;

import java.io.File;


public class AWSS3Storage {

    private Context ctx;
    private static AWSS3Storage instance;

    public static final String ROOT = "https://s3.us-east-2.amazonaws.com/morabaacrafts-userfiles-mobilehub-518695353/";
    public static final String subDir = "public/";

    public AWSS3Storage(Context ctx) {
        this.ctx = ctx;
    }

    public static AWSS3Storage getInstance(@Nullable Context ctx) {
        instance = instance == null ? new AWSS3Storage(ctx) : instance;
        return instance;
    }

    public void upload(Uri uri,
                       final OnUploadProgressListener onUploadProgressListener) {
        String path = ImageUtils.saveLocalFile(ctx, uri);
        path = ImageUtils.compress(path, 1024L * 300L);
        uri = Uri.fromFile(new File(path));

        System.out.println("AWSS3Storage uploading = " + uri.getLastPathSegment());
        Uri finalUri = uri;
        AWSMobileClient.getInstance()
                .initialize(ctx, awsStartupResult -> {
                    TransferUtility transferUtility =
                            TransferUtility.builder()
                                    .context(ctx.getApplicationContext())
                                    .awsConfiguration(
                                            AWSMobileClient
                                                    .getInstance()
                                                    .getConfiguration()
                                    ).s3Client(
                                    new AmazonS3Client(
                                            AWSMobileClient.getInstance()
                                                    .getCredentialsProvider()
                                    )
                            ).build();
                    String name = finalUri.getLastPathSegment();
                    TransferObserver uploadObserver = transferUtility.upload(
                            subDir + name,
                            new File(finalUri.getPath()),
                            CannedAccessControlList.PublicReadWrite);
                    // Attach a listener to the observer to get state update and progress notifications
                    uploadObserver
                            .setTransferListener(new TransferListener() {

                                @Override
                                public void onStateChanged(int id, TransferState state) {
                                    if (TransferState.COMPLETED == state) {
                                        System.out.println("AWSS3Storage uploadObserver = " +
                                                uploadObserver.getBucket() + "/"
                                                + uploadObserver.getKey());
                                        onUploadProgressListener.onSuccess(uploadObserver.getKey());
                                    }
                                }

                                @Override
                                public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                                    double percentDoneD = ((double) bytesCurrent /
                                            (double) bytesTotal) * 100;
                                    int percentDone = (int) percentDoneD;
                                    onUploadProgressListener.onProgress(percentDoneD);
                                    System.out.println("AWSS3Storage ID:" + id + " bytesCurrent: " + bytesCurrent
                                            + " bytesTotal: " + bytesTotal + " " + percentDone + "%");
                                }

                                @Override
                                public void onError(int id, Exception ex) {
                                    System.out.println("AWSS3Storage Error: " + ex.getMessage());
                                    onUploadProgressListener.onFailure(ex.getMessage());
                                }

                            });

                    if (TransferState.COMPLETED == uploadObserver.getState()) {
                        // Handle a completed upload.
                    }

                    System.out.println("AWSS3Storage Bytes Transferrred: " + uploadObserver.getBytesTransferred());
                    System.out.println("AWSS3Storage Bytes Total: " + uploadObserver.getBytesTotal());

                }).execute();
    }

    public void uploadMini(Uri uri,
                           final OnUploadProgressListener onUploadProgressListener) {
        String path = ImageUtils.saveLocalFile(ctx, uri);
        path = ImageUtils.compress(path, 1024L * 75L);
        uri = Uri.fromFile(new File(path));

        System.out.println("AWSS3Storage uploading = " + uri.getLastPathSegment());
        Uri finalUri = uri;
        AWSMobileClient.getInstance()
                .initialize(ctx, awsStartupResult -> {
                    TransferUtility transferUtility =
                            TransferUtility.builder()
                                    .context(ctx.getApplicationContext())
                                    .awsConfiguration(
                                            AWSMobileClient
                                                    .getInstance()
                                                    .getConfiguration()
                                    ).s3Client(
                                    new AmazonS3Client(
                                            AWSMobileClient.getInstance()
                                                    .getCredentialsProvider()
                                    )
                            ).build();
                    String name = finalUri.getLastPathSegment();
                    TransferObserver uploadObserver = transferUtility.upload(
                            subDir + name,
                            new File(finalUri.getPath()),
                            CannedAccessControlList.PublicReadWrite);
                    // Attach a listener to the observer to get state update and progress notifications
                    uploadObserver

                            .setTransferListener(new TransferListener() {

                                @Override
                                public void onStateChanged(int id, TransferState state) {
                                    if (TransferState.COMPLETED == state) {
                                        System.out.println("AWSS3Storage uploadObserver = " +
                                                uploadObserver.getBucket() + "/"
                                                + uploadObserver.getKey());
                                        onUploadProgressListener.onSuccess(uploadObserver.getKey());
                                    }
                                }

                                @Override
                                public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                                    double percentDoneD = ((double) bytesCurrent /
                                            (double) bytesTotal) * 100;
                                    int percentDone = (int) percentDoneD;
                                    onUploadProgressListener.onProgress(percentDoneD);
                                    System.out.println("AWSS3Storage ID:" + id + " bytesCurrent: " + bytesCurrent
                                            + " bytesTotal: " + bytesTotal + " " + percentDone + "%");
                                }

                                @Override
                                public void onError(int id, Exception ex) {
                                    System.out.println("AWSS3Storage Error: " + ex.getMessage());
                                    onUploadProgressListener.onFailure(ex.getMessage());
                                }

                            });

                    if (TransferState.COMPLETED == uploadObserver.getState()) {
                        // Handle a completed upload.
                    }

                    System.out.println("AWSS3Storage Bytes Transferrred: " + uploadObserver.getBytesTransferred());
                    System.out.println("AWSS3Storage Bytes Total: " + uploadObserver.getBytesTotal());

                }).execute();
    }

    public void downloadWithTransferUtility(String fileName) {

        TransferUtility transferUtility =
                TransferUtility.builder()
                        .context(ctx.getApplicationContext())
                        .awsConfiguration(AWSMobileClient.getInstance().getConfiguration())
                        .s3Client(new AmazonS3Client(AWSMobileClient.getInstance().getCredentialsProvider()))
                        .build();

        TransferObserver downloadObserver =
                transferUtility.download(
                        "s3Folder/s3Key.txt",
                        new File("/path/to/file/localFile.txt"));

        // Attach a listener to the observer to get state update and progress notifications
        downloadObserver.setTransferListener(new TransferListener() {

            @Override
            public void onStateChanged(int id, TransferState state) {
                if (TransferState.COMPLETED == state) {
                    // Handle a completed upload.
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                float percentDonef = ((float) bytesCurrent / (float) bytesTotal) * 100;
                int percentDone = (int) percentDonef;

                Log.d("LOG_TAG", "   ID:" + id + "   bytesCurrent: " + bytesCurrent + "   bytesTotal: " + bytesTotal + " " + percentDone + "%");
            }

            @Override
            public void onError(int id, Exception ex) {
                // Handle errors
            }

        });

        // If you prefer to poll for the data, instead of attaching a
        // listener, check for the state and progress in the observer.
        if (TransferState.COMPLETED == downloadObserver.getState()) {
            // Handle a completed upload.
        }

        Log.d("LOG_TAG", "Bytes Transferrred: " + downloadObserver.getBytesTransferred());
        Log.d("LOG_TAG", "Bytes Total: " + downloadObserver.getBytesTotal());
    }

    public static String getReference(String fileName) {
        return ROOT + fileName;
    }

    public interface OnUploadProgressListener {

        void onProgress(double progress);

        void onSuccess(String fileName);

        void onFailure(String error);
    }

    public interface OnDownloadProgressListener {

        void onProgress(double progress);

        void onSuccess(Uri uri);

        void onFailure(String error);
    }
}
