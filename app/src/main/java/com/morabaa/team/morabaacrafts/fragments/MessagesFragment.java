package com.morabaa.team.morabaacrafts.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.morabaa.team.morabaacrafts.R;
import com.morabaa.team.morabaacrafts.adapters.MessagesAdapter;
import com.morabaa.team.morabaacrafts.interfaces.OnCompleteListener;
import com.morabaa.team.morabaacrafts.interfaces.OnSingleCompleteListener;
import com.morabaa.team.morabaacrafts.model.Person;
import com.morabaa.team.morabaacrafts.model.message.Message;
import com.morabaa.team.morabaacrafts.model.message.MessageRepo;

import java.util.ArrayList;
import java.util.List;

public class MessagesFragment extends Fragment {
  public MessagesFragment() {
    // Required empty public constructor
  }

  private boolean isCanLoad = true;
  private RecyclerView recyclerViewMessages;
  private MessagesAdapter messagesAdapter;
  private List<Message> messages = new ArrayList<>();
  private ProgressBar bottomProgressBar;
  private EditText txtMassageBody;
  private String chatId;
  private Message lastMessage = new Message();

  public static MessagesFragment newInstance(String chatId) {
    MessagesFragment fragment = new MessagesFragment();
    Bundle args = new Bundle();
    args.putString("chatId", chatId);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if (getArguments() != null) {
      chatId = getArguments().getString("chatId");
    }
  }

  @Override
  public View onCreateView(
      @NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    return inflater.inflate(R.layout.fragment_messages, container, false);
  }

  public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    recyclerViewMessages = view.findViewById(R.id.recyclerViewMessages);
    bottomProgressBar = view.findViewById(R.id.bottomProgressBar);
    txtMassageBody = view.findViewById(R.id.txtMassageBody);
    view.findViewById(R.id.btnSendMessage)
        .setOnClickListener(
            this::send
            //        view1 -> {
            //          Message message = new Message();
            //          message.setId(new Random().nextLong());
            //          message.setBody(txtMassageBody.getText().toString());
            //          MessageRepo.push(
            //              message,
            //              new OnSingleCompleteListener<Message>() {
            //                @Override
            //                public void onComplete(Message message) {}
            //
            //                @Override
            //                public void onError(String error) {}
            //              });
            //          messagesAdapter.addMessage(message);
            //          txtMassageBody.setText("");
            //          recyclerViewMessages.scrollToPosition(messagesAdapter.getMessages().size() -
            // 1);
            //        }
            );
    bottomProgressBar.setVisibility(View.GONE);
    messagesAdapter = new MessagesAdapter(messages);
    messagesAdapter.setOnMessageClickedListener(message -> {

    });

    recyclerViewMessages.setLayoutManager(
        new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));

    recyclerViewMessages.setAdapter(messagesAdapter);
    load(true);

    recyclerViewMessages.addOnScrollListener(
        new RecyclerView.OnScrollListener() {
          @Override
          public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            if (newState == AbsListView.OnScrollListener.SCROLL_STATE_FLING)
              if (!recyclerView.canScrollVertically(-1) && isCanLoad) {
                isCanLoad = false;
                load(false);
              }
          }
        });
  }

  private void load(boolean init) {
    lastMessage.setConversationKey(chatId);
    bottomProgressBar.setVisibility(View.VISIBLE);
    MessageRepo.getAllBefore(
        lastMessage,
        new OnCompleteListener<Message>() {
          @Override
          public void onComplete(List<Message> messages) {
            messagesAdapter.addMessages(messages);
            isCanLoad = true;
            bottomProgressBar.setVisibility(View.GONE);
            if (messages != null && messages.size() > 0) {
              lastMessage = messages.get(0);
            }
            if (init) {
              MessageRepo.listenToNewMessages(
                  lastMessage,
                  new OnSingleCompleteListener<Message>() {
                    @Override
                    public void onComplete(Message message) {
                      messagesAdapter.addMessage(message);
                      recyclerViewMessages.scrollToPosition(
                          messagesAdapter.getMessages().size() - 1);
                    }

                    @Override
                    public void onError(String error) {}
                  });
            }
          }

          @Override
          public void onError(String error) {
            isCanLoad = true;
            bottomProgressBar.setVisibility(View.GONE);
          }
        });
  }

  public void send(View view) {
    if (txtMassageBody.getText().toString().length() <= 0) {
      return;
    }
    Message message = new Message();
    message.setBody(txtMassageBody.getText().toString());
    message.setConversationKey(chatId);
    message.setDate(System.currentTimeMillis());
    message.setSenderId(Person.getCurrentPerson().getId());
    MessageRepo.push(
        message,
        new OnSingleCompleteListener<Message>() {
          @Override
          public void onComplete(Message message) {
            //            messagesAdapter.addMessage(message);
            //            recyclerViewMessages.scrollToPosition(
            //                messagesAdapter.getMessages().size() == messages.size()
            //                    ? messagesAdapter.getMessages().size() - 1
            //                    : messagesAdapter.getMessages().size() - messages.size());
          }

          @Override
          public void onError(String error) {
            Toast.makeText(getContext(), "Error", Toast.LENGTH_LONG).show();
          }
        });
    txtMassageBody.setText("");
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
  }

  @Override
  public void onDetach() {
    super.onDetach();
  }
}
