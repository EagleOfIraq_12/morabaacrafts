package com.morabaa.team.morabaacrafts.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build.VERSION_CODES;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.morabaa.team.morabaacrafts.R;
import com.morabaa.team.morabaacrafts.model.Craft;
import com.morabaa.team.morabaacrafts.utils.AWSS3Storage;

import java.util.ArrayList;
import java.util.List;

/** Created by eagle on 10/21/2017. */
public class CraftsAdapter extends RecyclerView.Adapter<CraftHolder> {

  private List<Craft> crafts;
  private Context ctx;
  private OnCraftClickedListener onCraftClickedListener;

  public CraftsAdapter(Context ctx, List<Craft> crafts) {
    this.crafts = crafts;
    this.ctx = ctx;
  }

  @Override
  public CraftHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    CardView view =
        (CardView)
            LayoutInflater.from(parent.getContext()).inflate(R.layout.craft_layout, parent, false);

    return new CraftHolder(view);
  }

  @RequiresApi(api = VERSION_CODES.LOLLIPOP)
  @SuppressLint("SetTextI18n")
  @Override
  public void onBindViewHolder(
      @NonNull final CraftHolder craftHolder, @SuppressLint("RecyclerView") final int position) {

    craftHolder.txtCraftName.setText(crafts.get(position).getName());

    if (crafts.get(position).getMediaItem() != null) {
      Glide.with(ctx)
          .load(AWSS3Storage.getReference(crafts.get(position).getMediaItem().getUrl()))
          .into(craftHolder.imgCraftImage);
    }

    craftHolder.view.setOnClickListener(
        view ->
            onCraftClickedListener.onCraftClicked(crafts.get(craftHolder.getAdapterPosition())));
  }

  public void setOnCraftClickedListener(OnCraftClickedListener onCraftClickedListener) {
    this.onCraftClickedListener = onCraftClickedListener;
  }

  @Override
  public int getItemCount() {
    return crafts.size();
  }

  public List<Craft> getCrafts() {
    return crafts;
  }

  public void setCrafts(List<Craft> crafts) {
    //        List<Craft> craftList=crafts.stream().filter(c -> c.get)
    this.crafts = crafts;
    notifyDataSetChanged();
  }

  public void clear() {
    crafts = new ArrayList<>();
    notifyDataSetChanged();
  }

  public interface OnCraftClickedListener {

    void onCraftClicked(Craft craft);
  }
}

class CraftHolder extends RecyclerView.ViewHolder {

  CardView view;
  ImageView imgCraftImage;
  TextView txtCraftName;

  CraftHolder(View itemView) {
    super(itemView);
    this.view = (CardView) itemView;

    imgCraftImage = view.findViewById(R.id.imgCraftImage);
    txtCraftName = view.findViewById(R.id.txtCraftName);
  }
}
