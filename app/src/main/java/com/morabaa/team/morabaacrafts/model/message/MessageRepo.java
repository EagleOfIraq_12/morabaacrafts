package com.morabaa.team.morabaacrafts.model.message;

import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.morabaa.team.morabaacrafts.interfaces.OnCompleteListener;
import com.morabaa.team.morabaacrafts.interfaces.OnSingleCompleteListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MessageRepo {

  private static final String COLLECTION_NAME = "messages";

  public static void push(
      Message message, OnSingleCompleteListener<Message> onSingleCompleteListener) {
    FirebaseFirestore.getInstance()
        .collection(COLLECTION_NAME)
        .add(message)
        .addOnSuccessListener(documentReference -> onSingleCompleteListener.onComplete(message))
        .addOnFailureListener(e -> onSingleCompleteListener.onError(e.getMessage()));
  }

  public static void listenToNewMessages(
      Message message, OnSingleCompleteListener<Message> onSingleCompleteListener) {
    FirebaseFirestore.getInstance()
        .collection(COLLECTION_NAME)
        .whereEqualTo("conversationKey", message.getConversationKey())
        .orderBy("date", Query.Direction.DESCENDING)
        .limit(1)
        .addSnapshotListener(
            (queryDocumentSnapshots, e) -> {
              if (e != null) {
                onSingleCompleteListener.onError(e.getMessage());
                return;
              }
              assert queryDocumentSnapshots != null;
              List<Message> newMessages = queryDocumentSnapshots.toObjects(Message.class);
              if (newMessages.size() > 0) onSingleCompleteListener.onComplete(newMessages.get(0));
            });
  }

  public static void getAllBefore(
      Message lastMessage, OnCompleteListener<Message> onCompleteListener) {
    if (lastMessage.getBody() == null) {
      //      Query latestQuery
      FirebaseFirestore.getInstance()
          .collection(COLLECTION_NAME)
          .whereEqualTo("conversationKey", lastMessage.getConversationKey())
          .orderBy("date", Query.Direction.DESCENDING)
          .limit(1)
          .get()
          .addOnSuccessListener(
              documentSnapshots -> {
                if (documentSnapshots.getDocuments().size() > 0) {
                  DocumentSnapshot lastVisible = documentSnapshots.getDocuments().get(0);
                  Message lastMessage2 = lastVisible.toObject(Message.class);
                  assert lastMessage2 != null;
                  getAllBefore(lastMessage2, onCompleteListener);
                  return;
                }
                onCompleteListener.onComplete(new ArrayList<>());
              })
          .addOnFailureListener(e -> onCompleteListener.onError(e.getMessage()));
      return;
    }

    /*
     FAILED_PRECONDITION: The query requires an index. You can create it here:
     https://console.firebase.google.com/project/morabaacrafts/database/firestore/indexes?create_index=EghtZXNzYWdlcxoTCg9jb252ZXJzYXRpb25LZXkQAhoICgRkYXRlEAMaDAoIX19uYW1lX18QAw
    */

    //    Query next =
    FirebaseFirestore.getInstance()
        .collection(COLLECTION_NAME)
        .whereEqualTo("conversationKey", lastMessage.getConversationKey())
        .whereLessThan("date", lastMessage.getDate())
        .orderBy("date", Query.Direction.DESCENDING)
        .limit(3)
        .get()
        .addOnSuccessListener(
            documentSnapshots -> {
              List<Message> messages = new ArrayList<>();
              for (DocumentSnapshot document : documentSnapshots) {
                messages.add(document.toObject(Message.class));
              }
              if (messages.contains(lastMessage)) {
                onCompleteListener.onComplete(new ArrayList<>());
                return;
              }
              Collections.reverse(messages);
              onCompleteListener.onComplete(messages);
            });
  }
}
