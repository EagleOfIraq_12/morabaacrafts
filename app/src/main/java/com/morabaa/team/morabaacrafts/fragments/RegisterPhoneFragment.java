package com.morabaa.team.morabaacrafts.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.morabaa.team.morabaacrafts.R;
import com.morabaa.team.morabaacrafts.activities.PhoneActivity;
import com.morabaa.team.morabaacrafts.interfaces.OnCompleteListener;
import com.morabaa.team.morabaacrafts.model.Person;
import com.morabaa.team.morabaacrafts.utils.CheckPermission;

import java.util.List;

public class RegisterPhoneFragment extends Fragment {

  private Button btnRegister;
  private EditText txtPhoneNumber;

  private String phoneNumber;

  private OnRegisterListener onRegisterListener;

  public RegisterPhoneFragment() {}

  public static RegisterPhoneFragment newInstance(String phoneNumber) {
    RegisterPhoneFragment fragment = new RegisterPhoneFragment();
    Bundle args = new Bundle();
    args.putString(PhoneActivity.PHONE_NUMBER, phoneNumber);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if (getArguments() != null) {
      phoneNumber = getArguments().getString(PhoneActivity.PHONE_NUMBER);
    }
  }

  @Override
  public View onCreateView(
      LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    return inflater.inflate(R.layout.fragment_register_phone, container, false);
  }

  @SuppressLint({"MissingPermission", "HardwareIds"})
  @Override
  public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    txtPhoneNumber = view.findViewById(R.id.txtPhoneNumber);
    txtPhoneNumber.setText(phoneNumber == null ? "" : phoneNumber);
    btnRegister = view.findViewById(R.id.btnRegister);
    TelephonyManager telephonyManager =
        ((TelephonyManager) getContext().getSystemService(Context.TELEPHONY_SERVICE));
    assert telephonyManager != null;
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
      CheckPermission.newInstance(
          getContext(),
          Manifest.permission.READ_PHONE_NUMBERS,
          granted -> {
            if (granted) {
              txtPhoneNumber.setText(
                  phoneNumber != null ? phoneNumber : telephonyManager.getLine1Number());
            } else {
              txtPhoneNumber.setText(phoneNumber != null ? phoneNumber : "");
            }
          });
    } else {
      txtPhoneNumber.setText(phoneNumber != null ? phoneNumber : "");
    }

    btnRegister.setOnClickListener(
        view1 -> {
          view1.setEnabled(false);
          new Person() {
            {
              setPhoneNumber(txtPhoneNumber.getText().toString());
            }
          }.isRegistered(
              "",
              new OnCompleteListener<Boolean>() {
                @Override
                public void onComplete(List<Boolean> booleans) {
                  boolean registered = booleans.get(0);
                  if (!registered) {
                    onRegisterListener.onRegister(txtPhoneNumber.getText().toString());
                  } else {
                    Snackbar.make(txtPhoneNumber, "هذا الرقم مسجل مسبقا", 5000)
                        .setAction("تسجيل الدخول؟", RegisterPhoneFragment.this::goToLogIn)
                        .show();
                  }
                }

                @Override
                public void onError(String error) {
                  getChildFragmentManager()
                      .beginTransaction()
                      .detach(RegisterPhoneFragment.this)
                      .attach(RegisterPhoneFragment.this)
                      .commit();
                }
              });
        });
  }

  private void goToLogIn(View view) {
    Intent intent = new Intent(getContext(), PhoneActivity.class);
    startActivity(intent);
    getActivity().finish();
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    if (context instanceof OnRegisterListener) {
      onRegisterListener = (OnRegisterListener) context;
    } else {
      System.out.println(" must implement OnRegisterListener");
    }
  }

  @Override
  public void onDetach() {
    super.onDetach();
    onRegisterListener = null;
  }

  public interface OnRegisterListener {
    void onRegister(String phoneNumber);
  }
}
