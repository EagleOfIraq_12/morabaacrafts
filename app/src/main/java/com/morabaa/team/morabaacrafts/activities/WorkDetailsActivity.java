package com.morabaa.team.morabaacrafts.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.GsonBuilder;
import com.morabaa.team.morabaacrafts.R;
import com.morabaa.team.morabaacrafts.adapters.ImagesPagerAdapter;
import com.morabaa.team.morabaacrafts.adapters.RatesAdapter;
import com.morabaa.team.morabaacrafts.interfaces.OnCompleteListener;
import com.morabaa.team.morabaacrafts.model.MediaItem;
import com.morabaa.team.morabaacrafts.model.Person;
import com.morabaa.team.morabaacrafts.model.Rate;
import com.morabaa.team.morabaacrafts.model.Work;
import com.morabaa.team.morabaacrafts.utils.AWSS3Storage;
import com.morabaa.team.morabaacrafts.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class WorkDetailsActivity extends AppCompatActivity {

  private Work work;
  private RecyclerView recyclerViewRates;
  private RatesAdapter ratesAdapter;
  private ImageView imgWorkImage;
  private TextView txtWorkName;
  private TextView txtWorkDescription;
  private TextView txtWorkDate;
  private ViewPager pagerWorkImages;
  private ProgressBar progressBarRates;
  private RatingBar ratingWorkRating;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_work_details);
    getWindow().setSoftInputMode(LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    String workString = getIntent().getStringExtra("workString");
    work =
        new GsonBuilder()
            .serializeSpecialFloatingPointValues()
            .create()
            .fromJson(workString, Work.class);
    recyclerViewRates = findViewById(R.id.recyclerViewRates);
    ratingWorkRating = findViewById(R.id.ratingWorkRating);
    imgWorkImage = findViewById(R.id.imgWorkImage);
    txtWorkName = findViewById(R.id.txtWorkName);
    txtWorkDescription = findViewById(R.id.txtWorkDescription);
    txtWorkDate = findViewById(R.id.txtWorkDate);
    progressBarRates = findViewById(R.id.progressBarRates);
    pagerWorkImages = findViewById(R.id.pagerWorkImages);

    try {
      work.getById(
          "",
          new OnCompleteListener<Work>() {
            @Override
            public void onComplete(List<Work> works) {
              work = works.get(0);
              initWork();
            }

            @Override
            public void onError(String error) {}
          });
      initWork();
      setTitle(work.getTitle());
    } catch (Exception e) {
      System.out.println("e.getMessage() = " + e.getMessage());
    }

    final Rate rate = new Rate();
    rate.setRated(Person.getCurrentPerson());
    rate.setWork(work);
    rate.getWorkRates(
        "",
        new OnCompleteListener<Rate>() {
          @Override
          public void onComplete(List<Rate> rates) {
            initRates(rates);
          }

          @Override
          public void onError(String error) {}
        });
  }

  private void initWork() {
    txtWorkName.setText(work.getTitle());
    txtWorkDescription.setText(work.getDescription());
    txtWorkDate.setText(Utils.getRelativeTimeSpan(work.getTimeStamp()));

    String url = work.getMediaItems().get(0).getUrl();

    try {
      Glide.with(this).load(AWSS3Storage.getReference(url)).thumbnail(0.1f).into(imgWorkImage);
    } catch (Exception e) {
      // You cannot start a load for a destroyed activity
    }
    initPagerWorkImages();

    imgWorkImage.setOnClickListener(
        view -> {
          Intent imageIntent = new Intent(WorkDetailsActivity.this, FullScreenImageActivity.class);
          imageIntent.putExtra("title", work.getTitle());
          imageIntent.putExtra("url", work.getMediaItems().get(0).getUrl());
          startActivity(imageIntent);
        });

    float rating =
        work.getRate().getRatingSpeed()
            + work.getRate().getRatingAccuracy()
            + work.getRate().getRatingDate();
    ratingWorkRating.setRating(rating / 3);
  }

  private void initRates(List<Rate> rates) {
    progressBarRates.setVisibility(View.GONE);
    ratesAdapter = new RatesAdapter(this, new ArrayList<>());

    recyclerViewRates.setLayoutManager(
        new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
    ratesAdapter.setOnRateClickedListener(
        rate -> {
          if (Person.getCurrentPerson() != null
              && rate.getRater().getId() == Person.getCurrentPerson().getId()) {
            AlertDialog alertDialog = new AlertDialog.Builder(this).create();
            alertDialog.setCancelable(true);
            LayoutInflater inflater =
                (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.dialog_rate_work_request_layout, null, false);
            view.findViewById(R.id.frameDialog).setBackgroundColor(Color.argb(0, 0, 0, 0));
            Button btnOk = view.findViewById(R.id.btnOk);
            final EditText txtRequestCraftManRate = view.findViewById(R.id.txtRequestCraftManRate);
            final RatingBar ratingCraftManRating = view.findViewById(R.id.ratingReceiverRating);

            final ProgressBar progressBarUpdating = view.findViewById(R.id.progressBarUpdating);

            txtRequestCraftManRate.setText(rate.getComment());
            ratingCraftManRating.setRating(rate.getRatingAccuracy());
            ratingCraftManRating.setRating(rate.getRatingDate());
            ratingCraftManRating.setRating(rate.getRatingSpeed());

            btnOk.setOnClickListener(
                view1 -> {
                  progressBarUpdating.setVisibility(View.VISIBLE);
                  rate.setComment(txtRequestCraftManRate.getText().toString());
                  rate.setRatingAccuracy(ratingCraftManRating.getRating());
                  rate.setRatingDate(ratingCraftManRating.getRating());
                  rate.setRatingSpeed(ratingCraftManRating.getRating());
                  rate.setWork(work);
                  rate.setRater(Person.getCurrentPerson());
                  rate.updateWorkRate(
                      "",
                      new OnCompleteListener<Rate>() {
                        @Override
                        public void onComplete(List<Rate> rates) {
                          progressBarUpdating.setVisibility(View.GONE);
                          alertDialog.dismiss();
                          Intent workActivityIntent =
                              new Intent(WorkDetailsActivity.this, WorkDetailsActivity.class);
                          workActivityIntent.putExtra(
                              "workString",
                              new GsonBuilder()
                                  .serializeSpecialFloatingPointValues()
                                  .create()
                                  .toJson(work, Work.class));
                          startActivity(workActivityIntent);
                          finish();
                        }

                        @Override
                        public void onError(String error) {
                          progressBarUpdating.setVisibility(View.GONE);
                          alertDialog.dismiss();
                        }
                      });
                });

            alertDialog.setView(view);
            alertDialog.show();
          }
        });
    recyclerViewRates.setAdapter(ratesAdapter);
    ratesAdapter.setRates(rates);
  }

  void initPagerWorkImages() {
    List<MediaItem> mediaItems = new ArrayList<>();
    for (MediaItem mediaItem : work.getMediaItems()) {
      if (mediaItem != null) {
        mediaItems.add(mediaItem);
      }
    }
    pagerWorkImages.setAdapter(new ImagesPagerAdapter(WorkDetailsActivity.this, mediaItems));
  }
}
