package com.morabaa.team.morabaacrafts.activities;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.morabaa.team.morabaacrafts.R;
import com.morabaa.team.morabaacrafts.utils.FireStorage;
import com.morabaa.team.morabaacrafts.utils.FireStorage.OnDownloadProgressListener;
import com.morabaa.team.morabaacrafts.utils.FireStorage.OnUploadProgressListener;
import com.morabaa.team.morabaacrafts.utils.LocalStorageFile;
import com.morabaa.team.morabaacrafts.utils.LocalStorageFile.FILE_TYPES;
import com.morabaa.team.morabaacrafts.utils.LocalStorageFile.OnFileReceivedListener;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class StorageActivity extends AppCompatActivity {


    private Uri imageUri;
    private ImageView imgUpload;
    private SeekBar seekUpload;
    private ImageView imgDownload;
    private TextView txtMediaName;
    private ProgressBar progressUpload;
    private ProgressBar progressDownload;
    private StorageReference mStorageRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_storage);
        mStorageRef = FirebaseStorage.getInstance().getReference();
        imgUpload = findViewById(R.id.imgUpload);
        imgDownload = findViewById(R.id.imgDownload);
        Button btnUpload = findViewById(R.id.btnUpload);
        Button btnDownload = findViewById(R.id.btnDownload);
        progressDownload = findViewById(R.id.progressDownload);
        progressUpload = findViewById(R.id.progressUpload);
        seekUpload = findViewById(R.id.seekUpload);
        txtMediaName = findViewById(R.id.txtMediaName);

        imgUpload.setOnClickListener(view -> {
            LocalStorageFile localStorageFile =
                    new LocalStorageFile(StorageActivity.this, FILE_TYPES.IMAGES,
                            FILE_TYPES.VIDEOS)
                            .setOnFileReceivedListener(
                                    new OnFileReceivedListener() {
                                        @Override
                                        public void onFileReceived(Uri uri, String name,
                                                                   String ext, String type) {
                                            imageUri = uri;
                                            txtMediaName.setText(name + "." + ext);
                                            Glide.with(StorageActivity.this)
                                                    .load(uri)
                                                    .into(imgUpload);
                                            LocalStorageFile.releaseInstance();
                                        }

                                        @Override
                                        public void onFileError(String error) {
                                            LocalStorageFile.releaseInstance();
                                        }
                                    });
        });

        btnUpload.setOnClickListener(view -> {
            progressUpload.setVisibility(View.VISIBLE);
            seekUpload.setProgress(0);
            FireStorage.getInstance(StorageActivity.this).upload(
                    imageUri, new OnUploadProgressListener() {
                        @Override
                        public void onProgress(double progress) {
                            seekUpload.setProgress((int) Math.ceil(progress));
                            Toast.makeText(StorageActivity.this,
                                    "" + (int) Math.ceil(progress),
                                    Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onSuccess(String fileName) {
                            Toast.makeText(StorageActivity.this, "" + fileName,
                                    Toast.LENGTH_SHORT).show();
                            progressUpload.setVisibility(View.GONE);

                        }

                        @Override
                        public void onFailure(String error) {
                            Toast.makeText(StorageActivity.this, "" + error,
                                    Toast.LENGTH_SHORT).show();
                            progressUpload.setVisibility(View.GONE);

                        }
                    }
            );

//                        lastFileName = new File(imageUri.getPath()).getName();
//                        riversRef = mStorageRef.child("images/" + lastFileName + ".jpg");
//
//                        riversRef.putFile(imageUri)
//                              .addOnSuccessListener(new OnSuccessListener<TaskSnapshot>() {
//                                    @Override
//                                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
//                                          // Get a URL to the uploaded content
//                                          Task<Uri> downloadUrl = riversRef.getDownloadUrl();
//                                          Glide.with(StorageActivity.this)
//                                                .load(downloadUrl)
//                                                .thumbnail(.1f)
//                                                .into(imgDownload);
//                                          progressUpload.setVisibility(View.GONE);
//                                    }
//                              })
//                              .addOnFailureListener(new OnFailureListener() {
//                                    @Override
//                                    public void onFailure(@NonNull Exception exception) {
//                                          // Handle unsuccessful uploads
//                                          // ...
//                                    }
//                              }).addOnProgressListener(new OnProgressListener<TaskSnapshot>() {
//                              @RequiresApi(api = VERSION_CODES.N)
//                              @Override
//                              public void onProgress(TaskSnapshot taskSnapshot) {
//                                    long bytesTransferred = taskSnapshot.getBytesTransferred();
//                                    long totalByteCount = taskSnapshot.getTotalByteCount();
//                                    seekUpload.setMax((int) totalByteCount);
//                                    seekUpload.setProgress((int) bytesTransferred);
//                                    Double p = (
//                                          bytesTransferred /
//                                                totalByteCount
//                                    ) * 100.0d;
//                                    System.out.println(
//                                          "BytesTransferred : " + bytesTransferred + "\t" +
//                                                "TotalByteCount : " + totalByteCount + "\t"
//                                                + seekUpload.getProgress() +
//                                                " %"
//                                    );
//                              }
//                        });

        });
        btnDownload.setOnClickListener(view -> {
            progressDownload.setVisibility(View.VISIBLE);
            seekUpload.setProgress(0);
            FireStorage.getInstance(StorageActivity.this)
                    .download("Screenshot_٢٠١٨٠٨٠١-١٩٥٧٠٢.png", new OnDownloadProgressListener() {

                        @Override
                        public void onSuccess(Uri uri) {
                            Glide.with(StorageActivity.this)
                                    .load(uri)
                                    .thumbnail(0.1f)
                                    .into(imgDownload);
                            progressDownload.setVisibility(View.GONE);
                        }

                        @Override
                        public void onProgress(double progress) {
                            seekUpload.setProgress((int) Math.ceil(progress));
                            Toast.makeText(StorageActivity.this,
                                    "" + (int) Math.ceil(progress),
                                    Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onFailure(String error) {
                            Toast.makeText(StorageActivity.this, "" + error,
                                    Toast.LENGTH_SHORT).show();
                            progressDownload.setVisibility(View.GONE);

                        }
                    });

        });
    }

    public void download(String fileName) {
        File localFile = null;
        try {
            localFile = File.createTempFile("images", "jpg");
            final File finalLocalFile = localFile;
            StorageReference riversRef = mStorageRef.child("images/" + fileName + ".jpg");

            riversRef.getFile(localFile)
                    .addOnSuccessListener(
                            new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(
                                        FileDownloadTask.TaskSnapshot taskSnapshot) {
                                    progressDownload.setVisibility(View.GONE);
                                    Uri uri = Uri.fromFile(finalLocalFile);
                                    try {
                                        final InputStream imageStream = getContentResolver()
                                                .openInputStream(uri);
                                        final Bitmap selectedImage = BitmapFactory
                                                .decodeStream(imageStream);
                                        imgDownload.setImageBitmap(selectedImage);
                                    } catch (IOException e) {
                                        Toast.makeText(StorageActivity.this,
                                                "Something went wrong",
                                                Toast.LENGTH_LONG).show();
                                    }
                                    // Successfully downloaded data to local file
                                    // ...
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle failed download
                    // ...
                }
            });
        } catch (IOException e) {
        }
    }
}
