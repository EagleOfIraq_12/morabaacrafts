package com.morabaa.team.morabaacrafts.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.morabaa.team.morabaacrafts.R;
import com.morabaa.team.morabaacrafts.fragments.ConfirmPhoneFragment;
import com.morabaa.team.morabaacrafts.fragments.RegisterInfoFragment;
import com.morabaa.team.morabaacrafts.fragments.RegisterPhoneFragment;

public class RegisterActivity extends AppCompatActivity
    implements RegisterPhoneFragment.OnRegisterListener,
        ConfirmPhoneFragment.OnPhoneConfirmListener {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_register);
    String phoneNumber = getIntent().getStringExtra("phoneNumber");
    if (phoneNumber != null) {
      getSupportFragmentManager()
          .beginTransaction()
          .replace(R.id.frameRegister, RegisterInfoFragment.newInstance(""))
          .addToBackStack(null)
          .commit();
      return;
    }
    getSupportFragmentManager()
        .beginTransaction()
        .replace(R.id.frameRegister, RegisterPhoneFragment.newInstance(""))
        .addToBackStack(null)
        .commit();
  }

  @Override
  public void onConfirm(String confirmCode) {}

  @Override
  public void onRegister(String phoneNumber) {
    Intent intent = new Intent(this, PhoneActivity.class);
    intent.putExtra("phoneNumber", phoneNumber);
    intent.putExtra("register", "register");
    startActivity(intent);
    finish();
  }
}
