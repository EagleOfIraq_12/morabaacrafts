package com.morabaa.team.morabaacrafts.bottomSheets;

public interface OnDoneListener {
  void onDone(boolean ok);
}
