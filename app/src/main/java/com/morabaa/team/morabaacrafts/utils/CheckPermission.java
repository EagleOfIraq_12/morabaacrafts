package com.morabaa.team.morabaacrafts.utils;

import android.content.Context;
import android.content.Intent;

public class CheckPermission {

    private static CheckPermission instance;

    public static CheckPermission getInstance() {
        return instance;
    }

    public static CheckPermission newInstance(Context ctx, String p, OnPermissionGrantedListener onPermissionGrantedListener) {
        return new CheckPermission(ctx, p, onPermissionGrantedListener);
    }

    public static void releaseInstance() {
        try {
            CheckPermissionActivity.getInstance().finish();
        } catch (Exception ignored) {
        }
    }

    public CheckPermission(Context ctx, String permission, OnPermissionGrantedListener onPermissionGrantedListener) {
        instance = this;
        Intent intent = new Intent(ctx, CheckPermissionActivity.class);
        intent.putExtra("reqCode", 99);
        intent.putExtra("permission", permission);
        ctx.startActivity(intent);
        this.onPermissionGrantedListener = onPermissionGrantedListener;
    }

    private OnPermissionGrantedListener onPermissionGrantedListener;

    public OnPermissionGrantedListener getOnPermissionGrantedListener() {
        return onPermissionGrantedListener;
    }

    public void setOnPermissionGrantedListener(OnPermissionGrantedListener onPermissionGrantedListener) {
        this.onPermissionGrantedListener = onPermissionGrantedListener;
    }

    public interface OnPermissionGrantedListener {
        void onPermissionGranted(boolean granted);
    }

}
